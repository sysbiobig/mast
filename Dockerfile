FROM ubuntu:18.04

RUN apt-get -qq update && \
    apt-get -y install make && \
    apt-get -y install gfortran && \
    apt-get -y install python3 && \
    apt-get -y install python3-dev && \
    apt-get -y install python3-venv && \
    apt-get -y install imagemagick && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Setup virtual environment
ENV VIRTUAL_ENV=/opt/venv/sysbiobig
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

WORKDIR /home/mast

# Install dependencies:
COPY requirements.txt .
RUN pip install --upgrade pip && \
    pip install -r requirements.txt

# Clone the application:
COPY . .
COPY policy.xml /etc/ImageMagick-6/

# Build and install PDE module
WORKDIR /home/mast/src/simulator/PyPDELab/fortran
RUN ./build.sh && \
    cd ../.. && \
    ./install.sh

# Setup the entry point
WORKDIR /home/mast

RUN mkdir data
RUN mkdir output
RUN mkdir plot

# We want to pre-generate the matplotlib font-list cache in a way that keeps the cache in the docker image.
# We do this by making matplotlib keep its cache in /var/cache by setting the environment variable MPLCONFIGDIR.
# We then import # matplotlib's pyplot later to trigger this caching behavior.

RUN mkdir -m 777 /var/cache/matplotlib
ENV MPLCONFIGDIR=/var/cache/matplotlib

ENV PYTHONPATH="${PYTHONPATH}:/home/mast"

ENTRYPOINT ["python3"]