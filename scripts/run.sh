#!/bin/bash

# path to mast folder
MAST_PATH="/nfsd/sysbiobig/miliamikel/mast/"

# names of simulation configurations (i.e. names subfolders in "data/")
# element in the array must be separeted by one space. no space before/after the "=" symbol, the "(" symbol and the ")" symbol.
#MAST_CONFS=("pdlp_0.08_cure_newantigen_effect" "pdlp_0.1_cure_newantigen_effect" "pdlp_0.4_cure_newantigen_effect" "pdlp_0.6_cure_newantigen_effect" "pdlp_0.8_cure_newantigen_effect")
#MAST_CONFS=("adjchange_0.01_newantigen_effect" "adjchange_0.05_newantigen_effect" "adjchange_0.1_newantigen_effect" "adjchange_0.5_newantigen_effect")
#MAST_CONFS=("pdlp_0.4_adj_0.05_newantigen_effect" "pdlp_0.1_adj_0.05_newantigen_effect")
#MAST_CONFS=("pdlp_0.08_newantigen_effect_2" "pdlp_0.4_newantigen_effect_3" "adjchange_0.01_newantigen_effect_2" "adjchange_0.05_newantigen_effect_2" "adjchange_0.1_newantigen_effect_2")

#MAST_CONFS=("treg_caf_bulk_cms1_A" "treg_caf_bulk_cms2_A" "treg_caf_bulk_cms3_A" "treg_caf_bulk_cms4_A" "treg_caf_sc_cms1_A" "treg_caf_sc_cms2_A" "treg_caf_sc_cms3_A" "treg_caf_sc_cms4_A")
#MAST_CONFS=("treg_caf_bulk_cms1_B" "treg_caf_bulk_cms2_B" "treg_caf_bulk_cms3_B" "treg_caf_bulk_cms4_B" "treg_caf_sc_cms1_B" "treg_caf_sc_cms2_B" "treg_caf_sc_cms3_B" "treg_caf_sc_cms4_B")

#MAST_CONFS=("test_grid_50")
MAST_CONFS=("test_grid_100")
#MAST_CONFS=("test_grid_200")
#MAST_CONFS=("test_grid_300")
#MAST_CONFS=("test_grid_400")
#MAST_CONFS=("cms3_paper_sc_ncons_030")

##MAST_CONFS=("cms1_paper_bulk_newant_001" "cms1_paper_bulk_newant_002" "cms1_paper_bulk_newant_004" "cms1_paper_bulk_newant_008" "cms1_paper_bulk_newant_010" "cms1_paper_bulk_newant_016")
##MAST_CONFS=("cms2_paper_sc_newant_001" "cms2_paper_sc_newant_002" "cms2_paper_sc_newant_004" "cms2_paper_sc_newant_008" "cms2_paper_sc_newant_010" "cms2_paper_sc_newant_016")
##MAST_CONFS=("cms3_paper_sc_newant_001" "cms3_paper_sc_newant_002" "cms3_paper_sc_newant_004" "cms3_paper_sc_newant_008" "cms3_paper_sc_newant_010" "cms3_paper_sc_newant_016")
##MAST_CONFS=("cms4_paper_bulk_newant_001" "cms4_paper_bulk_newant_002" "cms4_paper_bulk_newant_004" "cms4_paper_bulk_newant_008" "cms4_paper_bulk_newant_010" "cms4_paper_bulk_newant_016")

##MAST_CONFS=("cms1_paper_bulk_pdlp_002" "cms1_paper_bulk_pdlp_004" "cms1_paper_bulk_pdlp_008" "cms1_paper_bulk_pdlp_016" "cms1_paper_bulk_pdlp_020" "cms1_paper_bulk_pdlp_032")
##MAST_CONFS=("cms2_paper_sc_pdlp_002" "cms2_paper_sc_pdlp_004" "cms2_paper_sc_pdlp_008" "cms2_paper_sc_pdlp_016" "cms2_paper_sc_pdlp_020" "cms2_paper_sc_pdlp_032")
##MAST_CONFS=("cms3_paper_sc_pdlp_002" "cms3_paper_sc_pdlp_004" "cms3_paper_sc_pdlp_008" "cms3_paper_sc_pdlp_016" "cms3_paper_sc_pdlp_020" "cms3_paper_sc_pdlp_032")
##MAST_CONFS=("cms4_paper_bulk_pdlp_002" "cms4_paper_bulk_pdlp_004" "cms4_paper_bulk_pdlp_008" "cms4_paper_bulk_pdlp_016" "cms4_paper_bulk_pdlp_020" "cms4_paper_bulk_pdlp_032")

##MAST_CONFS=("cms1_paper_bulk_ncons_0002" "cms1_paper_bulk_ncons_005" "cms1_paper_bulk_ncons_010" "cms1_paper_bulk_ncons_015" "cms1_paper_bulk_ncons_020" "cms1_paper_bulk_ncons_030")
##MAST_CONFS=("cms2_paper_sc_ncons_0002" "cms2_paper_sc_ncons_005" "cms2_paper_sc_ncons_010" "cms2_paper_sc_ncons_015" "cms2_paper_sc_ncons_020" "cms2_paper_sc_ncons_030")
#MAST_CONFS=("cms3_paper_sc_ncons_0002" "cms3_paper_sc_ncons_005" "cms3_paper_sc_ncons_010" "cms3_paper_sc_ncons_015" "cms3_paper_sc_ncons_020" "cms3_paper_sc_ncons_030")
##MAST_CONFS=("cms4_paper_bulk_ncons_0002" "cms4_paper_bulk_ncons_005" "cms4_paper_bulk_ncons_010" "cms4_paper_bulk_ncons_015" "cms4_paper_bulk_ncons_020" "cms4_paper_bulk_ncons_030")

##MAST_CONFS=("cms1_paper_bulk_cafrec_01" "cms1_paper_bulk_cafrec_05" "cms1_paper_bulk_cafrec_10" "cms1_paper_bulk_cafrec_15" "cms1_paper_bulk_cafrec_20" "cms1_paper_bulk_cafrec_30")
##MAST_CONFS=("cms2_paper_sc_cafrec_01" "cms2_paper_sc_cafrec_05" "cms2_paper_sc_cafrec_10" "cms2_paper_sc_cafrec_15" "cms2_paper_sc_cafrec_20" "cms2_paper_sc_cafrec_30")
##MAST_CONFS=("cms3_paper_sc_cafrec_01" "cms3_paper_sc_cafrec_05" "cms3_paper_sc_cafrec_10" "cms3_paper_sc_cafrec_15" "cms3_paper_sc_cafrec_20" "cms3_paper_sc_cafrec_30")
##MAST_CONFS=("cms4_paper_bulk_cafrec_01" "cms4_paper_bulk_cafrec_05" "cms4_paper_bulk_cafrec_10" "cms4_paper_bulk_cafrec_15" "cms4_paper_bulk_cafrec_20" "cms4_paper_bulk_cafrec_30")

for c in ${MAST_CONFS[@]}; do
  sbatch singularity_job.slurm -p $MAST_PATH -c $c
done