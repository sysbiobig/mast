#!/bin/bash

# path to mast folder
MAST_PATH="/nfsd/sysbiobig/miliamikel/mast/"

for d in ${MAST_CONFS[@]}; do
  sbatch bash_plot.slurm -t "agent-comparison" -p $MAST_PATH -d $d -s "all" -k "all" -a "all"
done