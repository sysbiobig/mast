# Singularity image

Singularity is a container technology. It allows the user to create a container (just like Docker ones) where all the
needed software can be autonomously managed. By software, we mean that almost everything, from the operative system up,
can be customized and run regardless the hosting OS.

You can find a more detailed documentation [here](https://clusterdeiguide.readthedocs.io/en/latest).

### Convert Docker to Singularity image

To convert a docker image into a singularity one, you can proceed as follows:

```shell
# Move inside project folder
cd <project>                                                  

# Build the singularity image
singularity build <container> docker-daemon://registry.gitlab.com/sysbiobig/mast:latest     
```

Where `<container>` is the path to the container `.sif` file, it can be set to `singularity/mast.sif`.

### Run singularity image

In order to run the singularity image you can proceed in two main ways:

#### Singularity command: run

`singularity run [run options...] <container>` this command will launch a Singularity container and execute a 
runscript if one is defined for that container. The runscript is a metadata file within the container that contains
shell commands. If the file is present (and executable) then this command will execute that file within the container
automatically. All arguments following the container name will be passed directly to the runscript.

```shell
singularity run --bind $(pwd)/output:/home/mast/output \      # Bind the output/ folder
                --bind $(pwd)/data:/home/mast/data \          # Bind the data/ folder
                --bind $(pwd)/plot:/home/mast/plot \          # Bind the plot/ folder
                --pwd /home/mast \
                <container> \                                 # Singularity image
                src/main.py [...]                             
```

#### Singularity command: exec
`singularity exec [exec options...] <container> <command>` run a command within a container

```shell
singularity exec --bind $(pwd)/output:/home/mast/output \     # Bind the output/ folder
                 --bind $(pwd)/data:/home/mast/data \         # Bind the data/ folder
                 --bind $(pwd)/plot:/home/mast/plot \         # Bind the plot/ folder
                 --pwd /home/mast \
                 <container> \                                # Singularity image
                 python3 src/main.py [...]                    # Command to execute (i.e., python3)
```