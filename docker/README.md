# Docker Image

### Build image

```shell
cd mast
docker build -t registry.gitlab.com/sysbiobig/mast .
```

### Update image to registry

```shell
docker login registry.gitlab.com
docker build -t registry.gitlab.com/sysbiobig/mast .
docker push registry.gitlab.com/sysbiobig/mast
```

### Run docker image

In order to run the docker image you can use the following commands:

```shell
cd mast
docker run -v $(pwd)/output:/home/mast/output \
           -v $(pwd)/data:/home/mast/data \
           -v $(pwd)/plot:/home/mast/plot \
           -u $(id -u):$(id -g) \
           -ti registry.gitlab.com/sysbiobig/mast \
           src/main.py [...]
```