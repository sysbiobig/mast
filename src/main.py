import os
import sys

sys.path.append(os.getcwd())

from argparse import ArgumentParser
from datetime import datetime
from simulator import Simulator
from plotter import Plotter
from multiprocessing import Pool, cpu_count

from handler import ParameterHandler


def parse_command_line():

    parser = ArgumentParser(description='MAST simulator')

    # Select which mode MAST should work with
    flag = parser.add_mutually_exclusive_group(required=True)
    flag.add_argument('-s', '--simu', action='store_true', help='Enable to act as simulator')
    flag.add_argument('-p', '--plot', action='store_true', help='Enable to plot simulation results in an interactive way')
    flag.add_argument('-b', '--batch', action='store_true', help='Enable to plot simulation results in a batch way')

    # Simulator mode optional parameter
    parser.add_argument('--id',
                        type=str,
                        default=None,
                        help='Simulation identifier')

    parser.add_argument('-par', '--parallel', action='store_true', help='Enable simulation parallel execution')
    parser.add_argument('-d', '--debug', action='store_true', help='Enable debug messages')

    parser.add_argument('-e', '--environments',
                        type=str,
                        default='all',
                        help='List of comma separated environments you want to consider or all. '
                             'By default all instances will be simulated')

    # Batch mode optional parameter
    parser.add_argument('--chart',
                        type=str,
                        default='single',
                        choices=['single', 'multiple', 'agent-comparison', 'antigens', 'frames', 'gif'],
                        help='Type of chart you want to create')

    parser.add_argument('--index',
                        type=str,
                        default='mean_std',
                        choices=['mean_std', 'median'],
                        help='Type of numerical index you want to observe.')

    parser.add_argument('--env',
                        type=str,
                        help='Environment whose systems you want to plot')

    parser.add_argument('--num_cycles',
                        type=int,
                        help='Number of cycles of simulation that you want to consider')

    parser.add_argument('--num_simulations',
                        type=int,
                        help='Number of simulations that you want to consider')

    parser.add_argument('--systems',
                        type=str,
                        default='all',
                        help='List of comma separated systems you want to consider or all')

    parser.add_argument('--simulations',
                        type=str,
                        default='all',
                        help='Specific simulation to be selected (to be used only with antigens chart). '
                             'Must be less than the number of simulations considered')

    parser.add_argument('--agents',
                        type=str,
                        default='all',
                        help='List of comma separated agents you want to consider or all')

    parser.add_argument('--scale',
                        type=str,
                        default='lin',
                        choices=['lin', 'log'],
                        help='Scale to be used in the chart')

    parser.add_argument('--base',
                        type=str,
                        default='log2',
                        choices=['log2', 'log3', 'log5', 'log10'],
                        help='Base of the logarithm to be used in the chart')

    parser.add_argument('--ext',
                        type=str,
                        default='png',
                        choices=['png', 'jpg', 'jpeg', 'pdf', 'svg'],
                        help='File extension to be used')

    args = parser.parse_args()

    mode = [
        args.simu,
        args.plot,
        args.batch
    ]

    simulation_args = {
        'id': args.id,
        'environments': args.environments,
        'debug': args.debug,
        'parallel': args.parallel
    }

    batch_args = {
        'chart': args.chart,
        'numeric_index': args.index,
        'env': args.env,
        'num_cycles': args.num_cycles,
        'num_simulations': args.num_simulations,
        'systems': args.systems,
        'simulations': args.simulations,
        'agents': args.agents,
        'scale': args.scale,
        'base': args.base,
        'ext': args.ext
    }

    return mode, simulation_args, batch_args


def simulator_module(identifier, environments: str, debug: bool, parallel: bool, parameters: dict, data_path: str, output_path: str, plot_path: str):

    def ask_identifier(folder: str):
        print('[WARNING] - This directory already exists ({}), do you want to proceed anyway and overwrite its contents? [Y/N]'.format(folder), file=sys.stderr)

        while True:
            proceed = input('Your choice: ').upper()
            if proceed in ['Y', 'N']:
                if proceed == 'N':

                    print('[INFO] - Ok, try with another unique identifier.', file=sys.stderr)
                    exit(1)

                else:

                    break

            else:

                print('[ERROR] - You must select Y (yes) or N (no).', file=sys.stderr)

    if parallel:

        try:
            n_cpus = int(os.environ["SLURM_JOB_CPUS_PER_NODE"])
        except KeyError:
            n_cpus = cpu_count()

    else:

        # Serial execution
        n_cpus = 1

    if environments == 'all':

        environments = [s for s in os.listdir(data_path) if os.path.isdir(os.path.join(data_path, s))]

    else:

        print(environments)
        environments = [str(x.strip()) for x in environments.split(',')]

    # Useful print
    print('INITIALIZATION -------------------------------------')
    print('Selected environments: {}'.format(environments))
    print('Systems identifier: {}'.format(identifier))
    if parameters['pde'] == 1:
        print('Simulation PDE: ACTIVE')
    print('----------------------------------------------------\n')

    for environment in environments:

        # Gather environment path
        environment_path = os.path.join(data_path, environment)

        if not os.path.exists(environment_path):
            print("[{}] not found. Skipping.\n".format(environment), file=sys.stderr)
            continue

        start = datetime.now()

        # Gather all the system configuration from the current environment
        systems = []
        for root, dirs, files in os.walk(environment_path):
            for file in files:
                if file.endswith('.dat'):
                    name, _ = os.path.splitext(file)
                    systems.append([name, file])

        for system, file in sorted(systems):

            print('[{}] {} simulation in progress ...\n'.format(environment, system))

            # Create the folder to store the current environment simulation result
            if identifier is None:

                output = os.path.join(output_path, environment, system)
                plot = os.path.join(plot_path, environment, system)

            else:

                output = os.path.join(output_path, environment, system + '_' + identifier)
                plot = os.path.join(plot_path, environment, system + '_' + identifier)

            if not os.path.exists(output):
                os.makedirs(output)
            else:
                ask_identifier(output)

            if not os.path.exists(plot):
                os.makedirs(plot)
            else:
                ask_identifier(plot)

            # Retrieve current system parameters and update environment simulation
            system_parameters = ParameterHandler(os.path.join(environment_path, file)).read(separator=':')
            parameters.update(system_parameters)

            check_system_parameters(parameters=parameters)

            # Collect simulation arguments
            args = [[i, output, plot, debug, parameters] for i in range(1, parameters['num_sim'] + 1)]

            # Perform N simulation of the same system
            with Pool(processes=n_cpus) as pool:
                results = pool.starmap(f, args)

        end = datetime.now()
        print("Total execution time : {}".format(end - start))


def check_system_parameters(parameters: dict):

    # Parameter value check
    print('\nChecking input parameters ...')

    # Check PDL+ and PDL- probability
    sum_pdlm = 0.
    sum_pdlp = 0.

    ##############################################################################

    # Checking that this subset of parameters is a list

    if isinstance(parameters['inject_cure'], int):
        parameters['inject_cure'] = [parameters['inject_cure']]

    if isinstance(parameters['drug_duration'], int):
        parameters['drug_duration'] = [parameters['drug_duration']]

    ##############################################################################

    for parameter, value in parameters.items():

        if isinstance(value, (int, float)):

            if value >= 0:
                print('[PASS] {} : {}'.format(parameter, value))

            else:
                print('[HALT] {} : {}'.format(parameter, value), file=sys.stderr)
                print('Negative value encountered')
                exit(-1)

        elif isinstance(value, (tuple, list, str)):

            if len(value) > 0:
                print('[PASS] {} : {}'.format(parameter, value))

            else:
                print('[HALT] {} : {}'.format(parameter, value), file=sys.stderr)
                print('Empty value encountered')
                exit(-1)

        if parameter in ['tum_newantigen_rate', 'tum_adjchange_rate', 'tum_pdlp_mut', 'tum_necrchange_rate', 'tum_duplchange_rate']:
            sum_pdlm += value

        if parameter in ['tum_newantigen_rate', 'tum_adjchange_rate', 'tum_pdlm_mut', 'tum_necrchange_rate', 'tum_duplchange_rate']:
            sum_pdlp += value

        if sum_pdlm > 1:
            print('[HALT]', file=sys.stderr)
            print('PDL- probability sum to {}. Check the following parameters {}, their sum should not be greater '
                  'than one'.format(sum_pdlm, ['tum_newantigen_rate', 'tum_adjchange_rate', 'tum_pdlp_mut',
                                               'tum_necrchange_rate', 'tum_duplchange_rate']), file=sys.stderr)
            exit(-1)

        if sum_pdlp > 1:
            print('[HALT]', file=sys.stderr)
            print('PDL+ probability sum to {}. Check the following parameters {}, their sum should not be greater '
                  'than one'.format(sum_pdlp, ['tum_newantigen_rate', 'tum_adjchange_rate', 'tum_pdlm_mut',
                                               'tum_necrchange_rate', 'tum_duplchange_rate']), file=sys.stderr)
            exit(-1)

        if parameter in ['inject_cure', 'drug_duration']:

            good_duration = None

            if len(parameters['drug_duration']) != len(parameters['inject_cure']):

                print('[HALT] drug_duration : {} - drug_duration and inject_cure MUST have the same length'.format(parameters['drug_duration']), file=sys.stderr)
                exit(-1)

            else:

                drug_delivery = parameters['inject_cure'] + [int(parameters['num_cycles'] / (24 / parameters['num_hours']))]
                good_duration = [drug_delivery[n] - drug_delivery[n - 1] for n in range(1, len(drug_delivery))]

            for i in range(0, len(parameters['drug_duration'])):

                if parameters['drug_duration'][i] > good_duration[i]:
                    print('[HALT]', file=sys.stderr, end=' ')
                    print('Overlapping drug schedule it is not allowed! Please control parameter drug_duration or inject_cure.', file=sys.stderr)
                    exit(-1)

    print('OK')


def f(s, output, plot, debug, parameters):

    print('\nSimulation #{}'.format(s))
    s_start = datetime.now()
    simulation = Simulator(seed=s, debug=debug, parameters=parameters,
                           output=os.path.join(output, '_sim_num_{}_'.format(s)), plot=plot)
    simulation.simulate()

    s_end = datetime.now()

    print("\n\tExtermination obtained in : {}".format(s_end - s_start))
    print("\n-------------------------------------------------------------------\n")


def plotter_module(parameters: dict, output_path: str, plot_path: str):

    def ask_parameter(question: str, lower_bound: int, upper_bound: int = None):

        print(question)

        answer = None
        wrong = True

        while wrong:

            try:
                answer = int(input('Your choice: ').lower())

            except ValueError:

                print("[ERROR] - You must provide an integer value.", file=sys.stderr)
                continue

            if upper_bound is not None:

                if lower_bound < answer <= upper_bound:

                    wrong = False

                else:

                    print('[ERROR] - You must provide a value between {} and {}.'.format(lower_bound, upper_bound), file=sys.stderr)

            else:

                if lower_bound < answer:

                    wrong = False

                else:

                    print('[ERROR] - You must provide a value greater than {}.'.format(lower_bound), file=sys.stderr)

        return answer

    def ask_question(value_name: str, options: list):

        print('\nSelect {}:'.format(value_name))
        for i, option in enumerate(options):
            print('{} - {}'.format(i, option))

        answer = None
        question = True

        while question:
            option = input('\nYour choice : ').lower()
            question, answer = sanity_check(option, options + ['exit', 'quit', 'e', 'q'])

        return answer

    def sanity_check(selection: str, options: list):

        skip = False

        # Check user selection
        if selection.isdigit():

            try:

                selection = options[int(selection)]
                print('Selection: {}'.format(selection))

            except IndexError:

                print("[ERROR] - Wrong index provided.", file=sys.stderr)
                skip = True

        else:

            if selection.lower() in options:

                print('Selection: {}'.format(selection))

            else:

                print("[WARNING] - Bad choice, unable to find the value.", file=sys.stderr)
                skip = True

        return skip, selection

    # Start menu
    while True:

        environments = sorted([s for s in os.listdir(output_path) if os.path.isdir(os.path.join(output_path, s))])

        env = ask_question(value_name='environment', options=environments)

        if env in ['exit', 'quit', 'e', 'q']:

            break

        else:

            plot = os.path.join(plot_path, env)
            environment = os.path.join(output_path, env)

            if not os.path.exists(plot):
                os.makedirs(plot)

        systems = sorted([s for s in os.listdir(environment) if os.path.isdir(os.path.join(environment, s))])

        max_simulations = []
        for system in systems:
            max_simulations.append(len(sorted([s for s in os.listdir(os.path.join(environment, system)) if 'antigens' in s])))

        max_simulations = max(max_simulations)

        num_cycles = ask_parameter(question='\nHow many simulation cycles do you want to take into account?', lower_bound=1)
        num_simulations = ask_parameter(question='\nHow many simulations do you want to take into account?', lower_bound=1, upper_bound=max_simulations)

        num_instances = int(num_cycles * parameters['num_hours'] / (24 * parameters['day_save'])) + 1

        # Generate instance of Plotter
        plotter = Plotter(parameters=parameters, environment=environment, systems=systems,
                          num_instances=num_instances, num_simulations=num_simulations, plot=plot)

        # Load all the data inside directory
        plotter.load()

        charts = ['antigens', 'single', 'multiple', 'agent-comparison', 'frames', 'gif']
        metrics = ['relative', 'absolute']
        numeric_indices = ['mean_std', 'median']

        # Chart selection
        while True:

            chart = ask_question(value_name='chart', options=charts)

            # Proceed with plot
            if chart in ['exit', 'quit', 'e', 'q']:
                break

            elif chart == 'gif':

                plotter.generate_gif(systems=systems)

            elif chart == 'antigens':

                plotter.plot_antigens(systems=systems)

            elif chart == 'frames':

                plotter.plot_frames(systems=systems)

            elif chart in ['single', 'multiple', 'agent-comparison']:

                metric = ask_question(value_name='metric', options=metrics)
                numeric_index = ask_question(value_name='numeric index', options=numeric_indices)

                if chart == 'single':

                    plotter.plot_system(systems=systems, metric=metric, numeric_index=numeric_index)

                elif chart == 'multiple':

                    plotter.plot_systems(systems=systems, metric=metric, numeric_index=numeric_index)

                elif chart == 'agent-comparison':

                    plotter.plot_agent_comparison(systems=systems, metric=metric)


def batch_module(arguments: dict, parameters: dict, output_path: str, plot_path: str):

    # Check kind of plot
    chart = arguments['chart']                          # multiple/single/agent-comparison/antigens
    numeric_index = arguments['numeric_index']          # mean/median
    env = arguments['env']
    num_cycles = arguments['num_cycles']
    num_simulations = arguments['num_simulations']
    systems_data = arguments['systems']
    simulations_data = arguments['simulations']
    agents_data = arguments['agents']

    scale_data = arguments['scale']                     # base or log

    base_data = 'log2'
    if scale_data == 'log':
        base_data = arguments['base']

    extension = arguments['ext']

    # Generate useful paths
    environment = os.path.join(output_path, env)
    plot = os.path.join(plot_path, env)

    num_instances = int(num_cycles * parameters['num_hours'] / (24 * parameters['day_save'])) + 1

    systems = sorted([s for s in os.listdir(environment) if os.path.isdir(os.path.join(environment, s))])

    plotter = Plotter(parameters=parameters, environment=environment, systems=systems,
                      num_instances=num_instances, num_simulations=num_simulations,
                      plot=plot, interactive=False)

    # Load all the data inside directory
    plotter.load()

    if chart == 'single':

        print('Generating single system charts ...\n')

        systems_data = plotter.select_systems(systems=systems, data=systems_data)

        print()

        for system_data in systems_data:

            plotter.plot_system(systems=systems, metric='absolute', numeric_index=numeric_index,
                                system_data=system_data, agents_data=agents_data,
                                scale_data=scale_data, base_data=base_data, extension=extension)

            print('-' * 50)

    elif chart == 'multiple':

        print('Generating multiple systems chart ...\n')

        plotter.plot_systems(systems=systems, metric='absolute', numeric_index=numeric_index,
                             systems_data=systems_data, agents_data=agents_data,
                             scale_data=scale_data, base_data=base_data, extension=extension)

    elif chart == 'agent-comparison':

        print('Generating agent-comparison charts ...\n')
        plotter.plot_agent_comparison(systems=systems, metric='absolute',
                                      systems_data=systems_data, agents_data=agents_data,
                                      extension=extension)

    elif chart == 'antigens':

        print('Generating antigens charts ...\n')

        systems_data = plotter.select_systems(systems=systems, data=systems_data)
        simulations_data = plotter.select_simulations(data=simulations_data)

        print()

        for system_data in systems_data:

            for simulation_data in simulations_data:

                plotter.plot_antigens(systems=systems,
                                      systems_data=system_data, simulation_data=simulation_data,
                                      extension=extension)

                print('-' * 50)

            print('-' * 50)

    elif chart == 'gif':

        print('Generating spatial animation ...\n')

        systems_data = plotter.select_systems(systems=systems, data=systems_data)
        simulations_data = plotter.select_simulations(data=simulations_data)

        print()

        for system_data in systems_data:

            for simulation_data in simulations_data:

                plotter.generate_gif(systems=systems,
                                     systems_data=system_data, simulation_data=simulation_data)

                print('-' * 50)

            print('-' * 50)

    elif chart == 'frames':

        print('Generating spatial frames ...\n')

        systems_data = plotter.select_systems(systems=systems, data=systems_data)
        simulations_data = plotter.select_simulations(data=simulations_data)

        print()

        for system_data in systems_data:

            for simulation_data in simulations_data:

                plotter.plot_frames(systems=systems,
                                    systems_data=system_data, simulation_data=simulation_data,
                                    extension=extension)

                print('-' * 50)

            print('-' * 50)


if __name__ == '__main__':

    # Retrieve in/out path
    data_folder = os.path.join(os.getcwd(), 'data')
    output_folder = os.path.join(os.getcwd(), 'output')
    plot_folder = os.path.join(os.getcwd(), 'plot')

    usage, simulator_arguments, batch_arguments = parse_command_line()

    # Extract possible usage
    do_simulation, do_plot_interactively, do_plot_batch = usage

    # Retrieve environment parameters
    simulation_parameters = ParameterHandler(os.path.join(data_folder, 'setup.sim')).read(separator=':')

    if do_simulation:

        simulation_id = simulator_arguments['id']
        simulation_environments = simulator_arguments['environments']
        simulation_debug = simulator_arguments['debug']
        simulation_execution = simulator_arguments['parallel']

        simulator_module(identifier=simulation_id, environments=simulation_environments,
                         debug=simulation_debug, parallel=simulation_execution,
                         parameters=simulation_parameters, data_path=data_folder,
                         output_path=output_folder, plot_path=plot_folder)

    elif do_plot_interactively:

        print('*'*49 + '\n       Welcome to MAST interactive plotter\n' + '*'*49)
        plotter_module(parameters=simulation_parameters,
                       output_path=output_folder, plot_path=plot_folder)

    elif do_plot_batch:

        print('*'*49 + '\n            Welcome to MAST plotter\n' + '*'*49)

        batch_module(arguments=batch_arguments, parameters=simulation_parameters,
                     output_path=output_folder, plot_path=plot_folder)
