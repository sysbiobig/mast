import copy
import sys
import numpy as np


class Grid:
    """Class that instantiate a grid."""

    def __init__(self, width, height, value=0, grid_type=float):
        """Grid initializer

        Parameters
        ----------
        width : int
            Width of the grid
        height : int
            Height of the grid
        value : str | int
            Used to initialize each cell of the grid (default is 0)
        grid_type : Any
            Used to have each cell with the same data-type (default is float)
        """

        self._type = grid_type
        self._initializer = value

        if width < 0 or height < 0:
            raise ValueError
        else:
            self._w = width
            self._h = height

        self._grid = np.full((self._h, self._w), fill_value=self._initializer, dtype=self._type)

    def __str__(self):
        """Print the grid on standard output as debug
        """

        s = ""
        for i in range(self._h):
            for j in range(self._w):
                s += " " + str(self._grid[i, j])
            s += "\n"
        return s

    def __neighbour_lims__(self, i, j, function, v):
        """
        TODO : write some documentation for this function (not clear)
        """

        if i > 0:

            if j > 1:
                v = function(i - 1, j - 1, v)

            v = function(i - 1, j, v)

            if j < (self._w - 2):
                v = function(i - 1, j + 1, v)

        if j > 1:
            v = function(i, j - 1, v)

        v = function(i, j, v)

        if j < (self._w - 2):
            v = function(i, j + 1, v)

        if i < self._h - 1:

            if j > 1:
                v = function(i + 1, j - 1, v)

            v = function(i + 1, j, v)

            if j < (self._w - 2):
                v = function(i + 1, j + 1, v)

        return v

    def find_cells(self, i, j, cells):
        """Search in the adjacent cells (i, j) types contained in the list cells

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        :type cells : list | str
        :param cells : List of cell to find
        """

        matches = []

        if i > 0:

            if j > 1 and self.get_value(i - 1, j - 1) in cells:
                matches.append((i - 1, j - 1))

            if self.get_value(i - 1, j) in cells:
                matches.append((i - 1, j))

            if j < self._h - 2 and self.get_value(i - 1, j + 1) in cells:
                matches.append((i - 1, j + 1))

        if i < self._w - 1:

            if j > 1 and self.get_value(i + 1, j - 1) in cells:
                matches.append((i + 1, j - 1))

            if self.get_value(i + 1, j) in cells:
                matches.append((i + 1, j))

            if j < self._h - 2 and self.get_value(i + 1, j + 1) in cells:
                matches.append((i + 1, j + 1))

        if j > 1 and self.get_value(i, j - 1) in cells:
            matches.append((i, j - 1))

        if j < self._h - 2 and self.get_value(i, j + 1) in cells:
            matches.append((i, j + 1))

        return matches

    def find_move_probs(self, cell_type, i, j, cells, max_radius):
        """ Search in a MAX radius neighborhood with respect to position (i, j), cells that
        match one of the cell types and returns a tuple with adjuvanticity coordinates.

        TODO : finish documentation of this function
        """

        set_i = set(range(max(0, j - max_radius), j)).union(set(range(j, min(self._h - 1, j + 1 + max_radius))))
        set_j = set(range(max(0, i - max_radius), i)).union(set(range(i, min(self._w - 1, i + 1 + max_radius))))
        pr = []
        dirsadj = []
        p = 0.0

        for si in set_i:
            for sj in set_j:
                try:
                    p = self.get_value(si, sj)
                except ValueError:
                    print(self._w, "\t", self._h, "\t", si, "\t", sj)

                #  add 1 so that random.choices() can work correctly (if I have all 0 it becomes deterministic...)
                p = p + 1.0
                if (sj != i and si != j) and (cell_type.get_value(si, sj) in cells):
                    dirsadj.append((si, sj))
                    pr.append(p)

        return dirsadj, pr

    def find_cells_cross_neigh(self, i, j, cells):
        """
        TODO : finish documentation of this function
        """

        matches = []

        if i != 0:

            if j != 0 and self.get_value(i - 1, j - 1) in cells:
                matches.append((i - 1, j - 1))

            if j != self._h - 1 and self.get_value(i - 1, j + 1) in cells:
                matches.append((i - 1, j + 1))

        if i != self._w - 1:

            if j != 0 and self.get_value(i + 1, j - 1) in cells:
                matches.append((i + 1, j - 1))

            if j != self._h - 1 and self.get_value(i + 1, j + 1) in cells:
                matches.append((i + 1, j + 1))

        return matches

    def find_cells_antigen_specific(self, i, j, cells, antigensgrid, antigendictionary, ctlantigens):
        """
        TODO : finish documentation of this function
        """
        matches = self.find_cells(i, j, cells)

        targets = []
        for target in matches:
            try:
                antigensgrid.get_value(target[0], target[1]) != 0
            except:
                print("?", target)

            if antigensgrid.get_value(target[0], target[1]) != 0:
                target_antigens = set(antigendictionary[antigensgrid.get_value(target[0], target[1])][0])
                # print("target",targetantigens)
                # print("ctl", ctlantigens)

                if len(target_antigens.intersection(ctlantigens)) > 0:
                    targets.append(target)  # match da tenere

        # print (targets)
        return targets

    def clear_all(self):
        """Reset all value inside the grid."""

        self._grid = np.full((self._h, self._w), fill_value=self._initializer, dtype=self._type)

    def clear(self, i, j):
        """Reset value inside cell in position (i, j)

        :type i: int
        :param i: Row position
        :type j: int
        :param j: Col position
        """
        self._grid[i, j] = self._initializer

    @property
    def width(self):
        """Getter of property _w"""

        return self._w

    @width.setter
    def width(self, value):
        """Setter of property _w

        :type value : int
        """

        try:

            assert (value > 0), "You can't provide a negative width"

        except AssertionError as e:
            print(e, file=sys.stderr)
            exit(1)

        self._w = value

    @property
    def height(self):
        """Getter of property _h"""

        return self._h

    @height.setter
    def height(self, value):
        """Setter of property _h

        :type value : int
        """

        try:

            assert (value > 0), "You can't provide a negative height"

        except AssertionError as e:
            print(e, file=sys.stderr)
            exit(1)

        self._h = value

    def get_grid(self):
        """Getter of property _grid"""

        return self._grid

    def set_grid(self, value):
        """Setter of property _grid

        :type value : ndarray
        """

        self._grid = value

    def get_value(self, i, j):
        """Getter of property _grid[i, j]

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        """

        return self._grid[i][j]

    def set_value(self, i, j, v):
        """Setter of property _grid[i, j]

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        :type v : str | int | float
        :param v : Value to assign
        """

        self._grid[i][j] = v  # why is it wrong?

    def set_value_neigh(self, i, j, v):
        """Set the value of the neighborhood of _grid[i, j]

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        :type v : str | int | float
        :param v : Value to assign
        """

        def func(ii, jj, val):
            self._grid[ii, jj] = val
            return val

        self.__neighbour_lims__(function=func, i=i, j=j, v=v)

    def get_value_neigh(self, i, j):
        """Get the value of the neighborhood of _grid[i, j]

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        """

        return self.__neighbour_lims__(function=lambda ii, jj, val: val + self._grid[ii, jj], i=i, j=j, v=0)

    def get_list_neigh(self, i, j):
        """Get the list of the neighborhood of _grid[i, j]

        :type i : int
        :param i : Row position
        :type j : int
        :param j : Col position
        """

        v = np.full((3, 3), fill_value=self._initializer, dtype=self._type)

        v[1, 1] = self._grid[j, i]
        if j > 1:
            v[0, 1] = self._grid[j - 1, i]
        if j < (self._h - 3):
            v[2, 1] = self._grid[j + 1, i]
        if i > 0:
            v[1, 0] = self._grid[j, i - 1]
            if j > 1:
                v[0, 0] = self._grid[j - 1, i - 1]
            if j < (self._h - 2):
                v[2, 0] = self._grid[j + 1, i - 1]
        if i < self._w - 1:
            v[1, 2] = self._grid[j, i + 1]
            if j > 1:
                v[0, 2] = self._grid[j - 1, i + 1]
            if j < (self._h - 2):
                v[2, 2] = self._grid[j + 1, i + 1]
        return v
