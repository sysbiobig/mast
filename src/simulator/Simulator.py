import copy
import math
import random
import sys


import matplotlib
import numpy as np
import seaborn as sns
import matplotlib.animation as animation
import matplotlib.gridspec as gs
import matplotlib.pyplot as plt

from matplotlib.colors import LinearSegmentedColormap

from datetime import datetime

from src.handler import CSVHandler
from . import *
from .Grid import Grid
from .PyPDELab.classes.fem_module import mass_class as PyPDELab_mass
from .PyPDELab.classes.fem_module import stiffness_class as PyPDELab_stiffness
from .PyPDELab.classes.mesh_module import mesh_class as PyPDELab_mesh
from .PyPDELab.classes.meshio_module import dualsave as PyPDELab_save
from .PyPDELab.util.paraview_module import XMF_XML_Serial_Unstructured as PyPDELab_XMF
from .PyPDELab.util.quadmesh_module import quad2tri as PyPDELab_quad2tri
from .PyPDELab.util.quadmesh_module import quadgen2d as PyPDELab_quadgen2d
from .parametri import *


class Simulator:
    """Class that perform simulations."""

    def __init__(self, seed, parameters, output, plot, debug):
        """Simulator initializer

        Parameters
        ----------
        parameters : dict
            Dictionary containing all parameters of the current simulation
        output : str
            Path defining where the results will be stored
        """

        self._id = seed

        random.seed(self._id)

        # Set the output file
        self._fid_position = CSVHandler(path=output + POSITIONS_FILENAME, mode='w')
        self._fid_antigens = CSVHandler(path=output + ANTIGENS_FILENAME, mode='w')
        self._fid_timerecs = CSVHandler(path=output + TIMEREC_FILENAME, mode='w')

        self._plot_path = plot

        self._debug = debug

        self._doFileRecord = DOFILERECORD
        self._status = None

        # Parameters
        self._parameters = parameters

        self._pre_drug_ctl_kill_pdlp = self._parameters['ctl_kill_pdlp']
        self._pre_drug_ctl_recruit = self._parameters['ctl_reclut_par']

        self._w = parameters['width']       # simulation width
        self._h = parameters['height']      # simulation height

        # Generate the coordinates of each grid position
        self._coordinates = [(i, j) for i in range(self._w) for j in range(self._h)]

        self._x = parameters['tum_x_coord']
        self._y = parameters['tum_y_coord']
        self._r = parameters['tum_radius']

        self._cycles = 0  # cycles of simulation executed inside the saving loops
        self._days = 0  # days of simulation

        self._nextStroma = []  # list of cells that must become 'Stroma' on the next cycle
        self._nextTreg = []  # list of cells that must become 'Treg' in the next cycle

        # dictionary representing the antigens submitted to the lymph node (T cell maturation time is 24 hours)
        self._lymphNode = {
            1: [],  # 6  hours earlier
            2: [],  # 12 hours earlier
            3: [],  # 18 hours earlier
            4: [],  # 24 hours earlier
            5: [],  # 30 hours earlier
            6: [],  # 36 hours earlier
            7: [],  # 42 hours earlier
            8: []   # 48 hours earlier
        }

        # dictionary representing the number of stimulus received by the lymph node to activate T cells
        self._lymphNodeStimulus = {
            1: 0,  # 6  hours earlier
            2: 0,  # 12 hours earlier
            3: 0,  # 18 hours earlier
            4: 0,  # 24 hours earlier
            5: 0,  # 30 hours earlier
            6: 0,  # 36 hours earlier
            7: 0,  # 42 hours earlier
            8: 0   # 48 hours earlier
        }

        # dictionary of possible antigens (each symbol is an antigen)
        # it populates as the simulation proceeds
        self._antigenDict = {}
        for i in range(len(ANTIGENLIST)):
            self._antigenDict[i + 1] = list(ANTIGENLIST[i])

        self._antigenDict.update({(len(self._antigenDict) + 1): ['noadj']})  # used to simulate specific mutation effect

        # for i, mut in self._antigenDict.items():
        #     print("{} - {}".format(i, mut))

        # Ask what does it mean "try with enumerate"
        # GRID objects that store different aspects pf the simulation

        # store the cell type
        self._cellType = Grid(self._w, self._h, value='empty', grid_type='<U20')

        # stores which cells have already acted
        self._acted = Grid(self._w, self._h, grid_type=np.bool_)

        # stores how many more cells a CTL can kill before dying
        self._ctlKillsLeft = Grid(self._w, self._h, grid_type=np.uint16)

        # stores how many cycles of simulation (6h) have been carried out
        self._numCycles = Grid(self._w, self._h, grid_type=np.uint16)

        # stores 1 if there are factors that attract IS cells
        self._adjuvanticity = Grid(self._w, self._h, grid_type=np.uint8)

        # stores an index (key of the antigen dictionary), that allows to understand which are the antigens:
        # characteristic of a tumor agent
        # recognizable  by a CTL agent
        self._antigens = Grid(self._w, self._h, grid_type=np.uint16)

        # stores the duplication parameter
        self._thetaDupl = Grid(self._w, self._h, grid_type=np.float32)

        # stores the necrosis parameter
        self._thetaNecr = Grid(self._w, self._h, grid_type=np.float32)

        # stores the CTL's tumor neighbors, is used to calculate the infiltration index
        self._killableNeigh = Grid(self._w, self._h, grid_type=np.float32)

        # stores the glucose concentration
        self._N = Grid(self._w, self._h, grid_type=np.float32)

        # stores the oxygen concentration
        self._M = Grid(self._w, self._h, grid_type=np.float32)

        # non-tumor agent consumptions
        self._nCons = parameters['ncons']
        self._mCons = parameters['mcons']

        # tumor agent consumptions
        self._nConsTumor = parameters['tum_ncons']
        self._mConsTumor = parameters['tum_mcons']

        # Setup vessel glucose and oxygen values
        for j in range(self._w):
            self._N.set_value(0, j, 1)  # upper row
            self._N.set_value(self._h - 1, j, 1)  # lower row

            self._M.set_value(0, j, 1)  # upper row
            self._M.set_value(self._h - 1, j, 1)  # lower row

        # Setup non-vessel glucose and oxygen values when PDE are inactive
        if parameters['pde'] == 0:
            for i in range(1, self._h - 1):
                for j in range(self._w):
                    self._M.set_value(i, j, 0.7)
                    self._N.set_value(i, j, 0.7)

        # Initialize agents' dictionary
        #   agent: [class, cardinality, color, conversion str -> int]

        self._agents = {
            'empty': [EmptyAgent(self), self._w * (self._h - 2), 'white', 0],  # 0 'empty'
            'tumor': [TumorAgent(self), 0, 'black', 1],  # 1 'tumor'
            'blood': [BloodAgent(self), 2 * self._w, 'pink', 2],  # 2 'blood'
            'necro': [NecroticAgent(self), 0, 'gray', 3],  # 3 'necro'
            'n_killer': [NKAgent(self), 0, 'blue', 4],  # 4 'n_killer'
            'ctl': [CTLAgent(self), 0, 'red', 5],  # 5 'ctl'
            'pdlp': [ITumorAgent(self), 0, 'cyan', 6],  # 6 'pdlp'
            'dc': [DCAgent(self), 0, 'green', 7],  # 7 'dc'
            'stroma': [StromaAgent(self), 0, 'yellow', 8],  # 8 'stroma'
            'treg': [TregAgent(self), 0, 'orange', 9]  # 9 'treg'
        }

        self._simulation_statuses = {
            'tissue': [],
            'glu': [],
            'oxy': [],
            'adj': []
        }

        self._temp = None
        self._doTheraphy = True

    def initialize(self):
        """Initialize the agents inside the simulation"""

        # Open output
        self._fid_position.open()
        self._fid_antigens.open()
        self._fid_timerecs.open()

        # Placing capillaries (obstacles)
        for j in range(self._w):
            self._cellType.set_value(0, j, 'blood')  # upper row
            self._cellType.set_value(self._h - 1, j, 'blood')  # lower row

        # Placing tumor cells in the simulation
        coord_x = int(self._x)
        coord_y = int(self._y)
        radius = int(self._r)

        if radius == 1:

            self._cellType.set_value_neigh(coord_x, coord_y, 'tumor')
            self._adjuvanticity.set_value(coord_x, coord_y, self._parameters['adj_max'])  # only central cell has the maximum adj
            self._antigens.set_value_neigh(coord_x, coord_y, TUM_ANTIGEN_KEY)
            self._thetaDupl.set_value_neigh(coord_x, coord_y, self._parameters['tum_dupl_par'])
            self._thetaNecr.set_value_neigh(coord_x, coord_y, self._parameters['tum_necr_par'])
            self._numCycles.set_value_neigh(coord_x, coord_y, 0)
            self._agents['empty'][1] -= 9
            self._agents['tumor'][1] += 9

        else:

            for i in range(coord_x - radius, coord_x + radius + 1):
                for j in range(coord_y - radius, coord_y + radius + 1):
                    self._cellType.set_value(i, j, 'tumor')
                    self._adjuvanticity.set_value(i, j, self._parameters['adj_max'])  # each cell has the maximum adj
                    self._antigens.set_value(i, j, TUM_ANTIGEN_KEY)
                    self._thetaDupl.set_value(i, j, self._parameters['tum_dupl_par'])
                    self._thetaNecr.set_value(i, j, self._parameters['tum_necr_par'])
                    self._numCycles.set_value(i, j, 0)
                    self._agents['empty'][1] -= 1
                    self._agents['tumor'][1] += 1

        # Placing dendritic cells (DC) until a certain bound (%) is reached
        dc = 0
        while dc < int(self._parameters['innate_perc'] * self._w * self._h):  # computing % bound

            i = random.randint(0, self._h - 1)
            j = random.randint(0, self._w - 1)

            # swap empty cell with dendritic cell
            if self._cellType.get_value(i, j) == 'empty':
                self._cellType.set_value(i, j, 'dc')
                self._agents['empty'][1] -= 1
                self._agents['dc'][1] += 1
                dc += 1

        # Placing natural killer cells (NK) until a certain bound (%) is reached
        nk = 0
        while nk < int(self._parameters['innate_perc'] * self._w * self._h):  # computing % bound

            i = random.randint(0, self._h - 1)
            j = random.randint(0, self._w - 1)

            # swap empty cell with dendritic cell
            if self._cellType.get_value(i, j) == 'empty':
                self._cellType.set_value(i, j, 'n_killer')
                self._agents['empty'][1] -= 1
                self._agents['n_killer'][1] += 1
                nk += 1

        if self._doFileRecord:
            self.save()

    def iterate(self):
        """Perform an iteration inside the simulation.
        """

        # Check if there are scheduled therapy and apply in the right day-slot
        if self._doTheraphy:
            self.schedule_therapy()

        # Solve PDE
        self.compute_nutrient_concentration()

        # Store tissue status
        if self._parameters['gif_generate'] == 1:
            self.gather_simulation_information()

        # Clear all previous action
        self._acted.clear_all()

        # Perform agent activation based on the random activation pattern
        self.agent_activation()

        # Reset cytokines parameters (adjuvanticity)
        for i in range(self._h):
            for j in range(self._w):
                aus = self._adjuvanticity.get_value(i, j) - 1
                # if aus>0: print("adj: ",i,j,aus)
                self._adjuvanticity.set_value(i, j, max(0, aus))

        # Update lymph node maturation
        for i in range(7, 0, -1):
            self._lymphNode[i + 1] = self._lymphNode[i]
            self._lymphNodeStimulus[i + 1] = self._lymphNodeStimulus[i]

        self._lymphNode[1] = []
        self._lymphNodeStimulus[1] = 0

        # Update completed cycles
        self._cycles += 1

        # Update progress bar
        if PROGRESS_BAR:

            saves_break = (24 * self._parameters['day_save']) / self._parameters['num_hours']
            percentage = self._cycles / saves_break

            if (self._agents['tumor'][1] + self._agents['pdlp'][1]) == 0:
                self._status = 'Extermination'
            elif percentage != 1:
                self._status = 'Halt'
            else:
                self._status = 'Done'

            progress_bar = '\r\tPercent: [{}] {}% {}'.format(
                "#" * int(round(30 * percentage)) + "_" * (30 - int(round(30 * percentage))),
                "%.2f" % (percentage * 100),
                self._status
            )

            sys.stdout.write(progress_bar)
            sys.stdout.flush()

        save_token = False

        # Update completed days
        if self._cycles % (24 / self._parameters['num_hours']) == 0:
            self._days += 1
            if self._days % self._parameters['day_save'] == 0:
                save_token = True

        # Saves the trend of the populations every 3 days
        if save_token & self._doFileRecord:
            self.save()
            self._cycles = 0

    def agent_activation(self):

        # Shuffle agents coordinates
        random.shuffle(self._coordinates)

        for i, j in self._coordinates:
            if not self._acted.get_value(i, j):
                self._agents[self._cellType.get_value(i, j)][0].step(i, j)

    def compute_nutrient_concentration(self):

        if self._cycles % self._parameters['pde_update'] == 0 and self._parameters['pde'] == 1:

            if self._parameters['ncons'] == self._parameters['mcons'] and self._parameters['tum_ncons'] == self._parameters['tum_mcons']:
                # ausM = self.diffuse_explicit(Dn=1.0E-3)
                ausM = self.diffuse_explicit(Dn=self._parameters['pde_diffusion'])
                ausN = ausM
            else:
                # Compute nutrient diffusion
                ausM = self.diffuse_explicit(Dn=self._parameters['pde_diffusion'], nutrient='oxygen')
                ausN = self.diffuse_explicit(Dn=self._parameters['pde_diffusion'], nutrient='glucose')

            # Check whether two consecutive nutrient diffusion are identical or not
            if self._cycles > 2:
                if np.array_equiv(ausM, self._temp) and self._debug:
                    print("\nCycles {} and {} share the same grid.".format(self._cycles - self._parameters['pde_update'], self._cycles))

            self._temp = ausM

            # Set nutrient diffusion
            self._M.set_grid(ausM)
            self._N.set_grid(ausN)

    def schedule_therapy(self):

        # Therapy deliver
        if len(self._parameters['inject_cure']) >= 1 and self._parameters['inject_cure'][0] != 0:

            idx = 0

            # Find the current schedule
            for i, schedules in enumerate(sorted(zip(self._parameters['inject_cure'], self._parameters['drug_duration']), key=lambda x: x[0])):

                schedule = schedules[0]
                duration = schedules[1]

                if schedule <= self._days < schedule + duration:
                    idx = i
                else:
                    continue

            # drug_selection = self._parameters['drug_selection'][idx]
            drug_deliver = self._parameters['inject_cure'][idx]
            drug_duration = self._parameters['drug_duration'][idx]
            drug_selection = 0

            # elif drug_selection == 1:
            #     self.deliver_chemo(drug_deliver, drug_duration)
            if drug_selection == 0:
                self.deliver_immuno(drug_deliver, drug_duration)
            else:
                print("[WARNING] - The suggested treatment will not be applied in the schedule #{} occurring on days {} - {}.\n".format(idx, drug_deliver, drug_deliver + drug_duration))

        else:

            print("\n[NOTICE] - No theraphy scheduled within this simulation!")
            self._doTheraphy = False

    def deliver_immuno(self, schedule, duration):

        if schedule <= self._days < schedule + duration:
            self._parameters['ctl_kill_pdlp'] = 1.5 * self._parameters['ctl_kill']
        else:
            self._parameters['ctl_kill_pdlp'] = self._pre_drug_ctl_kill_pdlp

    # def deliver_chemo(self, schedule, duration):
    #
    #     def find_chemotherapy_target(agents: list):
    #
    #         t = [(i, j) for i in range(self._w) for j in range(self._h) if self._cellType.get_value(i, j) in agents]
    #
    #         # Shuffle agents coordinates
    #         random.shuffle(t)
    #
    #         return t
    #
    #     if schedule <= self._days < schedule + duration:
    #
    #         targets = find_chemotherapy_target(['pdlp', 'tumor', 'stroma', 'ctl'])
    #
    #         targets_coordinates = int(0.25 * len(targets))
    #
    #         for idx in range(0, targets_coordinates):
    #
    #             i, j = targets[idx]
    #
    #             cell_type = self._cellType.get_value(i, j)
    #
    #             self._cellType.set_value(i, j, 'empty')
    #             self._agents['empty'][1] += 1
    #             self._agents[cell_type][1] -= 1

    def stop(self):
        """Stop simulation, and close all the opened file"""

        self._fid_position.close()
        self._fid_antigens.close()
        self._fid_timerecs.close()

    def gather_simulation_information(self):

        if not os.path.exists(self._plot_path):
            os.makedirs(self._plot_path)

        tissue = Grid(self._w, self._h, grid_type=np.float32)

        for i in range(self._w):
            for j in range(self._h):
                key = self._cellType.get_value(i, j)
                tissue.set_value(i, j, self._agents[key][3])  # extract the agent conversion str -> int

        self._simulation_statuses['tissue'].append(tissue.get_grid())
        self._simulation_statuses['glu'].append(self._N.get_grid())
        self._simulation_statuses['oxy'].append(self._M.get_grid())
        adj = copy.deepcopy(self._adjuvanticity.get_grid())
        self._simulation_statuses['adj'].append(adj)

    def animate_simulation(self):

        matplotlib.rcParams['animation.embed_limit'] = 2 ** 128

        if self._debug:

            print('\n\n\tGIF generation ...')

        else:

            print('\n\n\tGIF generation ...', end=' ')

        fig = plt.figure()
        grid = gs.GridSpec(nrows=3, ncols=4)

        ax0 = fig.add_subplot(grid[:, :3])   # Tissue
        ax1 = fig.add_subplot(grid[0, 3])    # Glucose
        ax2 = fig.add_subplot(grid[1, 3])    # Oxygen
        ax3 = fig.add_subplot(grid[2, 3])    # Adjuvanticity

        # Define custom color map
        cMap = []
        colors = [color[2] for color in self._agents.values()]
        values = [value[3] for value in self._agents.values()]
        for value, color in zip(values, colors):
            cMap.append((value / 9.0, color))

        colorMap = LinearSegmentedColormap.from_list("custom", cMap)

        # Define frame number
        frames = len(self._simulation_statuses['tissue'])

        def init():

            sns.heatmap(data=np.zeros((self._w, self._h)), cmap=colorMap, cbar=False, yticklabels=False, xticklabels=False, vmin=0, vmax=9, ax=ax0)
            ax0.set_title('Tissue')

            sns.heatmap(data=np.zeros((self._w, self._h)), cmap='Blues', cbar=True, yticklabels=False, xticklabels=False, vmin=0, vmax=1, ax=ax1)
            ax1.set_title('Glucose')

            sns.heatmap(data=np.zeros((self._w, self._h)), cmap='Reds', cbar=True, yticklabels=False, xticklabels=False, vmin=0, vmax=1, ax=ax2)
            ax2.set_title('Oxygen')

            sns.heatmap(data=np.zeros((self._w, self._h)), cmap='Oranges', cbar=True, yticklabels=False, xticklabels=False, vmin=0, vmax=self._parameters['adj_max'], ax=ax3)
            ax3.set_title('Adjuvanticity')

            fig.tight_layout()

        def update(i, adj_max):

            k = i + 1

            if k % 4 == 0:
                if self._debug:
                    print('\tLoading day {}'.format(math.floor(k/4)))

            ax0.cla()
            ax0.set_title('Tissue - Day {} / Cycle {}'.format(math.floor(k/4), k % 4))
            sns.heatmap(data=self._simulation_statuses['tissue'][i], cmap=colorMap, cbar=False, yticklabels=False, xticklabels=False, vmin=0, vmax=9, ax=ax0)

            ax1.cla()
            ax1.set_title('Glucose')
            sns.heatmap(data=self._simulation_statuses['glu'][i], cmap='Blues', cbar=False, yticklabels=False, xticklabels=False, vmin=0, vmax=1, ax=ax1)

            ax2.cla()
            ax2.set_title('Oxygen')
            sns.heatmap(data=self._simulation_statuses['oxy'][i], cmap='Reds', cbar=False, yticklabels=False, xticklabels=False, vmin=0, vmax=1, ax=ax2)

            ax3.cla()
            ax3.set_title('Adjuvanticity')
            sns.heatmap(data=self._simulation_statuses['adj'][i], cmap='Oranges', cbar=False, yticklabels=False, xticklabels=False, vmin=0, vmax=adj_max, ax=ax3)

            fig.tight_layout()

            return ax0, ax1, ax2, ax3,

        anim = animation.FuncAnimation(fig, update, init_func=init, frames=frames, fargs=(self._parameters['adj_max'],), repeat=False, interval=50)

        writer = animation.ImageMagickWriter(fps=self._parameters['gif_fps'], metadata=dict(artist='SysBioBig'), bitrate=18000)

        anim.save("{}/simulation_{}.gif".format(self._plot_path, self._id), writer=writer, dpi=self._parameters['gif_dpi'])

        fig.clear()
        plt.close()

        print('OK')

    def save(self):
        """Save the current state of the simulation"""

        positions = {}
        antigens = {}
        temp_antigens_list = {}
        curr_antigens_list = {}

        for cell_type in self._agents:

            if cell_type in ['blood', 'empty']:
                continue

            if cell_type in ['tumor', 'pdlp', 'ctl']:
                antigens[cell_type] = np.array([], dtype='<U64')
                temp_antigens_list[cell_type] = []
                curr_antigens_list[cell_type] = []

            positions[cell_type] = np.array([], dtype='uint16')

        # Initialization
        counter = {}
        old_dist = {}

        for cell_type in self._agents:
            counter[cell_type] = 0

        # Sanity check
        for i in range(self._h):
            for j in range(self._w):

                cell_type = self._cellType.get_value(i, j)

                counter[cell_type] += 1

        sum = 0
        for cell_type in self._agents:

            if counter[cell_type] == self._agents[cell_type][1]:
                if self._debug:
                    print('\t{:<10} : OK [{}]'.format(cell_type.upper(), self._agents[cell_type][1]))
            else:
                print('\t{} : DISCREPANCY'.format(cell_type.upper()))
                print('\tGrid count : {}'.format(self._agents[cell_type][1]))
                print('\tManually count : {}'.format(counter[cell_type]))

            sum += counter[cell_type]

        if self._debug:
            print('\n\tTOT : {}\n'.format(sum))

        for cell_type in positions:
            counter[cell_type] = 0
            old_dist[cell_type] = 0

        for i in range(self._h):
            for j in range(self._w):

                cell_type = self._cellType.get_value(i, j)

                if cell_type in ['empty', 'blood']:
                    continue

                elif cell_type in ['tumor', 'pdlp', 'ctl']:

                    # Warning: rare but there are tumors without antigen
                    if self._antigens.get_value(i, j) == 0:
                        curr_antigens_list[cell_type] = ['noantigens']

                    else:
                        # for h, v in self._antigenDict.items():
                        #     print("{} - {}".format(h, v))
                        # print("Antigen : {}\n\n".format(self._antigens.get_value(i, j)))
                        curr_antigens_list[cell_type] = self._antigenDict[self._antigens.get_value(i, j)]

                    # Save antigens with a symbol separator
                    antigens_string = ''
                    if curr_antigens_list[cell_type] != temp_antigens_list[cell_type]:
                        for element in curr_antigens_list[cell_type]:
                            antigens_string += element + '%'

                    antigens[cell_type] = np.append(antigens[cell_type], antigens_string)
                    temp_antigens_list[cell_type] = curr_antigens_list[cell_type]

                # Counts distance from previous cell
                old_dist[cell_type], new_dist = self.__count_distance__(old_dist[cell_type], i, j)
                positions[cell_type] = np.append(positions[cell_type], new_dist)

                counter[cell_type] += 1

        # Saves data positions
        for cell_element, list_dist in positions.items():
            # if len(list_dist) > 0:
            self._fid_position.write(list([cell_element]) + list(list_dist))

        # Saves antigens positions
        for cell_element, list_antigen in antigens.items():
            # if len(list_antigen) > 0:
            self._fid_antigens.write(list([cell_element]) + list(list_antigen))

        if self._days > 0 and self._status != 'Extermination':
            print('\n\tDay {} correctly saved'.format(self._days))

    def simulate(self):
        """Simulate the system"""

        self.initialize()

        exterminate = False

        iteration_time = []

        while not exterminate:

            i_start = datetime.now()
            self.iterate()
            i_finish = datetime.now()

            if self._days == self._parameters['num_cycles'] * self._parameters['num_hours'] / 24:
                exterminate = True
            elif (self._agents['tumor'][1] + self._agents['pdlp'][1]) == 0:
                exterminate = True

            iteration_time.append(i_finish - i_start)

        self._fid_timerecs.write(iteration_time)

        # Generate GIF
        if self._parameters['gif_generate'] == 1:
            self.animate_simulation()

        self.stop()

    def diffuse_explicit(self, Dn=1.0E-3, nutrient: str = 'oxygen'):
        # aggiunto Pier 23.01.2019
        ##    mesh = UnitSquareMesh(w, h)
        theverb = False
        QuadMesh = PyPDELab_quadgen2d("SquareGrid", self._w + 1, self._h + 1, verbose=theverb)
        # QuadMesh.list() #per stampare informazioni sulla mesh
        TriMesh = PyPDELab_quad2tri(QuadMesh, verbose=theverb)
        TriMesh.meshfix()
        # TriMesh.list() #per stampare informazioni sulla mesh
        del (QuadMesh)
        Krhs = PyPDELab_stiffness('K')
        Mrhs = PyPDELab_mass('M')

        def Dnfun(reg):
            siz = np.size(reg)
            p = np.ones(siz) * Dn
            return p

        def cpfun(reg, x, y):
            siz = np.size(reg)
            p = np.ones(siz)
            for k in range(siz):

                if nutrient == 'oxygen':
                    p[k] = self.getm_parconsum(x[k], y[k])
                elif nutrient == 'glucose':
                    p[k] = self.getn_parconsum(x[k], y[k])

            return p

        def selectallfun(reg):
            siz = np.size(reg)
            p = np.where(reg > -1, np.ones(siz), np.zeros(siz)) * 1
            return p

        ##    f = Expression("0.0", degree=2)
        def rhsfun(reg, x, y, lamb, elnods):
            siz = np.size(reg)
            p = np.ones(siz) * 0.0
            return p

        ##    u = TrialFunction(V)
        ##    v = TestFunction(V)
        ##    a = inner(Dn*grad(u), grad(v))*dx + cp * u * v * dx
        Krhs.assemble(TriMesh, Dnfun, selectallfun, quadorder=2, verbose=theverb)

        Mrhs.assemble(TriMesh, cpfun, selectallfun, quadorder=2, verbose=theverb)
        # pdb.set_trace()

        ##    L = -f*v*dx
        Krhs.addrhs(TriMesh, rhsfun, selectallfun, verbose=theverb)
        if theverb:
            Krhs.statistics()
            Mrhs.statistics()
        Krhs.matrix = Krhs.matrix + Mrhs.matrix
        if theverb:
            Krhs.statistics()

        ##    V = FunctionSpace(mesh, "Lagrange", 1, constrained_domain=PeriodicBoundary())
        ##class PeriodicBoundary(SubDomain):
        ##    def inside(self, x, on_boundary):
        ##        return bool(x[0] < DOLFIN_EPS and x[0] > -DOLFIN_EPS and on_boundary)
        ##    def map(self, x, y):
        ##        y[0] = x[0] - 1.0
        ##        y[1] = x[1]
        TriMesh.nodes.assignbc('abs(x)<1.0e-6', '1.0', verbose=theverb)
        master = np.where(TriMesh.nodes.icond)
        msort = np.argsort(TriMesh.nodes.n[1, master])
        master = master[0][msort[0]]
        nm = np.size(master)
        TriMesh.nodes.icond[:] = False
        TriMesh.nodes.assignbc('abs(x-1)<1.0e-6', '2.0', verbose=theverb)
        slave = np.where(TriMesh.nodes.icond)
        ssort = np.argsort(TriMesh.nodes.n[1, slave])
        slave = slave[0][ssort[0]]
        ns = np.size(slave)
        TriMesh.nodes.icond[:] = False
        Krhs.assignsymm(master, slave, verbose=theverb)

        ##class DirichletBoundary(SubDomain):
        ##    def inside(self, x, on_boundary):
        ##        return bool((x[1] < DOLFIN_EPS or x[1] > (1.0 - DOLFIN_EPS)) \
        ##                    and on_boundary)
        ##def boundary0(x):
        ##    return x[1] < DOLFIN_EPS
        ##def boundary1(x):
        ##    return x[1] > 1.0 - DOLFIN_EPS
        ##    u0 = Constant(1.0)
        ##    u1 = Constant(1.0)   #mettere a 0 se si vuole simulare senza vaso sanguigno in basso
        ##    bc0 = DirichletBC(V, u0, boundary0)
        ##    bc1 = DirichletBC(V, u1, boundary1)
        ##    bcs = [bc0, bc1]

        TriMesh.nodes.assignbc('abs(y)<1.0e-6', '1.0', verbose=theverb)
        TriMesh.nodes.assignbc('abs(y-1)<1.0e-6', '1.0', verbose=theverb)  # mettere a 0 se si vuole simulare senza vaso sanguigno in basso
        Krhs.assignbc(np.where(TriMesh.nodes.icond), np.where(TriMesh.nodes.icond), TriMesh.nodes.vcond, verbose=theverb)

        ##    u = Function(V)
        ##    solve(a == L, u, bcs)
        TriMesh.nodes.pot = Krhs.directsolve(verbose=theverb)
        u = TriMesh.nodes.pot

        ##    file = File("poisson.pvd")
        ##    file << u
        if (theverb):
            PyPDELab_mesh.fieldfrompot(TriMesh, selectallfun, 'grad')
            PyPDELab_save("tumor", TriMesh)
            writer = PyPDELab_XMF()
            writer.write('tumor', TriMesh)
        # fine aggiunto Pier 23.01.2019

        ##    newconc = u.compute_vertex_values()
        newconc = u

        W = self._w + 1
        newgrid = list()
        for i in range(self._h):
            internalgrid = list()
            for ind in range(i * W, (i + 1) * W - 1):
                res = (newconc[ind] + newconc[ind + 1] + newconc[ind + W] + newconc[ind + W + 1]) / 4
                internalgrid.append(res)
            newgrid.append(internalgrid)

        # print(newgrid[5])
        return (newgrid)

    def getn_parconsum(self, x, y):
        i = min(int(y * self._h), self._w - 1)
        j = min(int(x * self._h), self._h - 1)

        return self._agents[self._cellType.get_value(i, j)][0].get_n_consumption()

    def getm_parconsum(self, x, y):
        i = min(int(y * self._w), self._w - 1)
        j = min(int(x * self._h), self._h - 1)

        return self._agents[self._cellType.get_value(i, j)][0].get_m_consumption()

    def swap(self, i, j, u, v):
        """Swap position (i, j) and (u, v) in all grids.

        It is convenient for agents when they move, since it is not certain
        that the moving agent has access to all secondary parameters.

        :type i: int
        :param i: First agent row position
        :type j: int
        :param j: First agent col position
        :type u: int
        :param u: Second agent row position
        :type v: int
        :param v: Second agent col position
        """

        grids = [
            self._cellType,
            self._acted,
            self._ctlKillsLeft,
            self._numCycles,
            self._antigens,
            self._thetaDupl,
            self._thetaNecr
        ]

        for grid in grids:
            value = grid.get_value(i, j)
            grid.set_value(i, j, grid.get_value(u, v))
            grid.set_value(u, v, value)

    def find_antigen_list(self, mutation):
        """Find the antigen list inside the antigen dictionary.

        It is convenient for agents when they move, since it is not certain
        that the moving agent has access to all secondary parameters.

        :type mutation: list
        :param mutation: Mutation to find
        :return : The key of the dictionary associated to the mutation
        """

        if mutation in self._antigenDict.values():
            k = list(self._antigenDict.values()).index(mutation)
            return k
        else:
            return None

    def insert_antigen_list(self, mutation, i, j):
        """Insert the antigen list inside the antigen dictionary.

        If the mutation is found inside the antigen dictionary, update
        the antigen grid, otherwise update the dictionary with the new
        mutation and hereafter update the antigen grid.

        :type mutation: list
        :param mutation: Mutation to insert
        :type i: int
        :param i: Row position
        :type j: int
        :param j: Col position
        """

        found = self.find_antigen_list(mutation)

        if found is not None:

            # Mutation found, update the grid
            # print("Mutation {} found".format(mutation))
            self._antigens.set_value(i, j, found)
        else:
            # Mutation not found, adding inside the dictionary

            # print("Mutation {} not found. Adding it.".format(mutation))
            index = len(self._antigenDict) + 1
            self._antigenDict.update({index: mutation})
            # for k, v in self._antigenDict.items():
            #     print("## {} - {}".format(k, v))

            # Update the grid
            self._antigens.set_value(i, j, index)

    def __count_distance__(self, old, i, j):
        """Count the distance from the previous value starting from cell (0,0)

        :type old: int
        :param old: Old distance
        :type i: int
        :param i: Row position
        :type j: int
        :param j: Col position
        """

        abs_dist = i * self._w + j
        return abs_dist, abs_dist - old

    @property
    def width(self):
        """Getter of property _w"""

        return self._w

    @width.setter
    def width(self, value):
        """Setter of property _w

        :type value : int
        """

        self._w = value

    @property
    def height(self):
        """Getter of property _h"""

        return self._h

    @height.setter
    def height(self, value):
        """Setter of property _h

        :type value : int
        """

        self._h = value

    @property
    def days(self):
        """Getter of property _days"""

        return self._days

    @days.setter
    def days(self, value):
        """Setter of property _h

        :type value : int
        """

        self._days = value

    @property
    def agents(self):
        """Getter of property _agents"""

        return self._agents

    @agents.setter
    def agents(self, value):
        """Setter of property _agents

        :type value : dict
        """

        self._agents = value

    @property
    def parameters(self):
        """Getter of property _parameters"""

        return self._parameters

    @parameters.setter
    def parameters(self, value):
        """Setter of property _parameters

        :type value : dict
        """

        self._parameters = value

    @property
    def cell_type(self):
        """Getter of property _cellType"""

        return self._cellType

    @cell_type.setter
    def cell_type(self, grid):
        """Setter of property _parameters

        :type grid : Grid
        """

        self._cellType = grid

    @property
    def acted(self):
        """Getter of property _acted"""

        return self._acted

    @acted.setter
    def acted(self, grid):
        """Setter of property _acted

        :type grid : Grid
        """

        self._acted = grid

    @property
    def adjuvanticity(self):
        """Getter of property _adjuvanticity"""

        return self._adjuvanticity

    @adjuvanticity.setter
    def adjuvanticity(self, grid):
        """Setter of property _adjuvanticity

        :type grid : Grid
        """

        self._adjuvanticity = grid

    @property
    def antigens(self):
        """Getter of property _antigens"""

        return self._antigens

    @antigens.setter
    def antigens(self, grid):
        """Setter of property _antigens

        :type grid : Grid
        """

        self._antigens = grid

    @property
    def theta_duplication(self):
        """Getter of property _thetaDupl"""

        return self._thetaDupl

    @theta_duplication.setter
    def theta_duplication(self, grid):
        """Setter of property _thetaDupl

        :type grid : Grid
        """

        self._thetaDupl = grid

    @property
    def theta_necrosis(self):
        """Getter of property _thetaNecr"""

        return self._thetaNecr

    @theta_necrosis.setter
    def theta_necrosis(self, grid):
        """Setter of property _thetaNecr

        :type grid : Grid
        """

        self._thetaNecr = grid

    @property
    def num_cycles(self):
        """Getter of property _numCycles"""

        return self._numCycles

    @num_cycles.setter
    def num_cycles(self, grid):
        """Setter of property _numCycles

        :type grid : Grid
        """

        self._numCycles = grid

    @property
    def ctl_kills_left(self):
        """Getter of property _ctlKillsLeft"""

        return self._ctlKillsLeft

    @ctl_kills_left.setter
    def ctl_kills_left(self, grid):
        """Setter of property _ctlKillsLeft

        :type grid : Grid
        """

        self._ctlKillsLeft = grid

    @property
    def killable_neighbors(self):
        """Getter of property _killableNeigh"""

        return self._killableNeigh

    @killable_neighbors.setter
    def killable_neighbors(self, grid):
        """Setter of property _killableNeigh

        :type grid : Grid
        """

        self._killableNeigh = grid

    @property
    def glucose(self):
        """Getter of property _N"""

        return self._N

    @glucose.setter
    def glucose(self, grid):
        """Setter of property _N

        :type grid : Grid
        """

        self._N = grid

    @property
    def oxygen(self):
        """Getter of property _M"""

        return self._M

    @oxygen.setter
    def oxygen(self, grid):
        """Setter of property _M

        :type grid : Grid
        """

        self._M = grid

    @property
    def glucose_consumption(self):
        """Getter of property _nCons"""

        return self._nCons

    @property
    def oxygen_consumption(self):
        """Getter of property _mCons"""

        return self._mCons

    @property
    def tumor_glucose_consumption(self):
        """Getter of property _nCons"""

        return self._nConsTumor

    @property
    def tumor_oxygen_consumption(self):
        """Getter of property _mCons"""

        return self._mConsTumor

    @property
    def lymph_node(self):
        """Getter of property _lymphNode"""

        return self._lymphNode

    @lymph_node.setter
    def lymph_node(self, grid):
        """Setter of property _lymphNode

        :type grid : dict
        """

        self._lymphNode = grid

    @property
    def lymph_node_stimulus(self):
        """Getter of property _lymphNodeStimulus"""

        return self._lymphNodeStimulus

    @lymph_node_stimulus.setter
    def lymph_node_stimulus(self, value):
        """Setter of property _lymphNodeStimulus

        :type value : dict
        """

        self._lymphNodeStimulus = value

    @property
    def antigen_dictionary(self):
        """Getter of property _antigenDict"""

        return self._antigenDict

    @antigen_dictionary.setter
    def antigen_dictionary(self, value):
        """Setter of property _agents

        :type value : dict
        """

        self._antigenDict = value

    @property
    def next_stroma(self):
        """Getter of property _nextStroma"""

        return self._nextStroma

    @next_stroma.setter
    def next_stroma(self, value):
        """Setter of property _nextStroma

        :type value : list
        """

        self._nextStroma = value

    @property
    def next_treg(self):
        """Getter of property _nextTreg"""

        return self._nextTreg

    @next_treg.setter
    def next_treg(self, value):
        """Setter of property _nextTreg

        :type value : list
        """

        self._nextTreg = value
