from .Agent import ImmuneSystem as Agent


class NK(Agent):
    """NK is a class used to represent a natural killer system agent inside the sim"""

    def __init__(self, simulation):
        super().__init__(simulation)

    def attack(self, i, j, targets):
        win = super().attack(i, j, targets)

        # NK cells die after killing and induce an empty cell on the same position

        if win:
            self.replace_agent(i=i, j=j, new='empty', old='n_killer')
            self._acted.set_value(i, j, True)
