from ..Agent import Agent

from ..parametri import *
import random


class ImmuneSystem(Agent):
    """ImmuneSystem is a class used to represent an immune system agent inside the sim"""

    def __init__(self, simulation):
        """Agent initializer

        Parameters
        ----------
        simulation : Simulator
            It's an object useful to retrieve sim data
        """

        super().__init__(simulation)
        self._pkill = self._parameters['innate_kill']

    def step(self, i, j):
        """
        TODO : finish documentation of this function
        """

        targets = self._cellType.find_cells(i, j, KILLABLE)
        if len(targets) > 0:
            self.attack(i, j, targets)  # se ha cellule tumorali vicine e riconosciute attacca
        elif random.random() < self._parameters['p_die']:
            self.die(i, j)
        else:
            self.move(i, j)  # probabilità (piccola) di uscire dal dominio

    def attack(self, i, j, targets):
        """
        TODO : finish documentation of this function
        """

        # is a list of 2 elements: the coordinates of the chosen target
        toKill = random.choice(targets)

        # store the id of the target cell
        targetID = self._cellType.get_value(toKill[0], toKill[1])

        win = False
        if random.random() < self._pkill:
            win = True
            self.replace_agent(i=toKill[0], j=toKill[1], new='empty', old=targetID)

            # NK die after killing and induce an empty cell on the same cell.
            # DC instead do not die after "killing" and induce a CTL through the recall of cytokines both induce an
            # increase of adjuvanticity if the killed tumor cell was not mutated.
            self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_max'])

            if self._antigens.get_value(toKill[0], toKill[1]) != 0:
                tumant = list(self._antigenDict[self._antigens.get_value(toKill[0], toKill[1])])

                # stimulate the lymph node
                self._lymphNode[1] = self._lymphNode[1] + tumant
                self._lymphNodeStimulus[1] += 1
                if "noadj" in tumant:
                    self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

                # delete antigens from the dead cancer cell (update the antigen grid for the cancer cell that is dead)
                self._antigens.set_value(toKill[0], toKill[1], 0)

            self._acted.set_value(i, j, True)
            self._acted.set_value(toKill[0], toKill[1], True)

        else:

            self._acted.set_value(i, j, True)

        return win

    def move(self, i, j):
        """
        The SI cell moves in a self._parameters['maxmov'] size neighborhood relative to the initial position (i,j)
        with higher probability to reach high adjuvanticity positions it can end up on top of a tumor and attack
        instantly or close to it. If it ends up on top and attacks it can win (and take the place of the tumor cell
        leaving the initial cell empty) or lose and look for a cell (not tumor) in the self._parameters['maxmov']
        size neighborhood to swap with. If it doesn't find it, it disappears from the domain.
        """

        # possible directions of an IS cell (with whom it can exchange)
        aus = self._adjuvanticity.find_move_probs(self._cellType, i, j, CHANGEPOS, self._parameters['maxmov'])
        dirs = aus[0]
        targetprob = aus[1]
        if len(dirs) > 0:
            target = random.choices(dirs, targetprob)[0]

            if self._cellType.get_value(target[0], target[1]) in KILLABLE:
                # IS cell is "on top" of a tumor and attacks it
                win = self.attack(i, j, [(target[0], target[1])])
                if win:
                    # if the attack method wins it has replaced instead of the tumor an empty cell.
                    # So I do the swap of the empty cell with the IS cell (if it is a NK p already
                    # replaced it with the empty cell)

                    self._sim.swap(target[0], target[1], i, j)
                    self._acted.set_value(target[0], target[1], True)
                    self._acted.set_value(i, j, True)

                    # TODO l'unica cosa non aggiornata in caso di ctl è l'indice di infiltrazione che forse
                    #  è da ripensare completamente in funzione del lavoro di Alessio

                else:
                    # if it loses the tumor stays put. The IS moves to the empty cell available or,
                    # if it does not find it, it disappears from the domain

                    aus0 = self._adjuvanticity.find_move_probs(self._cellType, i, j, set(CHANGEPOS).difference(set(KILLABLE)), self._parameters['maxmov'])
                    dirs0 = aus0[0]
                    targetprob0 = aus0[1]
                    if len(dirs0) > 0:
                        target0 = random.choices(dirs0, targetprob0)[0]
                        self._sim.swap(target0[0], target0[1], i, j)
                        self._acted.set_value(target0[0], target0[1], True)
                        if not self._acted.get_value(i, j):
                            self._sim.agents[self._cellType.get_value(i, j)][0].step(i, j)
                    else:
                        self.die(i, j)
            else:

                self._sim.swap(target[0], target[1], i, j)
                self._acted.set_value(target[0], target[1], True)

                if not self._acted.get_value(i, j):
                    self._sim.agents[self._cellType.get_value(i, j)][0].step(i, j)
