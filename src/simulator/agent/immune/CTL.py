from .Agent import ImmuneSystem as Agent

from ..parametri import *

import random


class CTL(Agent):
    """CTL is a class used to represent a T-lymphocyte cell agent inside the sim"""

    def __init__(self, simulation):
        super().__init__(simulation)

        self._penaltyOnMiss = False

        if self._parameters['ctl_penalty'] == 1:
            self._penaltyOnMiss = True

    def initialize(self, i, j):
        """
        TODO : finish documentation of this method
        """

        self._ctlKillsLeft.set_value(i, j, self._parameters['ctl_duration'])

    def step(self, i, j):
        """
        A CTL can randomly move, die or attack. Can attack only if there are tumors in the neighborhood.
        If there are Treg around the probability of attack is reduced by 1/4 (ARBITRARY).

        Calculate the number of tumor neighbors of the CTL (to calculate an index of infiltration)
        Recognize targets to kill based on type and compatible or specific antigens
        """

        targets = []
        if self._antigens.get_value(i, j) != 0:

            # Find antigens in current CTL
            ctlantigens = set(self._antigenDict[self._antigens.get_value(i, j)])

            # Find cells antigen specific
            targets = self._cellType.find_cells_antigen_specific(i, j, KILLABLE, self._antigens, self._antigenDict, ctlantigens)
            tregNearby = self._cellType.find_cells(i, j, 'treg')

        if (len(targets) > 0) and (len(tregNearby) < 1):
            self.attack(i, j, targets)
        elif (len(targets) > 0) and (len(tregNearby) >= 1) and (random.random() < 1 / 4):
            self.attack(i, j, targets)
        elif random.random() < self._parameters['p_die']:
            self.die(i, j)
            # print('\n\tCTL [{}, {}]: DIE'.format(i, j))
        else:
            # print('\n\tCTL [{}, {}]: MOVE'.format(i, j))
            self.move(i, j)

    def attack(self, i, j, target):
        """
        TODO : finish documentation of this method
        """

        kLeft = self._ctlKillsLeft.get_value(i, j)

        # if kLeft == 0:
        #     print('\nNo attack available', file=sys.stderr)

        # is a list of 2 elements: the coordinates of the chosen target
        toKill = target[random.randint(0, len(target) - 1)]

        # store the id of the target cell
        targetID = self._cellType.get_value(toKill[0], toKill[1])

        if targetID == 'pdlp':
            p = self._parameters['ctl_kill_pdlp']
        else:
            p = self._parameters['ctl_kill']

        if random.random() < p:

            # print('\n\tCTL [{}, {}]: HIT'.format(i, j))
            kLeft -= 1

            self.replace_agent(i=toKill[0], j=toKill[1], new='empty', old=targetID)

            # since the attack was successful I induce CTL through cytokine recall
            self._adjuvanticity.set_value_neigh(toKill[0], toKill[1], self._parameters['adj_max'])

            # find tumor cell antigens
            if self._antigens.get_value(toKill[0], toKill[1]) != 0:

                tumant = self._antigenDict[self._antigens.get_value(toKill[0], toKill[1])]

                # TODO : fix this workaround
                if type(self._lymphNode[1]) != list:
                    print(f'Lymph node type: {type(self._lymphNode[1])}')
                    print('#' * 50)
                    self._lymphNode[1] = list(self._lymphNode[1])

                if type(tumant) != list:
                    print(f'Tumor antigen type: {type(tumant)}')
                    print('#' * 50)
                    tumant = list(tumant)

                self._lymphNode[1] = self._lymphNode[1] + tumant

                self._lymphNodeStimulus[1] += 1

                if "noadj" in tumant:
                    self._adjuvanticity.set_value_neigh(toKill[0], toKill[1], self._parameters['adj_min'])

                self._antigens.set_value(toKill[0], toKill[1], 0)

            self._acted.set_value(toKill[0], toKill[1], True)

        else:

            if self._penaltyOnMiss:
                kLeft -= 1

        # print('\tCTL [{}, {}]: LEFT ({})'.format(i, j, kLeft))

        # CTL is still alive?
        if kLeft == 0:

            # CTL dies and can attract Treg
            if random.random() < self._parameters['treg_reclut_par']:
                self.die(i, j)
                self.replace_agent(i=i, j=j, new='treg')
                signalNearby = self._adjuvanticity.get_value_neigh(i, j)

                if signalNearby > 8:
                    self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

            else:
                # print('\tCTL [{}, {}]: DIE'.format(i, j))
                self.die(i, j)

        else:

            self._ctlKillsLeft.set_value(i, j, kLeft)

        self._acted.set_value(i, j, True)

        # TODO remove
        targets = self._cellType.find_cells(i, j, KILLABLE)
        self._killableNeigh.set_value(i, j, len(targets) + 1)

    def die(self, i, j):
        """
        TODO : finish documentation of this method
        """

        super().die(i, j)
        self._ctlKillsLeft.set_value(i, j, 0)
        self._killableNeigh.set_value(i, j, 0)
