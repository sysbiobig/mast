from .Agent import ImmuneSystem as Agent


class DC(Agent):
    """DC is a class used to represent a dendritic cell agent inside the sim"""

    def __init__(self, simulation):
        super().__init__(simulation)
