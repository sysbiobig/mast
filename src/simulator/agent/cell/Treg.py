import random
from ..parametri import *

from ..Agent import Agent


class Treg(Agent):
    """ Treg is an agent inside the sim.

    Can die (i.e. disappear from the domain) with the same probability as other immune system cells or move
    in place of other immune system cells, or empty cells (but not stroma or tumors) that are in close proximity.
    It moves following the gradient of adjuvanticity (so it should approach CTL).
    When it reaches a position it decreases adjuvanticity around.
    """

    def __init__(self, simulation):
        super().__init__(simulation)

    def step(self, i, j):
        """
        TODO : finish documentation of this function
        """
        if random.random() < self._parameters['p_die']:
            self.die(i, j)
        else:
            self.move(i, j)

    def move(self, i, j):
        """Moves in the direction of high adjuvanticity and lowers it"""

        # possible directions
        dirs = self._cellType.find_cells(i, j, CHANGEPOS)
        if len(dirs) > 0:
            target = dirs[0]
            if len(dirs) > 1:
                targetprob = []
                for tgt in dirs:
                    # +1 otherwise random choices does not work
                    targetprob.append(self._adjuvanticity.get_value_neigh(tgt[0], tgt[1]) + 1)
                target = random.choices(dirs, targetprob)[0]

            # move to the new destination
            self._sim.swap(target[0], target[1], i, j)

            # lower adjuvanticity in the new position
            signalNearby = self._adjuvanticity.get_value_neigh(target[0], target[1])

            if signalNearby > 8:  # if the average signal is more than 1 per cell
                self._adjuvanticity.set_value_neigh(target[0], target[1], self._parameters['adj_min'])

            self._acted.set_value(target[0], target[1], True)

            if not self._acted.get_value(i, j):
                self._sim.agents[self._cellType.get_value(i, j)][0].step(i, j)

        else:

            self._acted.set_value(i, j, True)
