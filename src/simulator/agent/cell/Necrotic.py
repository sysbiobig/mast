from ..Agent import Agent
from math import exp

import random


class Necrotic(Agent):
    """
    Necrotic is an agent inside the sim.

    This cell is able to release cytokines calling DC in the surroundings. Also
    lets immune cells appear in its position after its death. Its consumption is
    zero.
    """

    def __init__(self, simulation):
        super().__init__(simulation)

    def initialize(self, i, j):
        """
        TODO : finish documentation of this method
        """

        self._numCycles.set_value(i, j, 1)
        self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_max'])  # necrotic cell releases cytokines

    def step(self, i, j):
        """
        TODO : finish documentation of this method
        """

        # can recall IS cells
        signalNearby = self._adjuvanticity.get_value_neigh(i, j)  # cytokines signal around

        self._numCycles.set_value(i, j, self._numCycles.get_value(i, j) + 1)

        # if there is a signal, recall immune system cells
        if signalNearby > 0:
            p = self._parameters['dc_reclut_par'] * exp(- (1 / signalNearby))

            # generate dendritic cell in place of the necrotic cell
            if random.random() < p:
                self._numCycles.set_value(i, j, 0)
                self._acted.set_value(i, j, True)
                self.replace_agent(i=i, j=j, new='dc', old='necro')

        # if its life cycle ends, disappears and leaves room for an empty cell
        if (self._acted.get_value(i, j) is False) and (self._numCycles.get_value(i, j) > self._parameters['max_necr_cycles']):
            self.die(i, j)
