import random
from math import exp

from ..Agent import Agent
from ..parametri import *


class Empty(Agent):
    """Empty is an agent inside the sim.

    This cell does nothing, has the ability to recruit other cells in its position
    """

    def __init__(self, simulation):
        """Empty initializer

        Parameters
        ----------
        simulation : Simulator
            It's an object useful to retrieve sim data
        """

        super().__init__(simulation)

    def step(self, i, j):
        """Depending on the surrounding signals, a specific cell or nothing can
        be generated.

        :type i: int
        :param i: The row position (inside the sim grid) in which to make the step
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the step
        """

        # Retrieving cytokine signaling in the neighborhood
        signalNearby = self._adjuvanticity.get_value_neigh(i, j)

        # Checking if (i,j) is in the list of grid cells where 'treg' or 'stroma'
        # should appear, if not sample the action
        if (i, j) in self._nextTreg:

            self._nextTreg.remove((i, j))

            self.replace_agent(i=i, j=j, new='treg')
            if signalNearby > 8:
                self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

        elif (i, j) in self._nextStroma:

            self._nextStroma.remove((i, j))

            self.replace_agent(i=i, j=j, new='stroma')
            if signalNearby > 8:
                self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

        else:

            # Checking if there are 'treg' or 'stroma' cells nearby
            inhibNearby = self._cellType.find_cells(i, j, INHIBITORY)

            # TODO: create a more specific function to avoid 'len' since we only need to
            #  know how many are and not which so doing you avoid the append

            # if the inhibitory cells in the surroundings are > 0

            # if there is sign of inflammation in the surroundings
            if signalNearby > 0:
                p = exp(- (1.0 / signalNearby))

                # pStromaTreg = exp(- (self._parameters['inhib_reclut_par'] / len(inhibNearby)))
                pStroma = exp(- (self._parameters['inhib_caf_reclut_par'] / len(inhibNearby))) if len(inhibNearby) > 0 else 0.0
                pTreg = exp(- (self._parameters['inhib_treg_reclut_par'] / len(inhibNearby))) if len(inhibNearby) > 0 else 0.0
                pNone = max(0.0, 1.0 - (p * self._parameters['nk_reclut_par'] + p * self._parameters['dc_reclut_par'] +
                                        p * self._parameters['ctl_reclut_par'] + pStroma + pTreg))

                rc = random.choices(("NK", "DC", "CTL", "STROMA", "TREG", "None"), (
                    p * self._parameters['nk_reclut_par'], p * self._parameters['dc_reclut_par'],
                    p * self._parameters['ctl_reclut_par'], pStroma, pTreg, pNone))[0]

            else:

                pNone = max(0.0, 1.0 - (self._parameters['innate_perc'] + self._parameters['innate_perc']))

                rc = random.choices(("NK", "DC", "None"), (self._parameters['innate_perc'], self._parameters['innate_perc'], pNone))[0]

            # Recall natural killer cells
            if rc == "NK":
                self.replace_agent(i=i, j=j, new='n_killer')

            # Recall dentritic cells
            elif rc == "DC":
                self.replace_agent(i=i, j=j, new='dc')

            # Set STROMA to appear in the next iteration
            elif rc == "STROMA":
                self._nextStroma.append((i, j))
            ##                S = p*self._parametri['nk_reclut_par']+p*self._parametri['dc_reclut_par']+p*self._parametri['ctl_reclut_par']+pStromaTreg/2+pStromaTreg/2+pNone
            ##                print(p*self._parametri['nk_reclut_par']/S,p*self._parametri['dc_reclut_par']/S,p*self._parametri['ctl_reclut_par']/S,pStromaTreg/2/S,pStromaTreg/2/S,pNone/S)
            ##                print(signalNearby)
            ##                input("...")
            ##            self._tipicell.set_value(i, j, 8)
            ##            self._cellCounts[0] -= 1
            ##            self._cellCounts[8] += 1
            ##            if signalNearby > 8 :
            ##                self._adjuvanticity.set_value_neigh(i, j, 1) #se c'era, diminuisco adjuvanticity, non la annullo

            # Set TREG to appear in the next iteration
            elif rc == "TREG":
                self._nextTreg.append((i, j))
            ##            self._tipicell.set_value(i, j, 9)
            ##            self._cellCounts[0] -= 1
            ##            self._cellCounts[9] += 1
            ##            if signalNearby > 8 :
            ##                self._adjuvanticity.set_value_neigh(i, j, 1) #se c'era, diminuisco adjuvanticity, non la annullo

            # Recall CTL cells (specific antigens)
            elif rc == "CTL":
                # If there is any memory of activity in the lymph node len(antigens)>0 and stimuli>0 in the last 48
                # hours then I sample a random antigen from those presented to the lymph node in the last 48 hours.
                # Higher probability to catch the antigens presented more frequently in the last 48 hours.

                # list of antigens recently (last 24 h) presented to the lymph node
                antigens = self._lymphNode[5] + self._lymphNode[6] + self._lymphNode[7] + self._lymphNode[8]

                # number of stimuli recently (last 24 h) presented to the lymph node
                stimuli = self._lymphNodeStimulus[5] + self._lymphNodeStimulus[6] + self._lymphNodeStimulus[7] + self._lymphNodeStimulus[8]

                if (len(antigens) > 0) and (stimuli > 0):
                    self.replace_agent(i=i, j=j, new='ctl')
                    self._sim.agents['ctl'][0].initialize(i, j)

                    ctl_antigen = random.choices(antigens)

                    if [ant for ant in self._antigenDict.values() if ant == ctl_antigen]:
                        k = list(self._antigenDict.values()).index(ctl_antigen) + 1
                        # print("{} FOUND with index {}".format(ctl_antigen, k))
                    else:
                        # for k, v in self._antigenDict.items():
                        #     print("{} - {}".format(k, v))
                        self._sim.insert_antigen_list(ctl_antigen, i, j)
                        k = list(self._antigenDict.values()).index(ctl_antigen) + 1
                        # for h, v in self._antigenDict.items():
                        #     print("{} - {}".format(h, v))
                        # print("{} NOT FOUND with index {}".format(ctl_antigen, k))

                    self._antigens.set_value(i, j, k)

        self._acted.set_value(i, j, True)

        '''
        QUESTA E' TUTTA COMMENTATA PER EVITARE DI ARRUOLARE CTL SPECIFICHE DIRETTAMENTE SUL TUMORE SE NO IL SI E' TROPPO FORTE...
        elif rc == "CTL": # richiamo CTL antigene specifici
            auslist = self._antigens.getListNeigh(i,j) #lista di chiavi (del dizionario _antigenDict) degli antigens nel vicinato della cella (i,j) [parametro memorizzato nella griglia se ci sono altre CTL o tumor cells]
            auslist = list(filter(lambda c: c!=0, auslist)) #elimino gli 0 che non corrispondono a nessun antigene
            if (len(auslist)>0):
                ctl_antigen = self._antigenDict[random.choices(auslist)[0]] #antigene (eventualmente composto) del vicino scelto a caso
                #print("qui ",ctl_antigen)
                #inizializzo la CTL
                self._tipicell.set_value(i, j, 5)
                self._gm._agents[5].inizializza(i, j)
                #"espongo" la CTL all'antigene. questo mi serve a trovare nel dizionario l'indice che corrisponde all'antigene
                for ind, value in self._antigenDict.items():
                    if value == ctl_antigen:
                        break
                self._antigens.set_value(i,j,ind) #aggiorno la griglia
                print("qua", self._tipicell.get_value(i,j), "(",i,j,")",self._antigens.get_value(i, j), self._antigenDict[self._antigens.get_value(i, j)],"K ",self._ctlKillsLeft.get_value(i, j))
                self._cellCounts[0] -= 1
                self._cellCounts[5] += 1
                #print(self._cellCounts[5],i,j,ind,value,"corrisponde?",self._antigens.get_value(i,j),self._antigenDict[self._antigens.get_value(i,j)])

            # Non ho alcun antigene nel vicinato (len(auslist)==0)
            # ma c'è una qualche memoria di attività nel linfonodo len(antigens)>0 and stimuli>0 nelle ultime 48 ore
            # allora campiono un antigen a caso tra quelli presentati al linfonodo nelle ultime 48 ore
            # In questo modo ho maggiori probabilità di beccare gli antigens presentati più frequentemente nelle ultime 48 ore
            if (len(auslist)== 0) and (len(antigens)>0) and (stimuli>0):
                ctl_antigen=random.choices(antigens) #questo e' l'antigene. Ad esempio "b".
                #inizializzo la CTL
                self._tipicell.set_value(i, j, 5)
                self._gm._agents[5].inizializza(i, j)
                #"espongo" la CTL all'antigene. questo mi serve a trovare nel dizionario l'indice che corrisponde all'antigene
                for ind, value in self._antigenDict.items():
                    if value == ctl_antigen:
                        break
                self._antigens.set_value(i,j,ind) #aggiorno la griglia
                print("qua qua", self._tipicell.get_value(i,j), "(",i,j,")",self._antigens.get_value(i, j), self._antigenDict[self._antigens.get_value(i, j)],"K ",self._ctlKillsLeft.get_value(i, j))
                self._cellCounts[0] -= 1
                self._cellCounts[5] += 1
                #print(self._cellCounts[5],i,j,ind,value,"corrisponde?",self._antigens.get_value(i,j),self._antigenDict[self._antigens.get_value(i,j)])

        self._acted.set_value(i, j, True)
        '''