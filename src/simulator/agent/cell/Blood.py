from ..Agent import Agent


class Blood(Agent):
    """
    Blood is an agent inside the sim.

    This cell is like an obstacle, doesn't have to do anything.
    Its nutrient consumption is zero.
    """

    def __init__(self, simulation):
        super().__init__(simulation)

    def step(self, i, j):
        """Does nothing

        :type i: int
        :param i: The row position (inside the sim grid) in which to make the swap
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the swap
        """

        pass
