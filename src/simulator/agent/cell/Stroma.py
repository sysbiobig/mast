from ..Agent import Agent
from math import exp
import random


class Stroma(Agent):
    """Stroma is an agent inside the sim.

    Can die due to lack of food like tumor, but gives space to empty cells.
    Can only move by exchanging with neighboring tumor cells (movement is implemented in the tumor move method)
    """

    def __init__(self, simulation):
        super().__init__(simulation)

    def step(self, i, j):
        """
        TODO : finish documentation of this function
        """

        # Calculate the probability of death (due to lack of food)
        p_die = exp(-(self._M.get_value(i, j) / (self._parameters['tum_necr_par'])) ** 2)

        # Calculate the probability of doing nothing
        p_none = max(0.0, 1.0 - p_die)

        action = random.choices(("die", "none"), (p_die, p_none))[0]
        if action == "die":
            self.die(i, j)
