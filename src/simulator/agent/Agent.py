class Agent:
    """Agent is a class used to represent a generic agent inside the sim"""

    def __init__(self, simulation):
        """Agent initializer

        Parameters
        ----------
        simulation : Simulator
            It's an object useful to retrieve sim data
        """

        self._sim = simulation

        self._parameters = simulation.parameters
        self._cellType = simulation.cell_type
        self._acted = simulation.acted
        self._adjuvanticity = simulation.adjuvanticity
        self._antigens = simulation.antigens
        self._thetaDupl = simulation.theta_duplication
        self._thetaNecr = simulation.theta_necrosis
        self._numCycles = simulation.num_cycles
        self._ctlKillsLeft = simulation.ctl_kills_left
        self._killableNeigh = simulation.killable_neighbors
        self._N = simulation.glucose
        self._M = simulation.oxygen

        # Setting the agent consumption
        self._nConsumption = simulation.glucose_consumption
        self._mConsumption = simulation.oxygen_consumption

        self._lymphNode = simulation.lymph_node
        self._lymphNodeStimulus = simulation.lymph_node_stimulus
        self._antigenDict = simulation.antigen_dictionary
        self._nextStroma = simulation.next_stroma
        self._nextTreg = simulation.next_treg

    def step(self, i, j):
        """Describe actions to be performed in position (i, j).

        :type i: int
        :param i: The row position (inside the sim grid) in which to make the step
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the step
        """

        pass

    def move(self, i, j):
        """Perform movement starting from position (i, j).

        :type i: int
        :param i: The row position (inside the sim grid) in which to make the move
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the move
        """
        pass

    def replace_agent(self, i, j, new: str, old: str = 'empty'):
        """Replace the cell type in position (i, j).

        If the argument `target` isn't passed in, the default swap occurs with
        an empty cell.

        :type i: int
        :param i: The row position (inside the sim grid) in which to make the swap
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the swap
        :type new: str
        :param new: The cell that replaces
        :type old: str
        :param old: The cell that needs to be replaced (default is empty)
        """

        # Set the new value of the cell in position (i, j)
        self._cellType.set_value(i, j, new)

        # Fix the overall count of cells
        self._sim.agents[new][1] += 1
        self._sim.agents[old][1] -= 1

    def die(self, i, j):
        """Kill the cell in position (i, j)
        
        :type i: int
        :param i: The row position (inside the sim grid) in which to make the swap
        :type j: int
        :param j: The col position (inside the sim grid) in which to make the swap
        """

        # Retrieve type of the cell to be killed
        cell_type = self._cellType.get_value(i, j)

        # Reset the status of the grid cell
        self.replace_agent(i=i, j=j, new='empty', old=cell_type)
        self._numCycles.set_value(i, j, 0)  # no cycle performed
        self._acted.set_value(i, j, True)  # just acted
        self._antigens.set_value(i, j, 0)  # no more antigens

        if self._sim.agents[cell_type][1] == 0:
            print("\n\tAgent {} currently annihilated\n".format(cell_type.upper(), self._sim.agents[cell_type][1]))

    def get_n_consumption(self):
        """Getter of property _nConsumption"""

        return self._nConsumption

    def get_m_consumption(self):
        """Getter of property _mConsumption"""

        return self._mConsumption
