import os

POSITIONS_FILENAME = 'data_position.csv'
ANTIGENS_FILENAME = 'antigens_position.csv'
TIMEREC_FILENAME = 'timerec.csv'


# NUM_SIM = 10 #numero totale di simulazioni
DOFILERECORD = True  #salvo o no il file con l'andamento delle popolazioni?
PROGRESS_BAR = True #Per avere la progress_bar a display

#COSTANTI di SIMULAZIONE
# W = 200 #larghezza griglia
# H = 200 #altezza griglia
# NUMCYCLES = 280  # numero di cicli di simulazione (ogni ciclo simula 6 ore)
# NUM_HOURS = 6 # numero di ore per singola iterazione
# DAY_SAVE = 3 # numero di giorno che bisogna attendere per il salvataggio dei dati automatico

#antigeni
ANTIGENLIST = tuple("abcdefghijklmnopqrstuvwxyz")  #un simbolo=un antigene. Lista possibili antigeni (al momento 26...)
TUM_ANTIGEN_KEY  = 1  #di default ha gli antigeni nella chiave 1 del dizionario

INT_ANTIGENS = ('tumor', 'pdlp') #Cellule interessate allo sviluppo di antigeni
CHANGEPOS = ('empty', 'tumor', 'n_killer', 'ctl', 'pdlp', 'dc', 'treg')  #Tupla di tipi cellulari che le cellule immunitarie possono scalzare quando si spostano o duplicano (sane, e sistema immunitario, ma non stroma) # settembre 2020 ho settato possibile scambio con tumore per permettere infiltrazione SI maggiore !!!!
KILLABLE = ('tumor', 'pdlp') #Tupla dei tipi cellulari che una cellula immunitaria può uccidere
TUM_CHANGEPOS = ('empty', 'n_killer', 'ctl', 'dc', 'stroma', 'treg')  #Tupla di tipi cellulari che le cellule tumorali possono scalzare quando si spostano o duplicano (sane, stroma e sistema immunitario) e non (altre tumorali, necrotiche o blood)
TUMOR_CHANGEPOS = ('stroma',)  #Tupla di tipi cellulari che aumentano la mobilità delle tumorali. Non serve scriverla 2 volte, per creare una tupla lunga 1 basta aggiungere la virgola finale
TUM_INDUCEDUPL = ('stroma',) #Cellule che inducono duplicazione nel tumore (8 stroma). Non serve scriverla 2 volte, per creare una tupla lunga 1 basta aggiungere la virgola finale
INHIBITORY = ('stroma', 'treg') #Cellule inibitorie del SI (8 stroma e 9 treg)





# # Parametri da letteratura
# TUM_DIV = 2 #Numero di cicli che una cellula tumorale impiega per dividersi
# INNATE_KILL = 0.3 #probabilità NK di uccidere quando attaccano
# INNATE_PERC = 0.005 # percentuale INIZIALE NK e DC sul totale di cellule sane
# CTL_DURATION = 6  #numero di attacchi CTL prima di morire
# CTL_KILL = 0.08 #probabilità di una ctl di uccidere una tumorale pdl-
# MAX_NECR_CYCLES = 8 # durata cellule necrotiche prima di sparire (2 days)
#
# #Parametri "arbitrari" CHE NON HA MOLTO SENSO TOCCARE
# CTL_KILL_PDLP = CTL_KILL/2 #probabilità di una ctl di uccidere una tumorale pdl+
# TUM_NECR_PAR = 0.3  ##parametro INIZIALE necrosi cellula tumorale nell'equazione. La cellula va in necrosi solo con concentrazioni nutrienti molto basse
# TUM_DUPL_PAR = 1.2  #Parametro INIZIALE duplicazione. Se lo abbasso aumento la prob di duplicare. Corrisponde a un range 0 (in assenza di nutrienti) 0.5
# TUM_MOV_PAR = 2 #parametro movement cellula tumorale nell'equazione. Se è alto si muove poco
# TUM_DUPLSTROMA_PAR = 0.5  #parametro (>0) di aumento proliferazione in presenza di stroma nell'intorno TUM_DUPL_PAR_USATO<-TUM_DUPL_PAR/(1+TUM_DUPLSTROMA_PAR*#stroma_cells/8)
# MAXMOV = 10 #numero massimo di spostamenti per ciclo per una cellula del SI.
# P_DIE = 0.1 #probabilità di una cellula del SI (comprese Treg) e Stroma di "morire" (o sparire dal dominio, il che è equivalente) se non trovano il target (solo per NK, DC e CTL)
#
# #Parametri nutrizionali "arbitrari" CHE HA SENSO SETTARE PER OSSERVARE DIVERSI COMPORTAMENTI DEL SISTEMA - al momento lasciare fissi
# NCONS = 0.005 #parametro consumo nutrienti tipo N (per duplicazione) da cellule non tumorali
# MCONS = 0.005 #parametro consumo nutrienti tipo M (per sopravvivenza) da cellule non tumorali
# TUM_NCONS = 0.05 #parametro consumo nutrienti tipo N (per duplicazione) da cellule non tumorali
# TUM_MCONS = 0.05 #parametro consumo nutrienti tipo M (per sopravvivenza) da cellule non tumorali
#
# #Parametri "arbitrari" CHE HA SENSO SETTARE PER OSSERVARE DIVERSI COMPORTAMENTI DEL SISTEMA da osservazioni dati/paper single cell
# INHIB_RECLUT_PAR = 1  # parametro della prob_reclutamento_stroma o Treg(da empty cell), che è proporzionale a exp(-(INHIB_RECLUT_PAR/#TregNearby)). Se è basso (2-3) si tende a formare lo stroma attorno al tumore
# INHIB_TREG_RECLUT_PAR = 1  # parametro della probabilità di reclutamento Treg (da empty cell), che è proporzionale a exp(-(INHIB_TREG_RECLUT_PAR/#TregNearby)).
# INHIB_STROMA_RECLUT_PAR = 1  # parametro della probabilità di reclutamento Treg (da empty cell), che è proporzionale a exp(-(INHIB_STROMA_RECLUT_PAR/#TregNearby)).
# Treg_RECLUT_PAR = 0.5  #probabilità di reclutare una Treg quando muore una CTL (dopo CTL_DURATION cicli)

# CTL_RECLUT_PAR = 0.7 # parametro equazione reclutamento in agent_empty p = CTL_RECLUT_PAR * exp( - (1 / signalNearby) ) #ref (cDC1 chemoattractants released by NK are CCL5 and XCL1)
# DC_RECLUT_PAR = 0.2  # parametro equazione reclutamento in agent_empty p = DC_RECLUT_PAR * exp( - (1 / signalNearby) )
# NK_RECLUT_PAR = 0.1  # parametro equazione reclutamento in agent_empty p = NK_RECLUT_PAR * exp( - (1 / signalNearby) )

# TUM_MUT_RATE  = 0.5  #tumor mutation rate
# TUM_NEWANTIGEN_RATE = 0.05   #tumor mutation rate with new antigen presentation
# TUM_ADJCHANGE_RATE = 0.005   #probabilità mutazione che porta la cellula tumorale a rilasciare agenti che allontanano IS cells (in realtà si abbassa l' adjuvanticity nell'intorno)
# TUM_DUPLCHANGE_RATE = 0.05   #probabilità mutazione che porta la cellula tumorale a duplicare con parametro thetadupl (memorizzato in un oggetto di tipo griglia) diverso
# TUM_NECRCHANGE_RATE = 0.05   #probabilità mutazione che porta la cellula tumorale a consumare piu' o meno e a modificare il parametro thetanecr (memorizzato in un oggetto di tipo griglia) diverso
# TUM_PDLP_MUT = 0.1  #probabilità mutazione cellula tumorale che la rende PDL+
# TUM_PDLM_MUT = TUM_PDLP_MUT / 20  #probabilità mutazione cellula tumorale che la rende di nuovo esposta all'attacco delle ctl. posso alzarla per mimare l'effetto immunotheraphy
