import copy
import random
import sys
from math import exp

from ..Agent import Agent
from ..parametri import *


class Tumoral(Agent):
    """Tumoral is a class used to represent a tumor agent inside the sim"""

    def __init__(self, simulation):
        super().__init__(simulation)
        self._localCount = 1

        # Setting the agent consumption
        self._nConsumptionTumor = simulation.tumor_glucose_consumption
        self._mConsumptionTumor = simulation.tumor_oxygen_consumption

    def directions(self, i: int, j: int, agents: tuple):

        # Sanity check
        if self._parameters['caf_impermeability'] > 1.0:
            print('Wrong value detected for stroma_block_perc parameter. It should assume value in [0, 1]', file=sys.stderr)
            exit(1)

        temp = agents

        if self._parameters['caf_impermeability'] == 1.0:
            # print('\nStroma modeled as obstacle')
            temp = [agent for agent in agents if agent != 'stroma']
            temp = tuple(temp)
        elif 0.0 < self._parameters['caf_impermeability'] < 1.0:
            if random.random() < self._parameters['caf_impermeability']:
                temp = [agent for agent in agents if agent != 'stroma']
                temp = tuple(temp)
                # print('\n Possibilities : ', temp)

        dirs = self._cellType.find_cells(i, j, temp)

        return dirs

    def step(self, i, j):
        """
        TODO : finish documentation of this method
        """

        numcycles = self._numCycles.get_value(i, j)

        if numcycles > self._parameters['tum_div']:

            # regardless of the action there is a chance to mutate
            if random.random() < self._parameters['tum_mut_rate']:
                self.mutate(i, j)

            # looking for cells (stroma ) that amplify tumor duplication
            induce_cells = self._cellType.find_cells(i, j, TUM_INDUCEDUPL)
            localdupl = self._thetaDupl.get_value(i, j)

            if localdupl == 0:
                p_dupl = 1.0
            else:
                # if stroma is present, tumor proliferation is increased
                p_dupl = 1.0 - exp(-(self._N.get_value(i, j) / (self._localCount * localdupl / (1 + (self._parameters['tum_duplstroma_par'] * len(induce_cells))))) ** 2)

            # calculate the probability of movement that increases in the presence of STROMA.
            # In this way I simulate the stroma that infiltrates the tumor tissue and somehow promotes
            # the displacement of tumor cells (although angiogenesis is not explicitly modeled).

            p_move = 1 - exp(-self._localCount * (self._M.get_value(i, j) / self._parameters['tum_mov_par']) ** 2)
            dirs = self._cellType.find_cells(i, j, TUMOR_CHANGEPOS)

            if len(dirs) > 0:
                p_move = 1.0 - exp(-self._localCount * (self._M.get_value(i, j) / (self._parameters['tum_mov_par'] / (len(dirs) + 1))) ** 2)

            # calculate the probability of necrosis
            localnecr = self._thetaNecr.get_value(i, j)

            if localnecr == 0:
                p_die = 0.0
            else:
                p_die = exp(-(self._M.get_value(i, j) / (self._localCount * localnecr)) ** 2)

            p_none = max(0.0, 1.0 - (p_move + p_dupl + p_die))

            action = random.choices(("move", "duplicate", "necrosis", "none"), (p_move, p_dupl, p_die, p_none), k=1)[0]

            if action == "move":
                self.move(i, j)
            elif action == "duplicate":
                self.duplicate(i, j)
            elif action == "necrosis":
                self.die(i, j)

        self._numCycles.set_value(i, j, numcycles + 1)

    def move(self, i, j):
        """
        TODO : finish documentation of this method
        """

        # dirs = self._cellType.find_cells(i, j, TUM_CHANGEPOS)
        dirs = self.directions(i, j, TUM_CHANGEPOS)

        if len(dirs) > 0:

            target = dirs[random.randint(0, len(dirs) - 1)]

            self._sim.swap(target[0], target[1], i, j)
            self._acted.set_value(target[0], target[1], True)
            # lower the adjuvanticity to the old position i,j if Stroma or Treg has moved here

            cell_type = self._cellType.get_value(i, j)
            if cell_type in INHIBITORY:
                # cytokines signal around
                signalNearby = self._adjuvanticity.get_value_neigh(i, j)
                if signalNearby > 8:
                    self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

            # if the cell I moved to position (i,j) has not acted yet it must act now
            if not self._acted.get_value(i, j):
                self._sim.agents[self._cellType.get_value(i, j)][0].step(i, j)
        else:

            # if there are no valid directions do nothing and mark acted
            self._acted.set_value(i, j, True)

    def duplicate(self, i, j):
        """
        TODO : finish documentation of this method
        """

        # looking for positions that the tumor cell can occupy by duplicating.
        # If there are none it does nothing and marks acted
        # dirs = self._cellType.find_cells(i, j, TUM_CHANGEPOS)
        dirs = self.directions(i, j, TUM_CHANGEPOS)

        if len(dirs) > 0:

            target = random.choice(dirs)

            cell_type = self._cellType.get_value(i, j)

            self._adjuvanticity.set_value(target[0], target[1], self._adjuvanticity.get_value(i, j))
            self._antigens.set_value(target[0], target[1], self._antigens.get_value(i, j))
            self._thetaDupl.set_value(target[0], target[1], self._thetaDupl.get_value(i, j))
            self._thetaNecr.set_value(target[0], target[1], self._thetaNecr.get_value(i, j))

            self._acted.set_value(target[0], target[1], True)
            self._acted.set_value(i, j, True)

            self._numCycles.set_value(target[0], target[1], 0)
            self._numCycles.set_value(i, j, 0)

            self.replace_agent(i=target[0], j=target[1], new=cell_type, old=self._cellType.get_value(target[0], target[1]))

        else:

            self._acted.set_value(i, j, True)
            self._numCycles.set_value(i, j, 0)

    def mutate(self, rc: str, i: int, j: int):
        """
        TODO : finish documentation of this method
        """

        # Mutation class
        mutation = {
            '-03': -0.3,
            '-02': -0.2,
            '-01': -0.1,
            '+01': 0.1,
            '+02': 0.2,
            '+03': 0.3
        }

        mut = "none"
        if rc == "newantigens":
            # generate a new antigen
            mut = random.choice(ANTIGENLIST)
        elif rc == "adjuvanticity":
            mut = "noadj"
        elif rc == "necr":
            key, value = random.choice(list(mutation.items()))
            mut = 'necr' + key
            self._thetaNecr.set_value(i, j, (self._parameters['tum_necr_par'] + value))
        elif rc == "dupl":
            key, value = random.choice(list(mutation.items()))
            mut = 'dupl' + key
            self._thetaDupl.set_value(i, j, (self._parameters['tum_dupl_par'] + value))

        if mut != "none":

            if self._antigens.get_value(i, j) == 0:

                self._sim.insert_antigen_list(mut, i, j)

            else:

                mut0 = [mut for mut in self._antigenDict[self._antigens.get_value(i, j)]]

                if mut in mut0:

                    pr = random.random()
                    if pr < self._parameters['tum_newantigen_rate'] / 10:

                        # Chance to lose the antigen

                        if len(mut0) == 1:

                            # Only one antigen is present (lose it and acquire antigen a)

                            self._antigens.set_value(i, j, 0)

                        else:

                            # More than one antigen is present

                            if mut in ['dupl' + x for x in mutation.keys()]:
                                self._thetaDupl.set_value(i, j, self._parameters['tum_dupl_par'])

                                # delta = mutation.get(mut[4:])
                                # current = self._thetaDupl.get_value(i, j)
                                # self._thetaDupl.set_value(i, j, current - delta)

                            if mut in ['necr' + x for x in mutation.keys()]:
                                self._thetaNecr.set_value(i, j, self._parameters['tum_necr_par'])

                                # delta = mutation.get(mut[4:])
                                # current = self._thetaNecr.get_value(i, j)
                                # self._thetaNecr.set_value(i, j, current - delta)

                            # Lose the antigen
                            mut0.remove(mut)

                            # Update antigen list
                            self._sim.insert_antigen_list(mut0, i, j)

                else:

                    if mut[:4] in ['dupl', 'necr']:
                        if [m for m in mut0 if mut[:4] in m]:
                            # Replace dupl or necr mutation with the last acquired
                            # print("\nMutation {} [{}] found".format(mut[:4], mut))
                            # print("Matches : {}".format([k for k, elem in enumerate(mut0) if mut[:4] in elem]))
                            # print("Mut 0 pre  : {}".format(mut0))
                            i = [k for k, elem in enumerate(mut0) if mut[:4] in elem][0]  # pick value
                            mut0[i] = mut
                            # print("Mut 0 post : {}".format(mut0))
                        else:
                            # Acquire the new antigen
                            # print("\nAcquire mutation")
                            mut0.append(mut)

                    else:
                        # Acquire the new antigen
                        mut0.append(mut)

                    # Update antigen list
                    self._sim.insert_antigen_list(mut0, i, j)

    def die(self, i, j):
        """
        This is death by necrosis due to lack of nutrients. It's not modeled yet death by apoptosis.
        """

        self._acted.set_value(i, j, True)
        cell_type = self._cellType.get_value(i, j)
        self.replace_agent(i=i, j=j, new='necro', old=cell_type)

        self._sim.agents['necro'][0].initialize(i, j)

        inhibNearby = self._cellType.find_cells(i, j, INHIBITORY)
        if len(inhibNearby) > 2:
            self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

        # check if the tumor cell is a carrier of the "noadj" mutation.
        # If it is, I decrease adjuvanticity, not cancel it.
        if self._antigens.get_value(i, j) != 0:
            mut0 = self._antigenDict[self._antigens.get_value(i, j)]
            if "noadj" in mut0:
                self._adjuvanticity.set_value_neigh(i, j, self._parameters['adj_min'])

        self._antigens.set_value(i, j, 0)

    def get_n_consumption(self):
        """Getter of property _nConsumption"""

        return self._nConsumptionTumor

    def get_m_consumption(self):
        """Getter of property _mConsumption"""

        return self._mConsumptionTumor
