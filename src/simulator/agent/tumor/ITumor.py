import random

from .Agent import Tumoral as Agent


class ITumor(Agent):
    """ITumor is a class used to represent a PDL+ cell agent inside the sim"""

    def __init__(self, simulation):
        super().__init__(simulation)

    def mutate(self, i, j):
        """
        TODO : finish documentation of this method
        """

        # mutation without effect on interesting phenotypes
        pNone = max(0.0, 1.0 - (
                self._parameters['tum_newantigen_rate'] +
                self._parameters['tum_adjchange_rate'] +
                self._parameters['tum_pdlm_mut'] +
                self._parameters['tum_necrchange_rate'] +
                self._parameters['tum_duplchange_rate']))

        rc = random.choices(("none", "newantigens", "PDL-", "adjuvanticity", "necr", "dupl"),
                            weights=(pNone,
                                     self._parameters['tum_newantigen_rate'],
                                     self._parameters['tum_pdlm_mut'],
                                     self._parameters['tum_adjchange_rate'],
                                     self._parameters['tum_necrchange_rate'],
                                     self._parameters['tum_duplchange_rate']),
                            k=1)[0]

        if rc == "PDL-":
            self.replace_agent(i=i, j=j, new='tumor', old='pdlp')
        else:
            super().mutate(rc, i, j)


