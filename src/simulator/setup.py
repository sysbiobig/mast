from setuptools import setup, find_packages, Extension

setup(
    name='PyPDELab',
    version='0.0.1',
    packages=find_packages(),
    package_data={
        '': [
            '*.pyd',
            'fortran/*.so',
            'fortran/*.dylib'
        ]
    },
    data_files=[
        ('/usr/local/lib', []),
    ],

)

packages = find_packages()
