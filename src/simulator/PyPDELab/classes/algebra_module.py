import numpy
import scipy.sparse
import scipy.sparse.linalg
import sys
from ..util import stats_module
from ..fortran import f90_module

class matrix_class:
	"""Matrix
	"""

	def __init__(self,size=None,name=None):
		"""Initialize
		"""
		self.n=size
		self.nnz=None
		self.matrix=None
		if(size != None):
			self.matrix=scipy.sparse.coo_matrix((size,size))
		self.matrixname=name
		self.freeze()
		
	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value

	def list(self):
		print('Matrix name:',self.matrixname)
		print('Nrows:',self.n)
		print('Nnz:',self.nnz)
		print('Matrix:')
		print(self.matrix.todense())

	def statistics(self):
		print('Matrix name:',self.matrixname)
		print('Nrows:',self.n)
		print('Nnz:',self.nnz)
		print('Matrix range:',min(self.matrix.data),max(self.matrix.data))

class vector_class:
	"""Vector (typically a RHS)
	"""
	
	def __init__(self,size=None,name=None):
		"""Initialize
		"""
		self.n=size
		self.vectorname=name
		self.vector=None
		if(size != None):
			self.vector=numpy.zeros(size)
		self.freeze()

	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value

	def list(self):
		print('Vector name:',self.vectorname)
		print('Nrows:',self.n)
		print('Vector:')
		print(self.vector)

	def statistics(self):
		print('Vector name:',self.vectorname)
		print('Nrows:',self.n)
		print('Range:',min(self.vector),max(self.vector))
		

class eqsystem_class:
	"""System of equations
	"""
	
	def __init__(self,name=None):
		"""Initialize
		"""
		self.n=None
		self.nnz=None
		self.matrix=None
		self.matrixname=name
		self.rhs=None
		self.freeze()

	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value

	def list(self):
		print('Matrix name:',self.matrixname)
		print('Nrows:',self.n)
		print('Nnz:',self.nnz)
		print('Matrix:')
		print(self.matrix.todense())
		print('RHS:')
		print(self.rhs)

	def statistics(self):
		print('-----------------------------')
		print('Matrix name:',self.matrixname)
		print('Nrows:',self.n)
		print('Nnz:',self.nnz)
		print('Matrix range:',min(self.matrix.data),max(self.matrix.data))
		print('Rhs range:',min(self.rhs),max(self.rhs))
		print('-----------------------------')

	def build(self,M,r):
		if(M.n != r.n):
			print('Inconsistent matrix and vector size...')
			sys.exit('Fatal')
		self.n=M.n
		self.nnz=M.nnz
		self.matrix=M.matrix
		self.matrixname=M.matrixname
		self.rhs=r.vector


	def assignbc(self,rowidx,colidx,xval,moverhs=True,rowerase=True,colerase=True,diagfix=True,xfix=True,verbose=True):
		"""Assign bc to matrix and rhs, maintain symmetry, exit is in CSR
		"""
		with stats_module.timer('BC assignment:', verbose):
			if(rowidx[0].size==0):
				print('No BCs...')
				sys.exit('Fatal')
			if(moverhs):
				self.matrix=self.matrix.tocsc()
				self.rhs=self.rhs-self.matrix[:,rowidx[0]]*xval[rowidx[0]]
			#rr=self.matrix.row
			#cc=self.matrix.col
			#vv=self.matrix.data
			if(numpy.iscomplexobj(self.matrix.data)):
				if(verbose):
					print("Complex cleaning")
				f90_module.cooerasecmplx(rr,cc,vv,idx,len(rr),len(idx[0]))
			else:
				if(verbose):
					print("Real cleaning")
				if(rowerase):
					self.matrix=self.matrix.tocsr()
					nr=self.matrix.shape[0]
					v=numpy.ones(nr)
					v[rowidx[0]]=0
					s=scipy.sparse.spdiags(v,0,len(v),len(v))
					self.matrix=s*self.matrix
					#embed()
					#self.matrix.data[self.matrix.indptr[rowidx[0]]:self.matrix.indptr[rowidx[0]+1]]=0.0
					#self.matrix[rowidx[0],:]=0.0
					#f90_module.rowerase(rr,cc,vv,rowidx,len(rr),len(rowidx[0]))
				if(colerase):
					self.matrix=self.matrix.tocsc()
					nc=self.matrix.shape[1]
					v=numpy.ones(nc)
					v[colidx[0]]=0
					s=scipy.sparse.spdiags(v,0,len(v),len(v))
					self.matrix=self.matrix*s
					#self.matrix[:,colidx[0]]=0.0
					#f90_module.colerase(rr,cc,vv,colidx,len(rr),len(colidx[0]))
				if(diagfix):
					#f90_module.diagfix(rr,cc,vv,rowidx,colidx,len(rr),len(rowidx[0]))
					#self.matrix=self.matrix.tolil()
					#n=rowidx[0].size
					#for i in range(n):
						#self.matrix[rowidx[0][i],colidx[0][i]]=1.0
					#assumes that these positions have been zeroed
					self.matrix=self.matrix.tocoo()
					n=len(rowidx[0])
					v=numpy.ones(n)
					m=scipy.sparse.coo_matrix((v,(rowidx[0],colidx[0])),(self.matrix.shape[0],self.matrix.shape[1]))
					self.matrix=self.matrix+m
			#self.matrix=scipy.sparse.coo_matrix((vv,(rr,cc)),shape=(self.n,self.n))
			self.matrix=self.matrix.tocsr()
			self.matrix.eliminate_zeros()
			self.nnz=len(self.matrix.data)
			if(xfix):
				self.rhs[rowidx[0]]=xval[rowidx[0]]

	def assignsymm(self,master,slave,verbose=True):
		"""Assign symmetry bc to matrix (not sure: and rhs, maintain symmetry) exit is in CSR
		"""
		with stats_module.timer('Symmetry BC assignment:', verbose):
			if(master.size==0):
				print('No Symmetry BCs...')
				sys.exit('Fatal')
			self.matrix=self.matrix.tolil()
			self.matrix[master,:]=self.matrix[master,:]+self.matrix[slave,:]	
			
			self.matrix[slave,:]=0.0
			self.matrix[slave,slave]=1.0
			self.matrix[slave,master]=-1.0
			
			self.matrix=self.matrix.tocsr()
			self.matrix.eliminate_zeros()
			self.nnz=len(self.matrix.data)
			
			self.rhs[master]=self.rhs[master]+self.rhs[slave]
			self.rhs[slave]=0.0
			
					
            
	def directsolve(self,verbose=True):
		with stats_module.timer('Direct algebraic system solution:', verbose):
			if(verbose):
				print('Direct solver, please wait...')
			self.matrix=self.matrix.tocsr()
			sol=scipy.sparse.linalg.spsolve(self.matrix,self.rhs)
		return sol


