import sys
import numpy
import scipy.sparse
from ..fortran import f90_module
from ..util import stats_module
from . import algebra_module
			
class stiffness_class(algebra_module.eqsystem_class):
	"""FEM div grad operator 
	"""
	
	def __init__(self,name=None,order=1):
		"""Initialize
		"""
		self.n=None
		self.nnz=None
		self.matrix=None
		self.matrixname=name
		self.rhs=None
		if(order>2):
			print('Invalid divgrad order ',order,' in divgrad init.')
			sys.exit('Fatal')
		else:
			self.order=order
		self.freeze()

	def assemble(self,mesh,kappafun,selectfun,quadorder=3,verbose=True,kappafun_extra_args=()):
		"""assemble \int_{\Omega}{ \nabla N_i \dot kfun() \nabla N_j}, sign to be used in formulation to be handled externally 
		"""
		if(mesh.dimension==None):
			print('Dimension not set')
			sys.exit('Fatal in assemble.')
		msg='Created '+str(mesh.dimension)+'D DivGrad matrix '+self.matrixname+':'
		with stats_module.timer(msg, verbose):
			if(self.order==1):
				msiz=mesh.nodes.nn*30
			else:
				msiz=mesh.nodes.nn*30*10/4
			htpsiz=int(numpy.log2(msiz))+1
			locsiz=2**htpsiz
			
			mat=numpy.empty((2,locsiz),dtype=int,order='F')
			vals=numpy.empty((locsiz),dtype=float,order='F')
			rhs=numpy.empty((mesh.nodes.nn),dtype=float,order='F')
			lnnz=numpy.array([0],dtype=int)
			if(mesh.dimension==2):
				coord=mesh.nodes.n[0:2,:]
				nnori=mesh.nodes.nnori
				if(self.order==1):					
					f90_module.divgrad2d(mesh.faces.n,mesh.faces.reg,coord,mat,vals,rhs,quadorder,htpsiz,lnnz,kappafun,locsiz,mesh.faces.nf,mesh.nodes.nn,kappafun_extra_args)
				else:
					f90_module.divgrad2dsecond(mesh.faces.n,mesh.faces.reg,mesh.faces.e,coord,nnori,mat,vals,rhs,htpsiz,lnnz,kappafun,locsiz,mesh.faces.nf,mesh.nodes.nn)	
			else:
				if(self.order==1):
					f90_module.divgrad(mesh.volumes.n,mesh.volumes.reg,mesh.nodes.n,mat,vals,rhs,htpsiz,lnnz,kappafun,selectfun,locsiz,mesh.volumes.nv,mesh.nodes.nn)
				else:
					if(mesh.nodes.nnori != None):
						f90_module.divgradsecond(mesh.volumes.n,mesh.volumes.e,mesh.nodes.n,mesh.nodes.nnori,mat,vals,rhs,htpsiz,lnnz,locsiz,mesh.volumes.nv,mesh.nodes.nn)			
					else:
						#no edges and no special node numbering
						f90_module.divgradsecondnoedges(mesh.volumes.n,mesh.nodes.n,mat,vals,rhs,htpsiz,lnnz,locsiz,mesh.volumes.nv,mesh.nodes.nn)			
			self.n=max(mat[0,0:lnnz[0]])
			self.nnz=lnnz[0]
			self.matrix=scipy.sparse.coo_matrix((vals[0:self.nnz],(mat[0,0:self.nnz]-1,mat[1,0:self.nnz]-1)),shape=(self.n,self.n)) #-1 need to change from fortran numbering
			self.nnz=self.matrix.nnz
			self.rhs=numpy.zeros((mesh.nodes.nn),dtype=float,order='F')

	def addrhs(self,mesh,rhsfun,selectfun,verbose=True,type='Real',rhsfun_extra_args=()):
		msg='Created '+str(mesh.dimension)+'D rhs :'
		if(mesh.dimension==None):
			print('Dimension not set')
			sys.exit('Fatal in addrhs.')
		with stats_module.timer(msg, verbose):
			if(self.order==1):
				msiz=mesh.nodes.nn*30
			else:
				msiz=mesh.nodes.nn*30*10/4
			htpsiz=int(numpy.log2(msiz))+1
			locsiz=2**htpsiz
			
			mat=numpy.empty((2,locsiz),dtype=int,order='F')
			vals=numpy.empty((locsiz),dtype=float,order='F')
			lnnz=numpy.array([0],dtype=int)
			if(mesh.dimension==2):
				coord=mesh.nodes.n[0:2,:]
				nnori=mesh.nodes.nnori
				if(self.order==1):
					newrhs=numpy.empty((mesh.nodes.nn),dtype=float,order='F')
					f90_module.rhs2d(mesh.faces.n,mesh.faces.reg,coord,newrhs,rhsfun,mesh.faces.nf,mesh.nodes.nn,rhsfun_extra_args)
				else:
					if(type=='Real'):
						newrhs=numpy.empty((mesh.nodes.nn),dtype=float,order='F')
						f90_module.rhs2dsecond(mesh.faces.n,mesh.faces.reg,mesh.faces.e,coord,nnori,newrhs,rhsfun,mesh.faces.nf,mesh.nodes.nn)
					else:
						newrhs=numpy.empty((mesh.nodes.nn),dtype=complex,order='F')
						f90_module.rhs2dsecondcmplx(mesh.faces.n,mesh.faces.reg,mesh.faces.e,coord,nnori,newrhs,rhsfun,mesh.faces.nf,mesh.nodes.nn)
			else:
				if(self.order==1):
					newrhs=numpy.empty((mesh.nodes.nn),dtype=float,order='F')
					f90_module.divgradrhs(mesh.volumes.n,mesh.volumes.reg,mesh.nodes.n,newrhs,htpsiz,lnnz,rhsfun,selectfun,locsiz,mesh.volumes.nv,mesh.nodes.nn)
				else:
					print('not yet done')
					sys.exit('Fatal.')
					f90_module.divgradsecond(vols.n,vols.e,coords.n,coords.nnori,mat,vals,newrhs,htpsiz,lnnz,locsiz,vols.nv,coords.nn)			
			self.rhs=self.rhs+newrhs



class mass_class(algebra_module.eqsystem_class):
	"""FEM mass operator
	"""
	
	def __init__(self,name=None,order=1):
		"""Initialize
		"""
		self.n=None
		self.nnz=None
		self.matrix=None
		self.matrixname=name
		self.rhs=None
		if(order>2):
			print('Invalid divgrad order ',order,' in divgrad init.')
			sys.exit('Fatal')
		else:
			self.order=order
		self.freeze()

	def assemble(self,mesh,kappafun,selectfun,quadorder=3,verbose=True):
		"""assemble \int_{\Omega}{ N_i kfun() N_j}, sign to be used in formulation to be handled externally 
		"""
		msg='Created '+str(mesh.dimension)+'D Mass matrix '+self.matrixname+':'
		if(mesh.dimension==None):
			print('Dimension not set')
			sys.exit('Fatal in assemble.')
		with stats_module.timer(msg, verbose):
			if(self.order==1):
				msiz=mesh.nodes.nn*30
			else:
				msiz=mesh.nodes.nn*30*10/4
			htpsiz=int(numpy.log2(msiz))+1
			locsiz=2**htpsiz
			
			mat=numpy.empty((2,locsiz),dtype=int,order='F')
			vals=numpy.empty((locsiz),dtype=float,order='F')
			rhs=numpy.empty((mesh.nodes.nn),dtype=float,order='F')
			lnnz=numpy.array([0],dtype=int)
			if(mesh.dimension==2):
				coord=mesh.nodes.n[0:2,:]
				nnori=mesh.nodes.nnori
				if(self.order==1):	
					f90_module.mass2d(mesh.faces.n,mesh.faces.reg,coord,mat,vals,rhs,quadorder,htpsiz,lnnz,kappafun,locsiz,mesh.faces.nf,mesh.nodes.nn)
				else:
					f90_module.mass2dsecond(mesh.faces.n,mesh.faces.reg,mesh.faces.e,coord,nnori,mat,vals,rhs,htpsiz,lnnz,kappafun,locsiz,mesh.faces.nf,mesh.nodes.nn)		
			else:
				if(self.order==1):
					f90_module.node_node_3d_first(mesh.volumes.n,mesh.volumes.reg,mesh.nodes.n,mat,vals,rhs,htpsiz,lnnz,kappafun,selectfun,locsiz,mesh.volumes.nv,mesh.nodes.nn)

				else:
					print('Not yet done')
					sys.exit('Fatal')				
			self.n=max(mat[0,0:lnnz[0]])
			self.nnz=lnnz[0]
			self.matrix=scipy.sparse.coo_matrix((vals[0:self.nnz],(mat[0,0:self.nnz]-1,mat[1,0:self.nnz]-1)),shape=(self.n,self.n)) #-1 need to change from fortran numbering
			self.nnz=self.matrix.nnz
			self.rhs=rhs
					
