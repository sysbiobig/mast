from __future__ import print_function
import numpy

class faces_class:
	"""Array of faces
	
	nf -- number of faces
	f -- faces
	ftypes -- types of faces
	"""
	
	def __init__(self):
		"""Initialize
		"""
		self.nf=None
		self.htpowersiz=None
		self.localsiz=None
		self.nx=None
		self.ny=None
		self.n=numpy.array([],dtype=int,order='F')
		self.e=numpy.array([],dtype=int,order='F')
		self.fht=numpy.array([],dtype=int,order='F')
		self.flinks=numpy.array([],dtype=int,order='F')
		self.reg=numpy.array([],dtype=int,order='F')
		self.isinterf=numpy.array([],dtype=int,order='F')
		self.isboundr=numpy.array([],dtype=int,order='F')		
		self.center=numpy.array([],dtype=int,order='F')
		self.measure=numpy.array([],dtype=int,order='F')
		self.ftypes=[]
		self.vec=None
		self.freeze()

	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value
		
	def list(self):
		if self.nf > 0:
			print("Faces")
			for i in range(self.nf):
				print(i+1,self.n[:,i],self.reg[:,i])

	def statistics(self):
		itemindex=numpy.where(self.reg[1,0:self.nf] != 0)
		idx=itemindex[0]
		nfe=self.nf-idx.size
		itemindex=numpy.where(self.reg[1,idx] != self.reg[0,idx])
		nfi=itemindex[0].size
		print('Number of faces:',self.nf)
		print('Number of external faces:',nfe)
		print('Number of interface faces:',nfi)

