from __future__ import print_function
import numpy
from ..util import stats_module

class nodes_class:
	"""Array of nodal coordinates

	nn -- number of points
	n -- nodal coordinates
	"""

	def __init__(self):
		"""Initialize
		"""
		self.nn=None
		self.nnori=None
		self.n=numpy.array([],dtype=float,order='F')
		self.icond=None
		self.vcond=None
		self.condassign=None
		self.pot=None
		self.freeze()

	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value

	def list(self):
		if self.nn > 0:
			print("Nodal coordinates")
			for i in range(self.nn):
				print(i+1,self.n[:,i])

	def assignbc(self,cond,val,verbose=True):
		"""Assign bc to nodes
		"""
		if(verbose):
			print('Assigning ',val,' to nodes with ',cond)
		with stats_module.timer('Done:', verbose):
			x=self.n[0,:]
			y=self.n[1,:]
			z=self.n[2,:]
			if(self.condassign==None):
				self.icond=numpy.array([False]*self.nn,dtype=bool,order='F')
				self.vcond=numpy.array([0]*self.nn,dtype=float,order='F')
				self.condassign=True
			idx=numpy.where(eval(cond))
			self.icond[idx]=True
			scratch=eval(val)
			if (isinstance(scratch, float)):
				scratch=numpy.array([scratch]*self.nn,dtype=float,order='F')
			self.vcond[idx]=scratch[idx]
			if(verbose):
				print('Number of assigned nodes:',len(idx[0]))
			idx=numpy.where(self.icond)
			if(verbose):
				print('Number of total assigned nodes:',len(idx[0]))
