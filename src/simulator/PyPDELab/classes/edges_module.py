from __future__ import print_function
import numpy
from ..util import stats_module
from ..fortran import f90_module

class edges_class:
    """Array of edges

    ne -- number of edges
    e -- edges
    """

    def __init__(self):
        """Initialize
        """
        self.ne=None
        self.htpowersiz=None
        self.localsiz=None
        self.n=numpy.array([],dtype=int,order='F')
        self.eht=numpy.array([],dtype=int,order='F')
        self.elinks=numpy.array([],dtype=int,order='F')
        self.isinterf=None
        self.isboundr=None
        self.istree=None
        self.icond=None
        self.vcond=None
        self.condassign=None
        self.pot=None
        self.siz=None
        self.freeze()

    def freeze(self):
        self._frozen=None

    def __setattr__(self,name,value):
        if hasattr(self,'_frozen') and not hasattr(self,name):
            raise AttributeError
        self.__dict__[name]=value

    def list(self):
        if self.ne > 0:
            print("Edges (edge+1,nodes,isboundr)")
            for i in range(self.ne):
                print(i+1,self.n[:,i],self.isboundr[i])

    def assignbc(self,condfun,valfun,typ=None,faces=None,nodes=None):
        """Assign bc to edges
        """
        print('Assigning BC to edges')
        #def assigner(x, y, z, val):
        #    ret=eval(val)
        #    return ret
        #def myfun(x,y,z):
        #    ret= abs(x)<1.0e-5
        #    return ret

        with stats_module.timer('Done:'):
            if not self.condassign:
                self.icond=numpy.array([False]*self.ne,dtype=bool,order='F')
                self.vcond=numpy.array([0]*self.ne,dtype=float,order='F')
                self.condassign=True
            iflag=numpy.array([0]*self.ne,dtype=int,order='F')
            icond1=numpy.array([False]*self.ne,dtype=bool,order='F')
            icond2=numpy.array([False]*self.ne,dtype=bool,order='F')
            if typ=='boundary' or typ=='boundaryflux':
                idx=numpy.where(self.isboundr==1)
            else:
                f90_module.edgeflag(self.n,self.ne,nodes.n,iflag,condfun)
                idx=numpy.where(iflag==2)
            icond1[idx]=True
            idx=numpy.where(icond1)
            self.icond[idx]=True
            if typ=='boundaryflux':
                self.boundarytree()
                f90_module.edgeassignflux(self.istree,self.vcond,faces.n,faces.e,faces.isboundr,nodes.n,valfun,self.localsiz,self.ne,faces.localsiz,faces.nf,nodes.nn)
            else:
                f90_module.edgeassign(self.n,n,icond1,self.vcond,valfun)
                
            idx=numpy.where(icond1)
            print('Number of assigned edges:',len(idx[0]))
            idx=numpy.where(self.icond)
            print('Number of total assigned edges:',len(idx[0]))

    def boundarytree(self,verbose=False):
        with stats_module.timer('Boundary tree creation:'):
            self.istree=numpy.zeros((self.localsiz),dtype=int,order='F')
            f90_module.edgtreequick(self.n,self.isboundr,self.istree)
            
