import numpy
import h5py
import sys
from ..util import stats_module

def dualsave(fnam,M,*args):
    msg='File '+fnam.strip()+'.hdf saved:'
    fnam=fnam+'.hdf'

    with stats_module.timer(msg):
        f = h5py.File(fnam, 'w')
        mesh = f.create_group("mesh")
#       print("write size of volumes:",vv.shape)
        nn=M.nodes.n.transpose()
        nodes = mesh.create_dataset("nodes", data=nn,compression='gzip')
        if(M.faces.nf != None):
            ff=M.faces.n.transpose()-1
            faces = mesh.create_dataset("faces",data=ff,compression='gzip')
            faceregions = mesh.create_dataset("faceregions", data=M.faces.reg,compression='gzip')
        if(M.nodes.pot.size != None):
            if(numpy.iscomplexobj(M.nodes.pot)):
                potential = mesh.create_dataset("potentials", data=M.nodes.pot.real,compression='gzip')
                potentialimag = mesh.create_dataset("potentialsimag", data=M.nodes.pot.imag,compression='gzip')
            else:
                potential = mesh.create_dataset("potentials", data=M.nodes.pot,compression='gzip')
        if(M.edges != None):
            if(M.edges.pot != None):
                if(numpy.iscomplexobj(M.edges.pot)):
                    edgepotential = mesh.create_dataset("edgepotentials", data=M.edges.pot.real,compression='gzip')
                    edgepotentialimag = mesh.create_dataset("edgepotentialsimag", data=M.edges.pot.imag,compression='gzip')
                else:
                    edgepotential = mesh.create_dataset("edgepotentials", data=M.edges.pot,compression='gzip')
                
#           print("write size of potentials:",potential.shape)
  
        if(M.faces.vec.size != None):
            vecvec=M.faces.vec.transpose()
            if(numpy.iscomplexobj(vecvec)):
                vectors = mesh.create_dataset("vectors", data=vecvec.real,compression='gzip')
                vectorsimag = mesh.create_dataset("vectorsimag", data=vecvec.imag,compression='gzip')
            else:
                vectors = mesh.create_dataset("vectors", data=vecvec,compression='gzip')
#                       print("write size of vectors:",vecvec.shape)
#now additional payload
        for arg in args:
            if(arg['type'] != 'node' and arg['type'] != 'cell'):
                sys.exit('Only nodal (node) and element (cell) tables allowed in meshio')
            else:
                thedata=arg['data']
                stuff = mesh.create_dataset(arg['name'], data=thedata,compression='gzip')
            #print('additional payload',thedata)
        f.close()

