import numpy
import sys
import os.path
from . import meshio_module
from . import nodes_module
from . import faces_module
from . import edges_module
from ..fortran import f90_module
from ..util import stats_module

class mesh_class:
	"""Basic datastructure.

	filename -- name of file or Auto
	np -- number of points
	ne -- number of edges
	nf -- number of faces
	nv -- number of volumes
	nodes -- point coordinates
	volumes -- volume nodal connectivity
	voltypes -- for each element its type
	edges -- edge nodal connectivity
	"""

	def __init__(self,name="Unknown",filename=None,order=None,verbose=True,sep=";",dimension=None):
		"""Optional parameters:

		name  -- mesh object name
		filename -- filename to read 
		
		"""
		self.dimension=None
		self.order=order
		self.nodes= nodes_module.nodes_class()
		self.faces= faces_module.faces_class()
		self.edges= edges_module.edges_class()
		warn=False
		self.name=name
		if filename is None:
			self.filename="None"
		else:
			if os.path.exists(filename):
				ext= meshio_module.validatextension(filename)
				if ext is None:
					warn=True
					self.filename="None"
				else:
					self.readmesh(filename,sep,dimension)
					self.filename=filename
			else:
				warn=True
				self.filename="None"
				print("WARNING: file",filename,"does not exist")
		if warn:
			print("Created mesh object",self.name,"with warnings")
		else:
			if(verbose):
				print("Created mesh object",self.name,"without warnings")	
		self.freeze()

	def freeze(self):
		self._frozen=None

	def __setattr__(self,name,value):
		if hasattr(self,'_frozen') and not hasattr(self,name):
			raise AttributeError
		self.__dict__[name]=value    
	
	def list(self):
		"""Print important mesh statistics
		"""
		print("------------------------------")
		print("Mesh object",self.name)
		print("Filename:",self.filename)
		print("Dimension:",self.dimension)
		print("Number of points:",self.nodes.nn)
		print("Number of edges:",self.edges.ne)
		print("Number of faces:",self.faces.nf)
		if self.nodes.pot is not None:
			print("Number of potentials:",self.nodes.pot.shape[0])
		print("------------------------------")
		print(" ")


	def fieldfrompot(self,selectfun,typ,verbose=True):
		"""Generate cell fields
		"""
		with stats_module.timer("Created cell fields:", verbose):
			if not typ in ['grad','curl']:
				print('Invalid field type ',typ,' valid are grad,curl')
			else:
				if typ=='grad':
					if(numpy.iscomplexobj(self.nodes.pot)):
						grad=numpy.empty((3,self.volumes.nv),dtype=complex,order='F')
						f90_module.cellgradcmplx(self.volumes.n,self.volumes.reg,self.nodes.n,self.nodes.pot,grad,selectfun)
						self.volumes.vec=grad
					else:
						if(self.dimension==3):
							grad=numpy.empty((3,self.volumes.nv),dtype=float,order='F')
							f90_module.cellgrad(self.volumes.n,self.volumes.reg,self.nodes.n,self.nodes.pot,grad,selectfun)
							self.volumes.vec=grad
						else:
							grad=numpy.empty((2,self.faces.nf),dtype=float,order='F')
							coord=self.nodes.n[0:2,:]
							f90_module.cellgrad2d(self.faces.n,self.faces.reg,coord,self.nodes.pot,grad,selectfun)
							self.faces.vec=grad
				else:
					if(numpy.iscomplexobj(self.edges.pot)):					
						curl=numpy.empty((3,self.volumes.nv),dtype=complex,order='F')
						f90_module.cellcurlcmplx(self.volumes.n,self.volumes.reg,self.nodes.n,self.edges.n,self.edges.eht,self.edges.elinks,self.edges.pot,curl,self.edges.htpowersiz,selectfun)
						self.volumes.vec=curl
					else:
						curl=numpy.empty((3,self.volumes.nv),dtype=float,order='F')
						f90_module.cellcurl(self.volumes.n,self.volumes.reg,self.nodes.n,self.edges.n,self.edges.eht,self.edges.elinks,self.edges.pot,curl,self.edges.htpowersiz,selectfun)
						self.volumes.vec=curl

	def meshfix(self,verbose=True):
		"""Enforce right hand rule
		"""
		if(self.dimension==None):
			print('Dimension not set')
			sys.exit('Fatal in meshfix.')
		if(self.dimension==2):
			f90_module.meshfix2d(self.faces.n,self.nodes.n)
		else:
			f90_module.meshfix(self.volumes.n,self.nodes.n)

