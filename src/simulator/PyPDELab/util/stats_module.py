import time

class timer():
    """Optional parameters:
        
        message  -- to be printed on exit
        
    """
    
    def __init__(self,message=None,verbose=True):
        self.msg=message
        self.verbose=verbose
    def __enter__(self): self.start = time.time()
    def __exit__(self,*args):
        if(self.verbose):
            print (self.msg,time.time() - self.start,"seconds")
