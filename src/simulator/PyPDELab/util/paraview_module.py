import numpy
import sys
from . import stats_module

class XMF_XML_Serial_Unstructured:
	"""
	"""
	def __init__(self):
		self.fileNames = []

	def write(self, fnam, mesh,*args):
		"""
        """
		if(mesh.dimension==None):
			print('Dimension not set')
			sys.exit('Fatal.')
		fileName=fnam+'.xmf'
		msg='File '+fileName.strip()+' saved:'
		with stats_module.timer(msg):
			import xml.dom.minidom

			doc = xml.dom.minidom.Document()
			root_element = doc.createElementNS("VTK", "Xdmf")
			doc.appendChild(root_element)

			domain = doc.createElementNS("VTK", "Domain")
			root_element.appendChild(domain)

			grid = doc.createElementNS("VTK", "Grid")
			grid.setAttribute("Name", "Mesh_Grid")
			domain.appendChild(grid)

# Tets -------------------------------------------------------------------------
			if(mesh.dimension==3):
				topology = doc.createElementNS("VTK", "Topology")
				topology.setAttribute("TopologyType", "Tetrahedron")
				nt=str(mesh.volumes.nv)
				topology.setAttribute("NumberOfElements",nt)
				grid.appendChild(topology)
			
				topodataitem = doc.createElementNS("VTK", "DataItem")
				topodataitem.setAttribute("DataType", "Int")
				dim=nt+' 4'
				topodataitem.setAttribute("Dimensions",dim)
				topodataitem.setAttribute("Format","HDF")
				topology.appendChild(topodataitem)
				
				topostring=fnam+'.hdf:/mesh/volumes'
				topodata = doc.createTextNode(topostring)
				topodataitem.appendChild(topodata)

# Triangles -------------------------------------------------------------------------
			if(mesh.dimension==2):
				topology = doc.createElementNS("VTK", "Topology")
				topology.setAttribute("TopologyType", "Triangle")
				nf=str(mesh.faces.nf)
				topology.setAttribute("NumberOfElements",nf)
				grid.appendChild(topology)
				
				topodataitem = doc.createElementNS("VTK", "DataItem")
				topodataitem.setAttribute("DataType", "Int")
				dim=nf+' 3'
				topodataitem.setAttribute("Dimensions",dim)
				topodataitem.setAttribute("Format","HDF")
				topology.appendChild(topodataitem)
	
				topostring=fnam+'.hdf:/mesh/faces'
				topodata = doc.createTextNode(topostring)
				topodataitem.appendChild(topodata)
				
# Nodes -------------------------------------------------------------------------
			geometry = doc.createElementNS("VTK", "Geometry")
			geometry.setAttribute("GeometryType", "XYZ")
			grid.appendChild(geometry)

			geodataitem = doc.createElementNS("VTK", "DataItem")
			geodataitem.setAttribute("DataType", "Float")
			np=str(mesh.nodes.nn)
			dim=np+' 3'
			geodataitem.setAttribute("Dimensions",dim)
			geodataitem.setAttribute("Format","HDF")
			geometry.appendChild(geodataitem)

			geostring=fnam+'.hdf:/mesh/nodes'
			geodata = doc.createTextNode(geostring)
			geodataitem.appendChild(geodata)
			

# Potential -------------------------------------------------------------------------
			attribute = doc.createElementNS("VTK", "Attribute")
			attribute.setAttribute("Center", "Node")
			attribute.setAttribute("Name", "Potential")
			grid.appendChild(attribute)

			attrdataitem = doc.createElementNS("VTK", "DataItem")
			attrdataitem.setAttribute("DataType", "Float")
			attrdataitem.setAttribute("Dimensions",np)
			attrdataitem.setAttribute("Format","HDF")
			attribute.appendChild(attrdataitem)

			attrstring=fnam+'.hdf:/mesh/potentials'
			attrdata = doc.createTextNode(attrstring)
			attrdataitem.appendChild(attrdata)
			
			if(numpy.iscomplexobj(mesh.nodes.pot)):
				attribute = doc.createElementNS("VTK", "Attribute")
				attribute.setAttribute("Center", "Node")
				attribute.setAttribute("Name", "PotentialImag")
				grid.appendChild(attribute)
	
				attrdataitem = doc.createElementNS("VTK", "DataItem")
				attrdataitem.setAttribute("DataType", "Float")
				attrdataitem.setAttribute("Dimensions",np)
				attrdataitem.setAttribute("Format","HDF")
				attribute.appendChild(attrdataitem)
	
				attrstring=fnam+'.hdf:/mesh/potentialsimag'
				attrdata = doc.createTextNode(attrstring)
				attrdataitem.appendChild(attrdata)				
#now additional nodal payload
			for arg in args:
				if(arg['type']=='node'):
					attribute = doc.createElementNS("VTK", "Attribute")
					attribute.setAttribute("Center", "Node")
					attribute.setAttribute("Name", arg['name'])
					grid.appendChild(attribute)
					attrdataitem = doc.createElementNS("VTK", "DataItem")
					attrdataitem.setAttribute("DataType", "Float")
					attrdataitem.setAttribute("Dimensions",np)
					attrdataitem.setAttribute("Format","HDF")
					attribute.appendChild(attrdataitem)
					attrstring=fnam+'.hdf:/mesh/'+arg['name']
					attrdata = doc.createTextNode(attrstring)
					attrdataitem.appendChild(attrdata)
				
# Field -------------------------------------------------------------------------
			attribute = doc.createElementNS("VTK", "Attribute")
			attribute.setAttribute("Center", "Cell")
			attribute.setAttribute("AttributeType", "Vector")
			attribute.setAttribute("Name", "Vectors")
			grid.appendChild(attribute)

			attrdataitem = doc.createElementNS("VTK", "DataItem")
			attrdataitem.setAttribute("DataType", "Float")
			if(mesh.dimension==3):
				dim=nt+' 3'
			else:
				dim=nf+' 2'
			attrdataitem.setAttribute("Dimensions",dim)
			attrdataitem.setAttribute("Format","HDF")
			attribute.appendChild(attrdataitem)

			attrstring=fnam+'.hdf:/mesh/vectors'
			attrdata = doc.createTextNode(attrstring)
			attrdataitem.appendChild(attrdata)

# Regions -------------------------------------------------------------------------
			attribute = doc.createElementNS("VTK", "Attribute")
			attribute.setAttribute("Center", "Cell")
			attribute.setAttribute("Name", "Regions")
			grid.appendChild(attribute)

			attrdataitem = doc.createElementNS("VTK", "DataItem")
			attrdataitem.setAttribute("DataType", "Int")
			if(mesh.dimension==3):
				dim=nt
			else:
				dim=nf
			attrdataitem.setAttribute("Dimensions",dim)
			attrdataitem.setAttribute("Format","HDF")
			attribute.appendChild(attrdataitem)
			
			if(mesh.dimension==3):
				attrstring=fnam+'.hdf:/mesh/regions'
			else:
				attrstring=fnam+'.hdf:/mesh/faceregions'
			attrdata = doc.createTextNode(attrstring)
			attrdataitem.appendChild(attrdata)


#now additional cell payload
			for arg in args:
				if(arg['type']=='cell'):
					attribute = doc.createElementNS("VTK", "Attribute")
					attribute.setAttribute("Center", "Cell")
					attribute.setAttribute("Name", arg['name'])
					grid.appendChild(attribute)
					attrdataitem = doc.createElementNS("VTK", "DataItem")
					attrdataitem.setAttribute("DataType", "Float")
					if(mesh.dimension==3):
						dim=nt
					else:
						dim=nf
					attrdataitem.setAttribute("Dimensions",dim)
					attrdataitem.setAttribute("Format","HDF")
					attribute.appendChild(attrdataitem)
					attrstring=fnam+'.hdf:/mesh/'+arg['name']
					attrdata = doc.createTextNode(attrstring)
					attrdataitem.appendChild(attrdata)
			
#write XML file and close					
			outFile = open(fileName, 'w')
			doc.writexml(outFile, newl='\n')
			outFile.close()
