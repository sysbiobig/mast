from ..classes import mesh_module
from . import stats_module
import numpy 

def quadgen2d(name="Unknown",nx=10,ny=10,xmin=0.0, xmax=1.0,ymin=0.0, ymax=1.0,verbose=True):
	"""Create a quadrilateral mesh on the (unit) square.
	Keyword arguments:
	name -- mesh object name 
	nx,ny,nz -- number of points along axes (default=10)
	xmin,xmax (idem for y and z) -- bounds
	"""
	if(nx<2 or ny<2):
		raise ValueError('nx and ny must be >1')
	theverb=verbose
	with stats_module.timer("Square meshing:", verbose=theverb):
		mname=name
		H= mesh_module.mesh_class(name=mname, verbose=theverb)
		H.nodes.nn=nx*ny
		H.faces.nx=nx
		H.faces.ny=ny
		H.faces.nf=(nx-1)*(ny-1)
		nn=numpy.arange(H.nodes.nn)+1
		nn=numpy.reshape(nn,[nx,ny],order='F')
		H.faces.n=numpy.empty((4,H.faces.nf),dtype=numpy.int64,order='F')
		H.faces.reg=numpy.empty((H.faces.nf),dtype=numpy.int64,order='F')
		H.faces.n[0:1,:] = numpy.reshape(nn[0:nx-1,0:ny-1],[1,H.faces.nf],order='F')
		H.faces.n[1:2,:] = numpy.reshape(nn[1:nx,0:ny-1],[1,H.faces.nf],order='F')
		H.faces.n[2:3,:] = numpy.reshape(nn[1:nx,1:ny],[1,H.faces.nf],order='F')
		H.faces.n[3:4,:] = numpy.reshape(nn[0:nx-1,1:ny],[1,H.faces.nf],order='F')
		H.faces.reg = numpy.reshape([1]*H.faces.nf,H.faces.nf,order='F')
		H.faces.ftypes=['Quad']*H.faces.nf
		coo=numpy.mgrid[xmin:xmax:complex(0,nx),ymin:ymax:complex(0,ny)]
		H.nodes.n=numpy.empty((3,H.nodes.nn),dtype=float,order='F')
		H.nodes.n[0:1,:]=numpy.reshape(coo[0:1,:,:],[1,H.nodes.nn],order='F')
		H.nodes.n[1:2,:]=numpy.reshape(coo[1:2,:,:],[1,H.nodes.nn],order='F')
		H.nodes.n[2:3,:]=0.0
		H.dimension=2
	return H

def quad2tri(H,verbose=True):
	theverb=verbose
	with stats_module.timer("Conversion to triangles:", verbose=theverb):
		T= mesh_module.mesh_class(name=H.name, verbose=theverb)
		T.nodes.nn=H.nodes.nn
		T.faces.nf=H.faces.nf*2
		T.nodes.n=H.nodes.n
		T.filename=H.filename
		T.faces.n=numpy.empty((3,T.faces.nf),dtype=int,order='F')
		#T.faces.n=numpy.reshape(H.faces.n[[0,1,2,0,2,3],:],[3,T.faces.nf],order='F')	
		for i in range(1,H.faces.nx):
			if (i % 2 == 0):
				ieven=True
			else:
				ieven=False
			for j in range(1,H.faces.ny):
				idxq=(i-1)*(H.faces.nx-1)+j-1
				if (j % 2 == 0):
					jeven=True
				else:
					jeven=False
				if((ieven and jeven) or (not ieven and not jeven)):
					T.faces.n[0:3,idxq*2:idxq*2+2]=numpy.reshape(H.faces.n[[0,1,2,0,2,3],idxq:idxq+1],[3,2],order='F')	
				else:
					T.faces.n[0:3,idxq*2:idxq*2+2]=numpy.reshape(H.faces.n[[0,1,3,1,2,3],idxq:idxq+1],[3,2],order='F')	
		T.faces.ftypes=['Tri']*T.faces.nf
		T.faces.reg=numpy.reshape([H.faces.reg]*2,H.faces.nf*2,order='F')
		T.dimension=H.dimension
		T.nodes.nnori=T.nodes.nn
	return T
