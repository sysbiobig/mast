subroutine meshfix2d(facs,coords,nf,np)
implicit none

integer*8::facs(3,nf)
double precision::coords(3,np)
integer nf,np
!f2py intent(in,out) facs,coords

call meshfix2dfast(facs,coords,nf,np)

end subroutine
