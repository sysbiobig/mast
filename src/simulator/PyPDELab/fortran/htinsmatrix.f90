subroutine htinsmatrix ( ind, ab, htpowersiz,mat, matht, matlinks )

  implicit none

  integer*8 ab(2)
  integer*8 ind,htpowersiz
  integer*8 mat(2,*)
  integer*8 matht(*),matlinks(*)

  integer*8 k,seed
  integer*8 len
  
  integer*8 fhash
  external fhash
  
  len=2 
  
  k=fhash(ab, len, seed,htpowersiz)
  mat(1:2,ind)=ab(1:2)
  matlinks(ind) = matht(k)
  matht(k) = ind

end
