subroutine cellgrad2d(facs,reg,coords,pot,grad,nf,np,theselfun)
implicit none

integer*8::facs(3,nf),reg(nf)
double precision::coords(2,np),grad(2,nf),pot(np)
integer*8 nf,np
!f2py intent(in,out) facs,coords,pot,grad

integer*8 i,j
double precision v(2,3),gradlambda(2,3),norm(2,3),area,leng(3)
integer*8 nd(3)
double precision, dimension(np):: fakef,fakekappa
double precision, allocatable, dimension(:):: thekappa,sel
external theselfun

!what follows is a dirty trick for f2py
!f2py intent (out) fakef
call theselfun(fakef,fakekappa,np)

allocate(sel(nf))
allocate(thekappa(nf))
do i=1,nf
  thekappa(i)=reg(i)
enddo

!compute selection function in the triangles
!f2py intent (out) sel
call theselfun(sel,thekappa,nf)

grad(:,:)=0.0d0

do i=1,nf
  if(sel(i).gt.0)then
  nd(1:3)=facs(1:3,i)
  v(1:2,1:3)=coords(1:2,nd(1:3))
  call trinorm(v,gradlambda,norm,leng,area)
  do j=1,3
    grad(1:2,i)=grad(1:2,i)+gradlambda(1:2,j)*pot(nd(j))   
  enddo
  endif !on element selection
enddo

end subroutine
