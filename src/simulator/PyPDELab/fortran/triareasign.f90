      subroutine triareasign(v,area)

      implicit none

      double precision v(2,3),area
      double precision r(2),s(2)

      r(:)=v(:,2)-v(:,1)
      s(:)=v(:,3)-v(:,1)

      area=0.5d0*(r(1)*s(2)-r(2)*s(1))

      if(area.eq.0.0d0)then
        write(6,*) 'zero:',area
        write(6,*) 'coords 1:',v(:,1)
        write(6,*) 'coords 2:',v(:,2)
        write(6,*) 'coords 3:',v(:,3)
      endif
      end subroutine
      
