subroutine divgrad2d(facs,reg,coords,mat,vals,rhs,quadorder,locsiz,htpsiz,nf,np,nnz,kappafun)

use integrationrules

implicit none

integer*8::facs(3,nf),reg(nf)
integer*8::mat(2,locsiz)
double precision::vals(locsiz),coords(2,np),rhs(np)
integer*8 nf,locsiz,nnz,np,htpsiz,quadorder

integer*8 i,j,nd(3),nd2(2),ind(3,3),lochtpsiz,loclocsiz
double precision v(2,3),norm(2,3),gradlambda(2,3),area,gwi(2),gwj(2),vv,leng(3)
integer*8,dimension(:)::matht(locsiz),matlinks(locsiz)
double precision, allocatable, dimension(:):: f,thereg,x,y
double precision, allocatable, dimension(:,:):: lamb
integer*8, allocatable, dimension(:,:):: elnods
double precision, dimension(np):: fakef,fakereg,fakex,fakey
double precision, dimension(3,np):: fakelamb
integer*8, dimension(3,np):: fakeelnods

integer*8 ntgp,therule,igp,ip,iel,spacedim
integer*8 htsrcmatrix
external htsrcmatrix
external kappafun
!f2py intent(in,out) facs,irow,icol,vals,coords,rhs 
!f2py intent(inout) nnz

spacedim=2
call rulesinit(spacedim)
!write(6,*) 'in divgrad2d, quadorder=',quadorder
therule=quadorder !max: maxrules+1
!call ruleprint(therule)
ntgp=rules(therule)%npoints*nf

fakereg(:)=1
fakex(:)=0.0d0
fakey(:)=0.0d0
fakelamb(:,:)=0.0d0
fakeelnods(:,:)=1
!what follows is a dirty trick for f2py
!f2py intent (out) fakef
call kappafun(fakef,fakereg,fakex,fakey,fakelamb,fakeelnods,np)

allocate(f(ntgp))
allocate(x(ntgp))
allocate(y(ntgp))
allocate(lamb(3,ntgp))
allocate(elnods(3,ntgp))
allocate(thereg(ntgp))

!first compute thereg,x,y in all Gauss points
ip=0
do iel=1,nf
    nd(1:3)=facs(1:3,iel)
    v(1:2,1:3)=coords(1:2,nd(1:3))
     do igp=1,rules(therule)%npoints
        ip=ip+1
        thereg(ip)=reg(iel)
        x(ip)=dot_product(v(1,1:3),rules(therule)%csi(1:3,igp))
        y(ip)=dot_product(v(2,1:3),rules(therule)%csi(1:3,igp))
        lamb(1:3,ip)=rules(therule)%csi(1:3,igp)
        elnods(1:3,ip)=nd(1:3)
     enddo
enddo

!compute function in the Gauss points
!f2py intent (out) f
call kappafun(f,thereg,x,y,lamb,elnods,ntgp)

!now begin with actual assembly procedure
lochtpsiz=htpsiz+2
loclocsiz=2**lochtpsiz

!allocate(matht(loclocsiz))
!allocate(matlinks(locsiz))

nnz=0
matht(:)=0
matlinks(:)=0

rhs(:)=0.0d0


ip=0
do iel=1,nf
  nd(1:3)=facs(1:3,iel)
  v(1:2,1:3)=coords(1:2,nd(1:3))
  call trinorm(v,gradlambda,norm,leng,area)
  if(area.lt.0.0d0)then
    write(6,*) 'impossible negative area',area
    write(6,*) iel,nd
    stop
  endif
  
  do igp=1,rules(therule)%npoints
     ip=ip+1
     do i=1,3
         nd2(1)=nd(i)
         gwi(1:2)=gradlambda(1:2,i)
         do j=1,3
             nd2(2)=nd(j)
             gwj(1:2)=gradlambda(1:2,j)
             vv=f(ip)*area*dot_product(gwi,gwj)*rules(therule)%w(igp)
!             if(nd2(1).lt.1.or.nd2(2).lt.1)then
!                 write(6,*) 'impossible error1: ',iel,igp,i,j,nd2
!                 stop
!             endif  
             if(igp.eq.1)then
               ind(i,j)=htsrcmatrix ( nd2, htpsiz,mat, matht, matlinks )
             endif
             
             if(ind(i,j).eq.0)then
                 nnz=nnz+1
                 call htinsmatrix( nnz, nd2, htpsiz,mat, matht, matlinks )   
!                 k=htsrcmatrix ( nd2, htpsiz,mat, matht, matlinks )
!                 if(k.ne.nnz)then
!                     write(6,*) 'impossible error3: ',i,j,nd2(:)
!                     stop
!                 endif
!                 if(mat(1,k).lt.1.or.mat(2,k).lt.1)then
!                     write(6,*) 'impossible error4: ',k,mat(:,k)
!                     stop
!                 endif 
                 ind(i,j)=nnz
                 vals(nnz)=vv
             else
                 vals(ind(i,j))=vals(ind(i,j))+vv
             endif
          enddo !enddo on i
       enddo !enddo on j
   enddo !enddo on gp
enddo !enddo on iel

!do i=1,nnz
!write(6,*) i,mat(1,i),mat(2,i)
!  if(mat(1,i).lt.1.or.mat(2,i).lt.1)then
!      write(6,*) 'impossible error2: ',i,mat(:,i)
!      stop
!  endif    
!enddo
    
!deallocate(matht,matlinks)
end subroutine
