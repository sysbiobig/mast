subroutine meshfix2dfast(facs,coords,nf,np)

implicit none

integer*8::facs(3,nf)
double precision::coords(3,np)
integer nf,np

integer i,j,k
double precision v(2,3),area,minarea,totarea
integer nd(3)
integer ifixed
logical suspicious

ifixed=0
suspicious=.false.
minarea=1.0d38
totarea=0.0d0

do i=1,nf
  nd(1:3)=facs(1:3,i)
  v(1:2,1:3)=coords(1:2,nd(1:3))

  call triareasign(v,area)

  totarea=totarea+abs(area)
  if(abs(area).lt.1.0d-6)then
   suspicious=.true.
   if(abs(area).lt.minarea) minarea=abs(area)
  endif
  if(area.lt.0.0d0)then
    k=facs(3,i)
    facs(3,i)=facs(2,i)
    facs(2,i)=k
    !write(6,*) 'Swapped tri. ',i
    ifixed=ifixed+1
  endif
enddo
if(suspicious)then
!  write(6,*) 'Suspicious minimum area ',minarea
endif
!write(6,*) 'Total mesh area:',totarea
!write(6,*) 'N. of fixed tri.:',ifixed


end subroutine
