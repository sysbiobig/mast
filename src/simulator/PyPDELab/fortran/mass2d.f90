subroutine mass2d(facs,reg,coords,mat,vals,rhs,quadorder,locsiz,htpsiz,nf,np,nnz,thefun)

use integrationrules
use commonstuff

implicit none

integer*8::facs(3,nf),reg(nf)
integer*8::mat(2,locsiz)
double precision::vals(locsiz),coords(2,np),rhs(np)
integer*8 nf,locsiz,nnz,np,htpsiz,quadorder
!f2py intent(in,out) facs,reg,mat,vals,coords,rhs 
!f2py intent(inout) nnz

integer*8 spacedim,i,j,k,nd(3),nd2(2),ind(3,3),lochtpsiz,loclocsiz,therule,igp,iel,ntgp,ip
double precision v(2,3),norm(2,3),gradlambda(2,3),area,vol,gwi(2),gwj(2),vv,leng(3),kappa,lambda(3)
integer*8,allocatable,dimension(:)::matht,matlinks 
double precision, allocatable, dimension(:):: f,thekappa,x,y
double precision, dimension(np):: fakef,fakekappa,fakex,fakey
integer*8 htsrcmatrix
external htsrcmatrix
external thefun

fakex(:)=0.0d0
fakey(:)=0.0d0
!what follows is a dirty trick for f2py
!f2py intent (out) fakef
call thefun(fakef,fakekappa,fakex,fakey,np)

!write(6,*) 'inside fast'
spacedim=2
call rulesinit(spacedim)
therule=quadorder !max: maxrules+1 
ntgp=rules(therule)%npoints*nf

allocate(f(ntgp))
allocate(thekappa(ntgp))
allocate(x(ntgp))
allocate(y(ntgp))
!first compute thereg in all Gauss points
ip=0
do iel=1,nf
    nd(1:3)=facs(1:3,iel)
    v(1:2,1:3)=coords(1:2,nd(1:3))
     do igp=1,rules(therule)%npoints
        ip=ip+1
        thekappa(ip)=reg(iel)
        x(ip)=dot_product(v(1,1:3),rules(therule)%csi(1:3,igp))
        y(ip)=dot_product(v(2,1:3),rules(therule)%csi(1:3,igp))
     enddo
enddo
!compute material function in the Gauss points
!f2py intent (out) f
call thefun(f,thekappa,x,y,ntgp)

lochtpsiz=htpsiz+2
loclocsiz=2**lochtpsiz

allocate(matht(loclocsiz))
allocate(matlinks(locsiz))

nnz=0
matht(:)=0
matlinks(:)=0

rhs(:)=0.0d0
ip=0
do i=1,nf
!  write(6,*) 'i=',i
  nd(1:3)=facs(1:3,i)
  v(1:2,1:3)=coords(1:2,nd(1:3))
  
  call trinorm(v,gradlambda,norm,leng,area)
  
  if(area.lt.0.0d0)then
    write(6,*) 'impossible negative area',area
    write(6,*) i,nd
    stop
  endif
  
   do igp=1,rules(therule)%npoints
! write(6,*) 'i,igp=',i,igp
   ip=ip+1
   
   lambda(1:3)=rules(therule)%csi(1:3,igp)
   
  do j=1,3
    nd2(1)=nd(j)
    
    do k=1,3
      nd2(2)=nd(k)
!      write(6,*) 'j,k=',j,k      
      vv=f(ip)*area*lambda(j)*lambda(k)*rules(therule)%w(igp)
      
      if(igp.eq.1)then
!        write(6,*) 'bef htsrcmatrix'
        ind(j,k)=htsrcmatrix ( nd2, lochtpsiz,mat, matht, matlinks )
!        write(6,*) 'aft htsrcmatrix'
      endif
      
      if(ind(j,k).eq.0)then
        nnz=nnz+1
	call htinsmatrix( nnz, nd2, lochtpsiz,mat, matht, matlinks )
        ind(j,k)=nnz
	vals(nnz)=vv
      else
	vals(ind(j,k))=vals(ind(j,k))+vv
      endif
    enddo !enddo on k
    
  enddo !enddo on j

enddo !enddo on igp

enddo !enddo on i

deallocate(matht,matlinks)
deallocate(f,thekappa,x,y)

end subroutine
