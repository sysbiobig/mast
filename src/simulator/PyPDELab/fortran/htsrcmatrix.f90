integer function htsrcmatrix ( ab, htpowersiz,mat, matht, matlinks )
  
  implicit none

  integer*8 ab(2)
  integer*8 htpowersiz
  integer*8 mat(2,*)
  integer*8 matht(*),matlinks(*)
  
  integer*8 ind
  integer*8 k
  integer*8 len,seed
  
  integer*8 fhash
  external fhash
 
  len=2 
  k=fhash(ab, len, seed, htpowersiz)
  
  ind = matht(k)
  
  do
    if ( ind.eq.0 .or. (mat(1,ind).eq.ab(1) .and. mat(2,ind).eq. ab(2)) )then
      exit
    else
      ind = matlinks(ind)
    endif
  end do
  
  htsrcmatrix = ind

end
