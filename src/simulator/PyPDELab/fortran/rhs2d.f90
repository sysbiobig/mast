subroutine rhs2d(facs,reg,coords,rhs,nf,np,thefun)

use integrationrules
use commonstuff

implicit none

integer*8::facs(3,nf),reg(nf)
!integer facsedg(3,nf),
!integer*8::mat(2,locsiz)
double precision::coords(2,np),rhs(np)
!double precision vals(locsiz)
integer nf,np
!integer nnori
!integer nnz,locsiz,htpsiz
!f2py intent(in,out) facs,reg,irow,icol,vals,coords,rhs,facsedg
!f2py intent(inout) nnz

integer*8 spacedim,i,nd(3),igp,iel,therule,ip
!integer ied,k,ei,ej,nd2(2),nd2(2),
double precision v(2,3),gradlambda(2,3),norm(2,3),area,lambda(3),leng(3)
double precision ni
!double precision wsum,vv,vol,source,kappa,gwi(2),gwj(2),
double precision, allocatable, dimension(:):: f,thereg,x,y
double precision, allocatable, dimension(:,:):: lamb
integer*8, allocatable, dimension(:,:):: elnods
double precision, dimension(np):: fakef,fakereg,fakex,fakey
double precision, dimension(3,np):: fakelamb
integer*8, dimension(3,np):: fakeelnods
!integer*8 matht(locsiz),matlinks(locsiz),j,ind(6,6)
integer*8 ntgp
integer htsrcmatrix
external htsrcmatrix
external thefun

spacedim=2
call rulesinit(spacedim)
therule=3 !max: maxrules+1
ntgp=rules(therule)%npoints*nf

fakereg(:)=1
fakex(:)=0.0d0
fakey(:)=0.0d0
fakelamb(:,:)=0.0d0
fakeelnods(:,:)=1
!what follows is a dirty trick for f2py
!f2py intent (out) fakef
call thefun(fakef,fakereg,fakex,fakey,fakelamb,fakeelnods,np)

allocate(f(ntgp))
allocate(x(ntgp))
allocate(y(ntgp))
allocate(lamb(3,ntgp))
allocate(elnods(3,ntgp))
allocate(thereg(ntgp))

!first compute thereg,x,y in all Gauss points
ip=0
do iel=1,nf
    nd(1:3)=facs(1:3,iel)
    v(1:2,1:3)=coords(1:2,nd(1:3))
     do igp=1,rules(therule)%npoints
        ip=ip+1
        thereg(ip)=reg(iel)
        x(ip)=dot_product(v(1,1:3),rules(therule)%csi(1:3,igp))
        y(ip)=dot_product(v(2,1:3),rules(therule)%csi(1:3,igp))
        lamb(1:3,ip)=rules(therule)%csi(1:3,igp)
        elnods(1:3,ip)=nd(1:3)
     enddo
enddo

!compute function in the Gauss points
!f2py intent (out) f
call thefun(f,thereg,x,y,lamb,elnods,ntgp)

!compute rhs
rhs(:)=0.0d0
ip=0
do iel=1,nf
  nd(1:3)=facs(1:3,iel)
  v(1:2,1:3)=coords(1:2,nd(1:3))
  call trinorm(v,gradlambda,norm,leng,area) 
  if(area.lt.0.0d0)then
    write(6,*) 'impossible negative area',area
    write(6,*) iel,nd
    write(6,*) v(:,1)
    write(6,*) v(:,2)
    write(6,*) v(:,3)
    stop
  endif
  do igp=1,rules(therule)%npoints
        ip=ip+1
        lambda(1:3)=rules(therule)%csi(1:3,igp)
        do i=1,3
            ni=lambda(i)
            rhs(nd(i))=rhs(nd(i))+f(ip)*ni*area*rules(therule)%w(igp)
!            write(91,*) f(ip),ni,area,rules(therule)%w(igp)
        enddo
  enddo
enddo

deallocate(f,thereg,x,y,lamb,elnods)

end subroutine
