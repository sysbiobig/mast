subroutine trinorm(v,gradlambda,norm,leng,area)

!from http://www.iue.tuwien.ac.at/phd/nentchev/node25.html

use commonstuff

implicit none

double precision v(2,3),norm(2,3),area,leng(3),gradlambda(2,3)

    double precision r(2),s(2)
    integer i
!    integer permut(3,3) ! node and nodes formin edge
!    data permut / 1,2,3, 2,3,1, 3,1,2 /

    do i=1,3
      
      r(:)=v(:,locfacedg(i,2))-v(:,locfacedg(i,1))
      leng(i)=sqrt(r(1)**2+r(2)**2)
      gradlambda(1,i)=r(2)
      gradlambda(2,i)=-r(1)
      
      norm(1:2,i)=gradlambda(1:2,i)/leng(i) !outgoing normal
      
    enddo
    
    r(:)=v(:,2)-v(:,1)
    s(:)=v(:,3)-v(:,1)

    area=0.5d0*(r(1)*s(2)-r(2)*s(1))

    gradlambda(1:2,1:3)= -gradlambda(1:2,1:3)/(2.0d0*area)
    
    
    end subroutine
    
