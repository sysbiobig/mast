module integrationrules

!Invariant Integration Formulas for the n-Simplex by Combinatorial Methods
!Axel Grundmann and H. M. Moller
!SIAM Journal on Numerical Analysis
!Vol. 15, No. 2 (Apr., 1978), pp. 282-290

implicit none

double precision wei(15),eta1(15),eta2(15),eta3(15),eta4(15)

      data eta1/0.250000000000000d0,0.000000000000000d0,0.333333333333333d0, &
                0.333333333333333d0,0.333333333333333d0,0.727272727272727d0,& 
                0.090909090909090d0,0.090909090909090d0,0.090909090909090d0,& 
                0.433449846426335d0,0.066550153573664d0,0.066550153573664d0,& 
                0.066550153573664d0,0.433449846426335d0,0.433449846426335d0 /

      data eta2/0.250000000000000d0,0.333333333333333d0,0.333333333333333d0, &
                0.333333333333333d0,0.000000000000000d0,0.090909090909090d0,& 
                0.090909090909090d0,0.090909090909090d0,0.727272727272727d0,& 
                0.066550153573664d0,0.433449846426335d0,0.066550153573664d0,& 
                0.433449846426335d0,0.066550153573664d0,0.433449846426335d0 /       
        
      data eta3/0.250000000000000d0,0.333333333333333d0,0.333333333333333d0, &
                0.000000000000000d0,0.333333333333333d0,0.090909090909090d0,& 
                0.090909090909090d0,0.727272727272727d0,0.090909090909090d0,& 
                0.066550153573664d0,0.066550153573664d0,0.433449846426335d0,& 
                0.433449846426335d0,0.433449846426335d0,0.066550153573664d0 /
                  
      data wei/ 0.181702068582535d0,0.036160714285714d0,0.036160714285714d0, &
                0.036160714285714d0,0.036160714285714d0,0.069871494516173d0, &
                0.069871494516173d0,0.069871494516173d0,0.069871494516173d0, &
                0.065694849368318d0,0.065694849368318d0,0.065694849368318d0, &
                0.065694849368318d0,0.065694849368318d0,0.065694849368318d0 /


type rule
  integer*8 dim,degree,npoints !dim: space dimensions
  double precision,allocatable,dimension(:)::w
  double precision,allocatable,dimension(:,:)::csi
end type rule

integer*8,parameter::maxrules=5
integer*8 maxpoints
type(rule)::rules(maxrules+1)


contains

  subroutine rulesprint()
    implicit none
	integer*8 i
	do i=1,maxrules+1
	  call ruleprint(i)
	enddo
  end subroutine
  
  subroutine ruleprint(k)
    implicit none
	integer*8 k,i
	double precision ws
	ws=0.0d0
	write(6,*) 'Rule n.',k,' of dimension ',rules(k)%dim,' of degree ',rules(k)%degree,' has ',rules(k)%npoints,' points'  
    do i=1,rules(k)%npoints
	  write(6,*) i,rules(k)%w(i),rules(k)%csi(:,i)
	  ws=ws+rules(k)%w(i)
	enddo
	write(6,*) 'Sum of weights:',ws
  end subroutine
  
  subroutine rulesend()
    integer*8 i
    if(allocated(rules(1)%csi))then
      do i=1,maxrules+1
        deallocate(rules(i)%w)
        deallocate(rules(i)%csi)
      enddo
    endif
   
  end subroutine

  subroutine rulesinit(dim)
    implicit none
    interface
        double precision function dfact(i)
            integer*8 :: i
        end function
    end interface
    integer*8 j,s,d,sminusi,k,nc,den,nct,dim
    integer*8,allocatable,dimension(:,:):: coeff
    double precision wi,f,sgn,sgn2,sgn3

! if already allocated reallocate all
    if(allocated(rules(1)%csi))then
        call rulesend()
    endif

    do s=0,maxrules
!        write(6,*) 'Initializing rule with s=',s
      nct=0
      rules(s+1)%dim=dim
      d=2*s+1
      rules(s+1)%degree=d
! this first pass is just to find out the number of points
      do k=0,s
        sminusi=s-k
        call betacount(dim,sminusi,nc)
        nct=nct+nc
      enddo
	  allocate(coeff(dim+1,nct))
      rules(s+1)%npoints=nct
      allocate(rules(s+1)%w(nct))
      allocate(rules(s+1)%csi(dim+1,nct))
      nct=0
      do k=s,0,-1
        sgn=(-1.0d0)**k*2.0d0**(-2*s)
    	sgn2=dble((d+dim-2*k))**d
    	sgn3=dfact(k)*dfact(d+dim-k)
        if(dim.eq.1)then
          wi=1.0d0*sgn*sgn2/sgn3
        else if(dim.eq.2)then
          wi=2.0d0*sgn*sgn2/sgn3
        else if(dim.eq.3)then
          wi=6.0d0*sgn*sgn2/sgn3
        else
          write(6,*) 'rulesinit: impossible dimension: ',dim
          stop
        endif
        sminusi=s-k
        call betafind(sminusi,coeff,dim,nc)
        do j=1,nc
    	  nct=nct+1
    	  rules(s+1)%w(nct)=wi
          den=d+dim-2*k
          rules(s+1)%csi(:,nct)=dble(2*coeff(:,j)+1)/den
    	enddo  
      enddo
	  deallocate(coeff)
    enddo
    maxpoints=rules(maxrules+1)%npoints
    !write(6,*) 'Total available rules:',maxrules+1
    !write(6,*) 'Points:',rules(:)%npoints
    !write(6,*) 'Maxpoints:',maxpoints
  
  !dirty hack
  
!  if(dim.eq.3)then
!    rules(3)%w(1:15)=wei(1:15)
!    eta4(:)=eta1(:)+eta2(:)+eta3(:)
!    eta4(:)=1.0d0-eta4(:)    
!    rules(3)%csi(1,1:15)=eta1(1:15)
!    rules(3)%csi(2,1:15)=eta2(1:15)
!    rules(3)%csi(3,1:15)=eta3(1:15)
!    rules(3)%csi(4,1:15)=eta4(1:15)
!  endif
  
  end subroutine

  subroutine betafind(sminusi,coeff,dim,nc)
  implicit none
  integer*8 sminusi,dim,nct,coeff(dim+1,*),i,j,k,l,nc
!write(6,*) 'betafind:',beta
  nc=0
  if(dim.eq.1)then
    do i=0,sminusi
      do j=0,sminusi
	    if(i+j.eq.sminusi)then
	      nc=nc+1
		  coeff(1,nc)=i
		  coeff(2,nc)=j
	    endif
      enddo
    enddo
  else if(dim.eq.2)then
    do i=0,sminusi
      do j=0,sminusi
        do k=0,sminusi
	    if(i+j+k.eq.sminusi)then
	      nc=nc+1
		  coeff(1,nc)=i
		  coeff(2,nc)=j
		  coeff(3,nc)=k
	    endif
	  enddo
      enddo
    enddo
   else if(dim.eq.3)then
    do i=0,sminusi
      do j=0,sminusi
        do k=0,sminusi
          do l=0,sminusi
	    if(i+j+k+l.eq.sminusi)then
	      nc=nc+1
		  coeff(1,nc)=i
		  coeff(2,nc)=j
		  coeff(3,nc)=k
          coeff(4,nc)=l
	    endif
            enddo
	  enddo
      enddo
    enddo
   else
      write(6,*) 'betafind: impossible dim: ',dim
      stop
   endif
!   write(6,*) 'betafind: looking for |beta|=',sminusi
!   do i=1,nc
!       write(6,*) coeff(1:dim+1,i)
!   enddo
  end subroutine
  
  subroutine betacount(dim,sminusi,nc)
  implicit none
  integer*8 sminusi,i,j,k,l,nc,dim
  nc=0
  if(dim.eq.1)then
    do i=0,sminusi
      do j=0,sminusi
  	    if(i+j.eq.sminusi)then
  	      nc=nc+1
  	    endif
      enddo
    enddo
  else if(dim.eq.2)then
  do i=0,sminusi
    do j=0,sminusi
      do k=0,sminusi
	    if(i+j+k.eq.sminusi)then
	      nc=nc+1
	    endif
	  enddo
    enddo
  enddo
  else if(dim.eq.3)then
  do i=0,sminusi
    do j=0,sminusi
      do k=0,sminusi
        do l=0,sminusi
	    if(i+j+k+l.eq.sminusi)then
	      nc=nc+1
	    endif
            enddo
	  enddo
    enddo
  enddo
  else
      write(6,*) 'betacount: impossible dim: ',dim
      stop
  endif
!  write(6,*) 'betacount: nct=',nc
  end subroutine
  
  subroutine adaptint2d(f,coord,tolin,tolout,irule,conv,integ)
    implicit none
	interface
      double precision function f(coord,csi)
        double precision coord(3,3),csi(3)
      end function
    end interface
    double precision coord(3,3),integ,tolin,tolout,previnteg
	double precision buffer(rules(maxrules+1)%npoints)
	integer*8 i,k,irule,prevnpoints
	logical conv
	conv=.false.
	previnteg=0.0d0
	prevnpoints=0
	do i=1,maxrules+1
	  irule=i
	  integ=0.0d0
	  do k=prevnpoints+1,rules(i)%npoints
	    buffer(k)=f(coord,rules(i)%csi(:,k))
	  enddo
	  do k=1,rules(i)%npoints
	    integ=integ+rules(i)%w(k)*buffer(k)
	  enddo
	  tolout=abs((integ-previnteg)/integ)
	  if(tolout.lt.tolin)then
		conv=.true.
		write(6,*) 'CONVERGENCE - Rule:',i,' order:',rules(i)%degree,'Integral:',integ,' Tol:',tolout
		return
	  endif
	  previnteg=integ
	  prevnpoints=rules(i)%npoints
	enddo
	write(6,*) 'NO CONVERGENCE - Rule:',i,' order:',rules(i)%degree,'Integral:',integ,' Tol:',tolout
  end subroutine

  
end module
