integer function fhash(arr,siz,seed,cut)
implicit none
integer*8 arr(*),siz,seed,cut

integer*8 i,shft
!FNV hash

fhash = 1966136261

do i=1,siz
   fhash=ieor(fhash*16777619,arr(i))
enddo

shft=ishft(1,cut)-1
fhash=iand(fhash,shft)+1

!write(6,*) 'fhash:',arr(1:siz),fhash,cut

end function

