import math
import os
import sys

import matplotlib.gridspec as gs
import numpy as np
import seaborn as sns
import random
import string

from matplotlib import pyplot as plt, animation
from matplotlib.colors import LinearSegmentedColormap

from src.handler import CSVHandler
from datetime import datetime


def add_value_labels(ax, max_value, spacing=5):
    """Add labels to the end of each bar in a bar chart.

    Arguments:
        ax (matplotlib.axes.Axes): The matplotlib object containing the axes of the plot to annotate.
        max_value (int): The maximum value that should be labelled.
        spacing (int): The distance between the labels and the bars.
    """

    # For each bar: Place a label
    for rect in ax.patches:
        # Get X and Y placement of label from rect.
        y_value = rect.get_height()
        x_value = rect.get_x() + rect.get_width() / 2

        # Number of points between bar and label. Change to your liking.
        space = spacing
        # Vertical alignment for positive values
        va = 'bottom'

        # If value of bar is negative: Place label below bar
        if y_value < 0:
            # Invert space to place label below
            space *= -1
            # Vertically align label at top
            va = 'top'

        # Use Y value as label and format number with one decimal place
        label = "{}".format('' if y_value == max_value else y_value)

        # Create annotation
        ax.annotate(
            label,  # Use `label` as label
            (x_value, y_value),  # Place label at end of the bar
            xytext=(0, space),  # Vertically shift label by `space`
            textcoords="offset points",  # Interpret `xytext` as offset in points
            rotation=90,
            fontsize=7,
            ha='center',  # Horizontally center label
            va=va)  # Vertically align label differently for positive and negative values.


class Plotter:

    def __init__(self, parameters: dict, environment: str, systems: list, num_instances: int, num_simulations: int,
                 plot: str, interactive: bool = True):

        # Parameters
        self._parameters = parameters

        self._w = parameters['width']  # simulation width
        self._h = parameters['height']  # simulation height

        self._environment = environment
        self._systems = sorted(systems)

        self._instances = num_instances
        self._simulations = num_simulations

        self._ticks = self._instances * self._parameters['day_save']

        self._plot_path = plot

        self._colorPalette = {
            'empty': 'white',
            'blood': 'pink',
            'tumor': "black",
            'n_killer': "blue",
            'ctl': "red",
            'pdlp': "cyan",
            'dc': "green",
            'stroma': "yellow",
            'treg': "orange",
            'necro': "grey"
        }

        self._systems_colorPalette = ['orange', 'blue', 'pink', "green"]
        self._comparisons_colorPalette = ['pink', 'black', 'blue', 'red', 'cyan', 'coral', 'green', 'blueviolet',
                                          'yellow', 'orange', 'grey']

        color_map = []
        for value, colour in zip([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], self._colorPalette.values()):
            color_map.append((value / 9.0, colour))

        self._custom_color_map = LinearSegmentedColormap.from_list("custom", color_map)

        self._agents = [
            'tumor',
            'n_killer',
            'ctl',
            'pdlp',
            'dc',
            'stroma',
            'treg',
            'necro'
        ]

        self._agents_with_antigens = [
            'ctl',
            'tumor',
            'pdlp'
        ]

        self._scales = ['lin', 'log']
        self._bases = ['log2', 'log3', 'log5', 'log10']

        self._extensions = ['svg', 'png', 'jpg', 'jpeg', 'pdf']

        # Initialize global dictionary structure
        self._dictionary = {}

        # For each system
        for system in self._systems:

            self._dictionary.update({system: {}})

            self._dictionary[system].update({'mean': {}})
            self._dictionary[system]['mean'].update({'relative': {}})
            self._dictionary[system]['mean'].update({'absolute': {}})

            self._dictionary[system].update({'median': {}})
            self._dictionary[system]['median'].update({'relative': {}})
            self._dictionary[system]['median'].update({'absolute': {}})

            self._dictionary[system].update({'std': {}})
            self._dictionary[system]['std'].update({'relative': {}})
            self._dictionary[system]['std'].update({'absolute': {}})

            self._dictionary[system].update({'max': {}})
            self._dictionary[system]['max'].update({'relative': {}})
            self._dictionary[system]['max'].update({'absolute': {}})

            self._dictionary[system].update({'min': {}})
            self._dictionary[system]['min'].update({'relative': {}})
            self._dictionary[system]['min'].update({'absolute': {}})

            self._dictionary[system].update({'parameter': {}})
            self._dictionary[system]['active'] = []

            # For each simulation
            for sim in range(1, self._simulations + 1):

                self._dictionary[system].update({'sim_' + str(sim): {}})

                # For each day
                for day in range(0, self._instances, 1):
                    self._dictionary[system]['sim_' + str(sim)].update({day: {}})

                    # For each agent
                    for agent in self._agents:
                        self._dictionary[system]['sim_' + str(sim)][day].update({agent: []})

                        self._dictionary[system]['mean']['relative'].update({agent: []})
                        self._dictionary[system]['mean']['absolute'].update({agent: []})

                        self._dictionary[system]['median']['relative'].update({agent: []})
                        self._dictionary[system]['median']['absolute'].update({agent: []})

                        self._dictionary[system]['std']['relative'].update({agent: []})
                        self._dictionary[system]['std']['absolute'].update({agent: []})

                        self._dictionary[system]['max']['relative'].update({agent: []})
                        self._dictionary[system]['max']['absolute'].update({agent: []})

                        self._dictionary[system]['min']['relative'].update({agent: []})
                        self._dictionary[system]['min']['absolute'].update({agent: []})

        # Whenever interactive is True, the plotter will interact with the user
        self._interactive = interactive

    def load(self):

        import warnings
        with warnings.catch_warnings():

            warnings.simplefilter("ignore", category=RuntimeWarning)

            print('\nEnvironment: {}'.format(self._environment))
            print('\nLoading systems ...\n')

            # Populate the dictionary
            for system in self._systems:

                print('[INFO] {}'.format(system.upper()), end=' ')
                start = datetime.now()

                subdir = os.path.join(self._environment, system)

                for sim in range(1, self._simulations + 1):

                    # print('simulation number', sim)
                    pos_file = os.path.join(subdir, '_sim_num_' + str(sim) + '_data_position.csv')
                    ant_file = os.path.join(subdir, '_sim_num_' + str(sim) + '_antigens_position.csv')

                    positions = CSVHandler(pos_file, mode='r').read(
                        nrows=self._instances * 8)  # 1 instance has 8 agents
                    antigens = CSVHandler(ant_file, mode='r').read(nrows=self._instances * 3)  # 1 instance has 3 agents

                    current_day = -1

                    for row in positions:

                        if row[0] == 'tumor':
                            current_day += 1

                        self._dictionary[system]['sim_' + str(sim)][current_day].update({
                            row[0]: [
                                len(row[1:]),
                                [int(k) for k in row[1:]],  # relative positions
                                [],  # antigen list
                                []  # antigen count
                            ]
                        })

                    current_day = -1

                    for row in antigens:

                        agent = row[0]

                        if agent == 'tumor':
                            current_day += 1

                        self._dictionary[system]['sim_' + str(sim)][current_day][agent][2] = [k for k in row[1:]]
                        self._dictionary[system]['sim_' + str(sim)][current_day][agent][3] = self.convert_antigens(
                            [k for k in row[1:]])

                cardinality, empty = self.extract_cardinality(system)

                for day in range(0, self._instances, 1):

                    # Compute mean  and std using nan value
                    for agent in self._agents:
                        self._dictionary[system]['mean']['absolute'][agent].append(
                            np.nanmean(cardinality[system][day][agent]))
                        self._dictionary[system]['median']['absolute'][agent].append(
                            np.nanmedian(cardinality[system][day][agent]))
                        self._dictionary[system]['std']['absolute'][agent].append(
                            np.nanstd(cardinality[system][day][agent]))

                        self._dictionary[system]['max']['absolute'][agent].append(
                            np.nanmax(cardinality[system][day][agent]))
                        self._dictionary[system]['min']['absolute'][agent].append(
                            np.nanmin(cardinality[system][day][agent]))

                        normalized_val = []
                        for i in range(len(cardinality[system][day][agent])):
                            normalized_val.append(cardinality[system][day][agent][i] / empty[system][day][i])

                        self._dictionary[system]['mean']['relative'][agent].append(np.nanmean(normalized_val))
                        self._dictionary[system]['median']['relative'][agent].append(
                            np.nanmedian(cardinality[system][day][agent]))
                        self._dictionary[system]['std']['relative'][agent].append(np.nanstd(normalized_val))

                        self._dictionary[system]['max']['relative'][agent].append(
                            np.nanmax(cardinality[system][day][agent]))
                        self._dictionary[system]['min']['relative'][agent].append(
                            np.nanmin(cardinality[system][day][agent]))

                    self._dictionary[system]['active'].append(
                        np.count_nonzero(~np.isnan(cardinality[system][day]['tumor'])))

                    # Restore mean and std replacing nan with zero
                    for agent in self._agents:
                        self._dictionary[system]['mean']['absolute'][agent] = [0 if np.isnan(x) else x for x in
                                                                               self._dictionary[system]['mean'][
                                                                                   'absolute'][agent]]
                        self._dictionary[system]['median']['absolute'][agent] = [0 if np.isnan(x) else x for x in
                                                                                 self._dictionary[system]['median'][
                                                                                     'absolute'][agent]]
                        self._dictionary[system]['std']['absolute'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['std'][
                                                                                  'absolute'][agent]]

                        self._dictionary[system]['max']['absolute'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['max'][
                                                                                  'absolute'][agent]]
                        self._dictionary[system]['min']['absolute'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['min'][
                                                                                  'absolute'][agent]]

                        self._dictionary[system]['mean']['relative'][agent] = [0 if np.isnan(x) else x for x in
                                                                               self._dictionary[system]['mean'][
                                                                                   'relative'][agent]]
                        self._dictionary[system]['median']['relative'][agent] = [0 if np.isnan(x) else x for x in
                                                                                 self._dictionary[system]['median'][
                                                                                     'relative'][agent]]
                        self._dictionary[system]['std']['relative'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['std'][
                                                                                  'relative'][agent]]

                        self._dictionary[system]['max']['relative'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['max'][
                                                                                  'relative'][agent]]
                        self._dictionary[system]['min']['relative'][agent] = [0 if np.isnan(x) else x for x in
                                                                              self._dictionary[system]['min'][
                                                                                  'relative'][agent]]

                end = datetime.now()
                print('loaded in {}'.format(end - start))

            print('\n' + '-' * 50)

            return self._dictionary

    def save(self, directory: str = '', name: str = 'plot', ext: str = 'png', tight: bool = False):

        if directory == '':
            plot_path = self._plot_path
        else:
            plot_path = os.path.join(self._plot_path, directory)

        if not os.path.exists(plot_path):
            os.makedirs(plot_path)

        x = ''.join(random.choices(string.ascii_letters + string.digits, k=8))

        # Check extension
        if ext in self._extensions:

            filename = '{}_{}.{}'.format(name, x, ext)

            print('Saving: {}'.format(filename))

            if tight:
                plt.savefig(fname=os.path.join(plot_path, filename), format=ext, bbox_inches='tight')
            else:
                plt.savefig(fname=os.path.join(plot_path, filename), format=ext)

        plt.clf()

    def convert_antigens(self, antigens: list):

        list_antigens = {}
        count = {}
        cur_ant = []

        for antigen in antigens:

            if antigen != '':
                cur_ant = sorted(antigen.split('%'))
                # cur_ant = antigen

            if tuple(cur_ant[1:]) not in count.keys():
                # print('Insert :', tuple(cur_ant[1:]))
                count.update({tuple(cur_ant[1:]): 1})
            else:
                count[tuple(cur_ant[1:])] += 1
                # print('Update :', tuple(cur_ant[1:]))

            # if cur_ant not in count.keys():
            #         count.update({cur_ant: 1})
            #     else:
            #         count[cur_ant] += 1

        for key, value in count.items():
            list_antigens.update({key: value})

        return [(k, v) for k, v in list_antigens.items()]

    def convert_positions(self, system: str, sim: str):

        grids = []

        # For each day define a possible grid
        for day in range(0, self._instances, 1):

            # Set all element as zeros (empty)
            grid = np.zeros((1, self._w * self._h), dtype=np.uint8)

            for val, agent in enumerate(self._agents):

                if len(self._dictionary[system][sim][day][agent]) > 0:
                    relative_positions = self._dictionary[system][sim][day][agent][1]

                    if len(relative_positions) > 0:
                        current_position = relative_positions[0]
                        grid[:, current_position] = val + 2

                        for relative_position in relative_positions[1:]:
                            current_position += relative_position
                            grid[:, current_position] = val + 2
                            # print(current_position, ' ---------- ', grid[:, current_position])

            # print(day, ' completed')

            # Reshape in order to obtain a matrix
            grid = grid.reshape((self._w, self._h))

            # Set blood
            grid[0, :] = np.ones((1, self._w), dtype=np.uint8)
            grid[self._h - 1, :] = np.ones((1, self._w), dtype=np.uint8)

            # Appending calculated grid
            grids.append(grid)

        return grids

    def extract_antigens(self, system: str, sim: str):

        # Initialize dictionary
        antigens = {}

        for agent in self._agents_with_antigens:

            antigens.update({agent: {}})

            for day in range(0, self._instances, 1):

                # initialize all clones and update

                if self._dictionary[system][sim][day][agent]:
                    for clone, count in self._dictionary[system][sim][day][agent][3]:
                        antigens[agent].update({clone: [0 for _ in range(self._instances)]})

        # Populate dictionary
        for agent in self._agents_with_antigens:
            for day in range(0, self._instances, 1):

                if self._dictionary[system][sim][day][agent]:
                    for clone, count in self._dictionary[system][sim][day][agent][3]:
                        (antigens[agent][clone])[day] = count

        return antigens

    def extract_day_antigens(self, system: str, sim: str):

        # TODO : collapse dupl and necr class

        # Initialize dictionary
        antigens = {}

        for agent in self._agents_with_antigens:

            antigens.update({agent: {}})

            for day in range(0, self._instances, 1):

                # initialize all clones and update
                antigens[agent].update({day: []})

                if self._dictionary[system][sim][day][agent]:
                    for clone, count in self._dictionary[system][sim][day][agent][3]:
                        antigens[agent][day].append([clone, count])

        return antigens

    def extract_cardinality(self, system: str):

        # Initialize
        cardinality = {}
        empty_cells = {}

        cardinality.update({system: {}})
        empty_cells.update({system: {}})

        for day in range(0, self._instances, 1):
            cardinality[system].update({day: {}})
            empty_cells[system].update({day: []})

            for agent in self._agents:
                cardinality[system][day].update({agent: []})

        # Compute
        for sim in range(1, self._simulations + 1):
            for day in range(0, self._instances, 1):

                empty = self._w * (self._h - 2)

                for agent in self._agents:

                    if not self._dictionary[system]['sim_' + str(sim)][day][agent]:
                        cardinality[system][day][agent].append(np.nan)
                    else:
                        cardinality[system][day][agent].append(
                            self._dictionary[system]['sim_' + str(sim)][day][agent][0])
                        empty -= self._dictionary[system]['sim_' + str(sim)][day][agent][0]

                empty_cells[system][day].append(empty)

        return cardinality, empty_cells

    def select_agents(self, data: [str, list] = 'all'):

        if self._interactive:

            print('\nSelect a set of agents (must be comma-separated or all):', sep='')
            print(*self._agents + ['all'], sep=', ')
            selected_agents = input('\nYour choice: ').lower()

        else:

            selected_agents = data

        # Parsing input string and return a list of agents
        if selected_agents == 'all':

            agents = self._agents

        else:

            agents = [str(x.strip()) for x in selected_agents.split(',')]

            temp_agent = []
            for agent in agents:

                if agent in self._agents:

                    temp_agent.append(agent)

                else:

                    print("[WARNING] - Agent {} not found, skipping.".format(agent.upper()), file=sys.stderr)

            agents = temp_agent

        print('Selected agents: {}'.format(agents))

        return agents

    def select_simulations(self, data: [str, list] = 'all'):

        if self._interactive:

            print('\nSelect a set of simulations (must be comma-separated or all): ')
            for i in range(self._simulations):
                print('{} - {}'.format(i + 1, 'sim_' + str(i + 1)))

            selected_simulations = input('\nYour choice: ').lower()

        else:

            selected_simulations = data

        # Parsing input string and return a list of agents
        if selected_simulations == 'all':
            selected_simulations = ['sim_' + str(x) for x in range(1, self._simulations + 1)]

        elif isinstance(selected_simulations, str):

            selected_simulations = sorted(
                [str(x) if x.startswith('sim_') else 'sim_' + str(x) for x in selected_simulations.split(',')])

            for selected_simulation in selected_simulations:

                if selected_simulation not in ['sim_' + str(x) for x in range(1, self._simulations + 1)]:
                    selected_simulations.remove(selected_simulation)
                    print(
                        "[WARNING] - Simulation {} not found, removing from list.".format(selected_simulation.upper()),
                        file=sys.stderr)

            if not selected_simulations:
                print("[WARNING] - Simulations list is empty, setting default.", file=sys.stderr)
                selected_simulations = ['sim_' + str(x) for x in range(1, self._simulations + 1)]

        print('Simulations selected: {}'.format(selected_simulations))

        return selected_simulations

    def select_systems(self, systems: list, data: [str, list] = 'all'):

        if self._interactive:

            print('\nSelect a set of systems (must be comma-separated or all):')
            for i, system in enumerate(systems):
                print('{} - {}'.format(i + 1, system))

            selected_systems = input('\nYour choice: ').lower()

        else:

            selected_systems = data

        if selected_systems == 'all':

            selected_systems = systems

        else:

            temp_systems = [str(x.strip()) for x in selected_systems.split(',')]

            temp = []
            for system in temp_systems:

                if system in systems:

                    temp.append(system)

                else:

                    try:

                        pos = int(system)
                        temp.append(systems[pos - 1])

                    except IndexError:

                        print("[WARNING] - System {} not found, skipping.".format(system.upper()), file=sys.stderr)
                        continue

                    except ValueError:

                        print("[WARNING] - System {} not found, skipping.".format(system.upper()), file=sys.stderr)
                        continue

            selected_systems = temp

        print('Selected systems: {}'.format(selected_systems))

        return selected_systems

    def select_y_axis_scale(self, data: str = 'lin'):

        if self._interactive:

            print('\nSelect a scale:')
            for i, scale in enumerate(self._scales):
                print('{} - {}'.format(i + 1, scale))

            selected_scale = input('\nYour choice: ').lower()

        else:

            selected_scale = data

        # Check if the selected system is available
        if selected_scale in self._scales:

            temp = selected_scale

        else:

            try:

                pos = int(selected_scale)
                temp = self._scales[pos - 1]

            except IndexError:

                print("[WARNING] - Scale {} not found, setting default.".format(selected_scale.upper()),
                      file=sys.stderr)
                temp = self._scales[0]

            except ValueError:

                print("[WARNING] - Scale {} not found, setting default.".format(selected_scale.upper()),
                      file=sys.stderr)
                temp = self._scales[0]

        selected_scale = temp

        print('Selected scale: {}'.format(selected_scale))

        return selected_scale

    def set_y_axis_scale(self, systems: list, agents: list, metric: str = 'absolute'):
        allowance = 0.1
        maximum = 0
        for system in systems:
            for agent in agents:
                value = self._dictionary[system]['mean'][metric][agent] + self._dictionary[system]['std'][metric][agent]
                if max(value) > maximum:
                    maximum = max(value)

        return maximum + (maximum * allowance)

    def select_log_base(self, data: str = 'log2'):

        if self._interactive:

            print('\nSelect a log-base:')
            for i, base in enumerate(self._bases):
                print('{} - {}'.format(i + 1, base))

            selected_base = input('\nYour choice: ').lower()

        else:

            selected_base = data

        # Check if the selected system is available
        if selected_base in self._bases:

            temp = selected_base

        else:

            try:

                pos = int(selected_base)
                temp = self._bases[pos - 1]

            except IndexError:

                print("[WARNING] - Base {} not found, setting default.".format(selected_base.upper()), file=sys.stderr)
                temp = self._bases[0]

            except ValueError:

                print("[WARNING] - Base {} not found, setting default.".format(selected_base.upper()), file=sys.stderr)
                temp = self._bases[0]

        selected_base = temp

        print('Selected log-base: {} ({})'.format(selected_base, selected_base[3:]))

        # Return the base value
        return int(selected_base[3:])

    def select_extension(self, extension='png'):

        if self._interactive:

            print('\nSelect an output extension:', sep='')
            for i, ext in enumerate(self._extensions):
                print('{} - {}'.format(i + 1, ext))

            selected_extension = input('\nYour choice: ').lower()

        else:

            selected_extension = extension

        # Check if the selected system is available
        if selected_extension in self._extensions:

            temp = selected_extension

        else:

            try:
                pos = int(selected_extension)
                temp = self._extensions[pos - 1]

            except IndexError:

                print("[WARNING] - Extension {} not found, setting default.".format(selected_extension.upper()),
                      file=sys.stderr)
                temp = self._extensions[0]

            except ValueError:

                print("[WARNING] - Extension {} not found, setting default.".format(selected_extension.upper()),
                      file=sys.stderr)
                temp = self._extensions[0]

        selected_extension = temp
        print('Selected extension: {}\n'.format(selected_extension))

        return selected_extension

    def plot_system(self, systems: [str, list], metric: str = 'absolute', system_data: str = '0',
                    agents_data: [str, list] = 'all', scale_data: str = 'lin', base_data: str = 'log2',
                    numeric_index: str = 'mean_std', extension='png'):

        selected_systems = self.select_systems(systems=systems, data=system_data)
        selected_agents = self.select_agents(data=agents_data)
        selected_scale = self.select_y_axis_scale(data=scale_data)

        if selected_scale == 'log':
            selected_base = self.select_log_base(data=base_data)
        else:
            selected_base = None

        ext = self.select_extension(extension=extension)

        for selected_system in selected_systems:

            # Plot
            fig, (ax_top, ax_bot) = plt.subplots(2, 1, figsize=(12, 9), sharex='all',
                                                 gridspec_kw={'height_ratios': [1, 4]})

            y = self.set_y_axis_scale(systems=[selected_system], agents=selected_agents, metric=metric)

            x = range(0, self._ticks, 3)
            ax_top.bar(x, self._dictionary[selected_system]['active'])
            ax_top.set_title('{}'.format(selected_system.upper()), fontsize=15)

            _, y_max = ax_top.get_ylim()
            # add_value_labels(ax_top, max_value=math.floor(y_max))

            ax_top.set_ylabel('NTF')
            ax_top.yaxis.set_label_coords(-0.07, 0.5)

            ax_top.grid(color='lightgray', linestyle='--', axis='y')

            for agent in selected_agents:

                if numeric_index == 'mean_std':

                    mean = None
                    max_std = None
                    min_std = None

                    sum_ops = [m + s for m, s in zip(self._dictionary[selected_system]['mean'][metric][agent],
                                                     self._dictionary[selected_system]['std'][metric][agent])]
                    dif_ops = [m - s for m, s in zip(self._dictionary[selected_system]['mean'][metric][agent],
                                                     self._dictionary[selected_system]['std'][metric][agent])]

                    dif_ops = [np.where(k > 0, k, 0) for k in dif_ops]

                    if selected_scale == 'lin':

                        mean = [val for val in self._dictionary[selected_system]['mean'][metric][agent]]

                        max_std = [val for val in sum_ops]
                        min_std = [val for val in dif_ops]

                    elif selected_scale == 'log':

                        mean = [math.log(val + 1, selected_base) for val in
                                self._dictionary[selected_system]['mean'][metric][agent]]

                        max_std = [math.log(val + 1, selected_base) for val in sum_ops]
                        min_std = [math.log(val + 1, selected_base) for val in dif_ops]

                    ax_bot.plot(x, mean, color=self._colorPalette[agent])
                    ax_bot.scatter(x, mean, color=self._colorPalette[agent])
                    ax_bot.fill_between(x, max_std, min_std, color=self._colorPalette[agent], alpha=0.2)

                elif numeric_index == 'median':

                    median = None

                    if selected_scale == 'lin':

                        median = [val for val in self._dictionary[selected_system]['median'][metric][agent]]

                    elif selected_scale == 'log':

                        median = [math.log(val + 1, selected_base) for val in
                                  self._dictionary[selected_system]['median'][metric][agent]]

                    ax_bot.plot(x, median, color=self._colorPalette[agent])

            # Format legend
            agent_labels = [x.upper() for x in selected_agents]
            ax_bot.legend(agent_labels, title='AGENTS', fontsize=9, loc='upper center', bbox_to_anchor=(0.5, -0.1),
                          ncol=8)
            ax_bot.grid(color='lightgray', linestyle='--')
            ax_bot.set_xlabel('SIMULATION DAYS')
            ax_bot.set_ylabel('CELL COUNTS')
            ax_bot.yaxis.set_label_coords(-0.07, 0.5)

            if selected_scale == 'lin':
                ax_bot.set_ylim(0, y)
            elif selected_scale == 'log':
                ax_bot.set_ylim(0, math.log(y, selected_base))

            fig.tight_layout()

            # Save figure
            self.save(directory=selected_system, name='single_system', ext=ext, tight=True)

            plt.close(fig)

    def plot_agent_comparison(self, systems: list, metric: str = 'absolute', systems_data: [str, list] = 'all',
                              agents_data: str = 'all', extension='png'):

        systems = self.select_systems(systems=systems, data=systems_data)
        agents = self.select_agents(data=agents_data)
        ext = self.select_extension(extension=extension)

        for agent in agents:

            # Plot
            fig, (ax_top, ax_bot) = plt.subplots(2, 1, figsize=(12, 9), sharex='all',
                                                 gridspec_kw={'height_ratios': [1, 4]})

            ax_top.set_title('{} AGENT'.format(agent.upper()), fontsize=20)
            ax_top.grid(color='lightgray', linestyle='--', axis='y')
            ax_top.set_ylabel('NTF')
            ax_top.yaxis.set_label_coords(-0.07, 0.5)

            x = range(0, self._ticks, 3)

            for k, system in enumerate(systems):

                if len(systems) == 4:

                    ax_top.plot(x, self._dictionary[system]['active'], color=self._systems_colorPalette[k])
                    ax_top.scatter(x, self._dictionary[system]['active'], color=self._systems_colorPalette[k])

                    ax_bot.plot(x, self._dictionary[system]['mean'][metric][agent], color=self._systems_colorPalette[k])
                    ax_bot.scatter(x, self._dictionary[system]['mean'][metric][agent],
                                   color=self._systems_colorPalette[k])

                    max_std = np.array(self._dictionary[system]['mean'][metric][agent]) + np.array(
                        self._dictionary[system]['std'][metric][agent])

                    operation = np.array(self._dictionary[system]['mean'][metric][agent]) - np.array(
                        self._dictionary[system]['std'][metric][agent])
                    min_std = np.where(operation > 0, operation, 0)

                    ax_bot.fill_between(x, max_std, min_std, color=self._systems_colorPalette[k], alpha=0.2)

                else:

                    ax_top.plot(x, self._dictionary[system]['active'], color=self._comparisons_colorPalette[k])
                    ax_top.scatter(x, self._dictionary[system]['active'], color=self._comparisons_colorPalette[k])

                    ax_bot.plot(x, self._dictionary[system]['mean'][metric][agent],
                                color=self._comparisons_colorPalette[k])
                    ax_bot.scatter(x, self._dictionary[system]['mean'][metric][agent],
                                   color=self._comparisons_colorPalette[k])

                    max_std = np.array(self._dictionary[system]['mean'][metric][agent]) + np.array(
                        self._dictionary[system]['std'][metric][agent])

                    operation = np.array(self._dictionary[system]['mean'][metric][agent]) - np.array(
                        self._dictionary[system]['std'][metric][agent])
                    min_std = np.where(operation > 0, operation, 0)

                    ax_bot.fill_between(x, max_std, min_std, color=self._comparisons_colorPalette[k], alpha=0.2)

            # Format legend
            system_labels = [x.upper() for x in systems]
            ax_bot.legend(system_labels, title='SYSTEMS', fontsize=9, loc='upper center', bbox_to_anchor=(0.5, -0.1),
                          ncol=8)
            ax_bot.set_xlabel('SIMULATION DAYS')
            ax_bot.set_ylabel('CELL COUNTS')
            ax_bot.grid(color='lightgray', linestyle='--')

            fig.tight_layout()

            # Save figure
            self.save(name='{}_comparison'.format(agent), ext=ext, tight=True)

            plt.close(fig)

    def plot_systems(self, systems: list, metric: str = 'absolute', systems_data: [str, list] = 'all',
                     agents_data: [str, list] = 'all', scale_data: str = 'lin', base_data: str = 'log2',
                     numeric_index: str = 'mean_std', extension='png'):

        selected_systems = self.select_systems(systems=systems, data=systems_data)

        if len(selected_systems) < 2:
            print('[ERROR] You cannot proceed. You MUST select at least two systems', file=sys.stderr)
            return

        selected_agents = self.select_agents(data=agents_data)
        selected_scale = self.select_y_axis_scale(data=scale_data)

        if selected_scale == 'log':
            selected_base = self.select_log_base(data=base_data)
        else:
            selected_base = None

        ext = self.select_extension(extension=extension)

        # Plot
        fig = plt.figure(figsize=(24, 6 * math.ceil(len(selected_systems) / 2)))

        rows = math.ceil(len(selected_systems) / 2)
        cols = 2

        outer_grid = gs.GridSpec(nrows=rows, ncols=cols, figure=fig, wspace=0, hspace=0)

        y = self.set_y_axis_scale(selected_systems, selected_agents, metric)

        for row in range(rows):

            for col in range(cols):

                i = row * cols + col

                if i >= len(selected_systems):
                    break

                inner_grid = outer_grid[row, col].subgridspec(nrows=2, ncols=1, height_ratios=[1, 4], wspace=0, hspace=0.05)
                (ax_top, ax_bot) = inner_grid.subplots()

                x = range(0, self._ticks, 3)
                ax_top.bar(x, self._dictionary[selected_systems[i]]['active'])
                ax_top.set_title('{}'.format(selected_systems[i]), fontsize=15)
                ax_top.set_xticks([])

                _, y_max = ax_top.get_ylim()
                # add_value_labels(ax_top, max_value=math.floor(y_max))

                ax_top.set_ylabel('NTF')
                ax_top.yaxis.set_label_coords(-0.07, 0.5)

                ax_top.grid(color='lightgray', linestyle='--', axis='y')
                fig.add_subplot(ax_top)

                for agent in selected_agents:

                    if numeric_index == 'mean_std':

                        mean = None
                        max_std = None
                        min_std = None

                        sum_ops = [m + s for m, s in zip(self._dictionary[selected_systems[i]]['mean'][metric][agent],
                                                         self._dictionary[selected_systems[i]]['std'][metric][agent])]
                        dif_ops = [m - s for m, s in zip(self._dictionary[selected_systems[i]]['mean'][metric][agent],
                                                         self._dictionary[selected_systems[i]]['std'][metric][agent])]

                        dif_ops = [np.where(k > 0, k, 0) for k in dif_ops]

                        if selected_scale == 'lin':

                            mean = [val for val in self._dictionary[selected_systems[i]]['mean'][metric][agent]]

                            max_std = [val for val in sum_ops]
                            min_std = [val for val in dif_ops]

                        elif selected_scale == 'log':

                            mean = [math.log(val + 1, selected_base) for val in
                                    self._dictionary[selected_systems[i]]['mean'][metric][agent]]

                            max_std = [math.log(val + 1, selected_base) for val in sum_ops]
                            min_std = [math.log(val + 1, selected_base) for val in dif_ops]

                        ax_bot.plot(x, mean, color=self._colorPalette[agent])
                        ax_bot.scatter(x, mean, color=self._colorPalette[agent])
                        ax_bot.fill_between(x, max_std, min_std, color=self._colorPalette[agent], alpha=0.2)

                    elif numeric_index == 'median':

                        median = None

                        if selected_scale == 'lin':

                            median = [val for val in self._dictionary[selected_systems[i]]['median'][metric][agent]]

                        elif selected_scale == 'log':

                            median = [math.log(val + 1, selected_base) for val in
                                      self._dictionary[selected_systems[i]]['median'][metric][agent]]

                        ax_bot.plot(x, median, color=self._colorPalette[agent])

                # Format legend
                agent_labels = [x.upper() for x in selected_agents]
                ax_bot.legend(agent_labels, title='AGENTS', fontsize=9, loc='upper center', bbox_to_anchor=(0.5, -0.1),
                              ncol=8)

                ax_bot.grid(color='lightgray', linestyle='--')
                ax_bot.set_xlabel('SIMULATION DAYS')
                ax_bot.set_ylabel('CELL COUNTS')
                ax_bot.yaxis.set_label_coords(-0.07, 0.5)

                if selected_scale == 'lin':
                    ax_bot.set_ylim(0, y)
                elif selected_scale == 'log':
                    ax_bot.set_ylim(0, math.log(y, selected_base))

                fig.add_subplot(ax_bot)

        outer_grid.tight_layout(fig)

        # Save figure
        self.save(name='systems_comparison', ext=ext, tight=True)
        plt.close(fig)

    def plot_antigens(self, systems: list, systems_data: [str, list] = 'all', simulation_data: str = 'all',
                      extension='png'):

        systems = self.select_systems(systems=systems, data=systems_data)
        ext = self.select_extension(extension=extension)

        for system in systems:

            simulations = self.select_simulations(data=simulation_data)

            for simulation in simulations:

                # Retrieve simulation antigens
                antigens = self.extract_antigens(system, simulation)

                # Plot
                fig = plt.figure(figsize=(18, 12))
                grid = gs.GridSpec(nrows=2, ncols=2, figure=fig)

                ctl_ax = fig.add_subplot(grid[1, :])
                tum_ax = fig.add_subplot(grid[0, 0])
                pdl_ax = fig.add_subplot(grid[0, 1])

                axes = [ctl_ax, tum_ax, pdl_ax]

                x = range(0, self._ticks, 3)

                filtered = {}
                for agent in self._agents_with_antigens:
                    filtered.update({agent: {}})
                    for clone in sorted(antigens[agent]):
                        # print(antigens[agent][clone])
                        if any(count > 1 for count in antigens[agent][clone]):
                            filtered[agent].update({clone: {}})
                            filtered[agent][clone].update({'antigens': []})
                            filtered[agent][clone].update({'rank': []})
                            filtered[agent][clone]['antigens'] = antigens[agent][clone]
                            filtered[agent][clone]['rank'] = np.sum(antigens[agent][clone])

                # Extract top 10 antigens
                for i, agent in enumerate(self._agents_with_antigens):

                    clones = []

                    for (clone, data) in sorted(filtered[agent].items(),
                                                key=lambda item: item[1]['rank'],
                                                reverse=True)[:10]:
                        # print(clone, data)

                        # if any(element >= 10 for element in antigens[agent][clone]):
                        # print(antigens[agent][clone])
                        axes[i].plot(x, data['antigens'])
                        axes[i].scatter(x, data['antigens'])

                        clones.append(' / '.join(clone))

                        if agent == 'ctl':

                            axes[i].legend(clones, title='FREQUENT CLONES', fontsize=9, loc='upper right')

                        else:

                            axes[i].legend(clones, title='FREQUENT CLONES', fontsize=9, loc='upper left')

                    axes[i].set_title('{}'.format(agent.upper()), fontsize=20)
                    axes[i].set_xlabel('SIMULATION DAYS')
                    axes[i].set_ylabel('CLONE COUNTS')
                    axes[i].grid(color='lightgray', linestyle='--')

                fig.tight_layout()

                self.save(directory=system, name='{}_antigens'.format(simulation), tight=True, ext=ext)

                plt.close(fig)

    def plot_frames(self, systems: list, systems_data: [str, list] = 'all', simulation_data: str = 'all',
                    extension='png'):

        systems = self.select_systems(systems=systems, data=systems_data)

        for system in systems:

            simulations = self.select_simulations(data=simulation_data)

            for simulation in simulations:

                grids = self.convert_positions(system, simulation)

                frames = []
                for grid in grids:
                    if np.count_nonzero(grid) > 2 * self._w:
                        frames.append(grid)

                ext = self.select_extension(extension=extension)

                if len(frames) == self._instances:

                    # print('[MESSAGE] - Extracting {} of system {}.'.format(simulation, system))

                    # Plot
                    num_frames = 5
                    for i in range(num_frames):
                        index = len(grids) // num_frames * (i + 1)

                        fig = plt.figure()
                        ax = sns.heatmap(data=grids[index - 1], vmin=0, vmax=9, cmap=self._custom_color_map, cbar=False,
                                         xticklabels=False, yticklabels=False, square=True)
                        ax.set_aspect('equal')

                        # Save figure
                        self.save(directory=system,
                                  name='{}_day_{}'.format(simulation, index * self._parameters['day_save']), tight=True,
                                  ext=ext)

                        plt.close(fig)

                else:

                    print(
                        '[WARNING] - Simulation {} of system {} contains only {}/{} non tumor-free frames. '
                        'Skipped.'.format(simulation, system, len(frames), self._instances), file=sys.stderr)

    def generate_gif(self, systems: list, systems_data: [str, list] = 'all', simulation_data: str = 'all'):

        systems = self.select_systems(systems=systems, data=systems_data)

        for system in systems:

            simulations = self.select_simulations(data=simulation_data)

            for simulation in simulations:

                grids = self.convert_positions(system, simulation)

                frames = []
                for grid in grids:
                    if np.count_nonzero(grid) > 2 * self._w:
                        frames.append(grid)

                # Plot
                writer = animation.ImageMagickWriter(fps=10, metadata=dict(artist='SysBioBig'), bitrate=18000)

                fig = plt.figure(tight_layout=True)

                def init():
                    ax = sns.heatmap(np.zeros((self._w, self._h)), vmin=0, vmax=9, cmap=self._custom_color_map,
                                     cbar=False, square=True)
                    ax.set_aspect('equal')

                def animate(i):
                    ax = sns.heatmap(data=frames[i], vmin=0, vmax=9, cmap=self._custom_color_map, cbar=False,
                                     xticklabels=False, yticklabels=False, square=True)
                    ax.set_aspect('equal')

                fig.tight_layout()

                anim = animation.FuncAnimation(fig, animate, init_func=init, frames=len(frames), repeat=False,
                                               interval=50)

                plot_path = os.path.join(self._plot_path, system)  # system == directory
                if not os.path.exists(plot_path):
                    os.makedirs(plot_path)
                x = ''.join(random.choices(string.ascii_letters + string.digits, k=8))

                # Save GIF

                filename = '{}_{}.gif'.format(simulation, x)
                print('Saving: {}'.format(filename))
                anim.save(os.path.join(plot_path, filename), writer=writer, dpi=150)
