import csv
from itertools import islice
import os
import sys


class CSV:
    """This is a class useful to handle CSV file."""

    def __init__(self, path, mode):
        """CSV initializer

        Parameters
        ----------
        path : str
            It's the absolute path to a specific file
        mode : str
            It's the access mode to the specific file
        """

        try:
            _, file_extension = os.path.splitext(path)
            assert (file_extension in {'.csv'}), "I can only work with .csv file"

        except AssertionError as e:
            print(e, file=sys.stderr)
            exit(1)

        # Initialization of the class parameters
        self._path = path
        self._mode = mode
        self._newline = '\n'
        self._file = None

    def open(self):
        """Open a file"""

        self._file = open(self._path, mode=self._mode, newline=self._newline)

    def read(self, nrows=None):
        """Read a file

        :return: The list of the lines inside the file
        """

        elements = []
        with open(self._path, 'r') as file:

            if nrows is None:

                for element in csv.reader(file):
                    elements.append(element)
            else:

                for element in islice(csv.reader(file), nrows):
                    elements.append(element)

        return elements

    def write(self, elements):
        """Write inside a file"""

        csv.writer(self._file).writerow(elements)

    def close(self):
        """Close a file"""

        self._file.close()
