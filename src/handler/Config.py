import os
import sys


class Config:
    """This is a class useful to handle simulation config file."""

    def __init__(self, path):
        """Config initializer

        Parameters
        ----------
        path : str
            It's the absolute path to a specific file
        """

        try:
            _, file_extension = os.path.splitext(path)
            assert (file_extension in {'.dat', '.sim'}), "I can only work with .dat or .sim file"

        except AssertionError as e:
            print(e, file=sys.stderr)
            exit(1)

        # Initialization of the class parameters
        self._path = path
        self._newline = '\n'

    def read(self, separator):
        """Read a configuration file

        :type separator: str
        :param separator: Character used to split the parameter
        :return : A dictionary containing the simulation parameters
        """

        with open(self._path, mode='r', newline=self._newline) as file:

            elements = {}

            for element in file.read().splitlines():
                try:
                    assert (element.find(':')), "You must insert a parameter as follow: '<name>:<value>"

                    name, value = element.split(separator)

                    tmp = str(value)
                    # Check if a single value is passed (it must be an int or a float value)
                    if tmp.isdigit():
                        elements[name] = int(value)
                    elif tmp.replace('.', '', 1).isdigit() and tmp.count('.') < 2:
                        elements[name] = float(value)

                    # Check if a multiple value are passed (it must be an int or a float value)
                    else:

                        try:

                            element_values = tmp.split(',')
                            elements[name] = []
                            for v in element_values:

                                if v.isdigit():
                                    elements[name].append(int(v))

                        except ValueError:
                            print("List values must be comma separated and integer", file=sys.stderr)

                    # print("Parameter - '{}' : {}".format(name, value))

                except AssertionError as e:
                    print(e, file=sys.stderr)
                    exit(1)

        return elements

    def write(self, elements):
        """Write inside a configuration file

        :type elements: list | dict
        :param elements: Parameters to write
        """

        with open(self._path, mode='w', newline=self._newline) as f:
            f.writelines(elements)
