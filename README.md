# MAST: a hybrid Multi-Agents based Spatio-Temporal model of tumor microenvironment informed using a data-driven approach

Several computational models have been developed for simulating complex systems of interacting cells in the tumor
microenvironment (TME), as a mean toward a better understanding of the tumor-immune dynamics that drive cancer
development.

MAST is a hybrid Multi-Agent Spatio-Temporal model of the interaction between immune system and tumor cells that uses a
data-driven approach to mimic specific TME scenarios. The coupling of a discrete agent-based model with a continuous
partial differential equations-based model allows capturing essential elements of the TME such as the spatio-temporal
dynamics of cancer development, its dependency on nutrient availability, immune recognition, and acquisition of escape
mechanisms through mutations. MAST is informed with bulk and single-cell sequencing data of human colorectal cancer to show how the proposed approach can be used to reproduce emergent properties of TME in four different subtypes of colorectal cancer.

To cite this software please use "MAST: a hybrid Multi-Agent Spatio-Temporal model of tumor microenvironment informed using a data-driven approach", Giulia Cesaro, Mikele Milia, Giacomo Baruzzo, Giovanni Finco, Francesco Morandini, Alessio Lazzarini, Piergiorgio Alotto, Noel Filipe da Cunha Carvalho de Miranda, Zlatko Trajanoski, Francesca Finotello, Barbara Di Camillo, Bioinformatics Advances, Volume 2, Issue 1, 2022, vbac092, https://doi.org/10.1093/bioadv/vbac092

## Structure

The complete project structure is shown and described below:

```graphql
.                                                   # Project folder
├── data/                                           # Data folder
│   ├── paper_bulk/                                 # Example of environment folder
│   │   ├── bulk_CMS1.dat                           # Example of system file
│   │   ├── bulk_CMS2.dat
│   │   ├── bulk_CMS3.dat
│   │   └── bulk_CMS4.dat
│   ├── paper_sc/                                   
│   ├── ...                                         
│   └── setup.sim                                   # General configuration (.sim) file
│
├── output/                                         # Output folder
│   ├── paper_bulk/                                 # Example of environment output folder
│   │   ├── bulk_CMS1/                              # Example of system output folder
│   │   │   ├── _sim_num_1_data_position.csv        # Example of position tabular output
│   │   │   ├── _sim_num_1_antigens_position.csv    # Example of antigens tabular output
│   │   │   └── ...
│   │   ├── bulk_CMS2/
│   │   ├── bulk_CMS3/
│   │   ├── bulk_CMS4/
│   │   └── ...
│   └── ...
│
├── plot/                                           # Plot folder
│   ├── paper_bulk/                                 # Example of environment plot folder
│   │   ├── bulk_CMS1/                              # Example of system plot folder
│   │   │   ├── *.png/jpg files                     # Example of specific graphical output
│   │   │   ├── *.svg/pdf files
│   │   │   └── *.gif files
│   │   ├── *.png/jpg files                         # Example of global graphical output
│   │   ├── *.svg/pdf files
│   │   └── ...
│   └── ...
│
├── src/                                            # Source folder
│   ├── simulator/                                  # Simulator module
│   │   ├── agents/      
│   │   └── *.py       
│   ├── plotter/                                    # Plotter module
│   │   └── *.py   
│   └── main.py    
│        
├── docker/                                                                      
├── singularity/           
├── requirements.txt                                # Required packages
├── LICENSE                                         # LICENSE
└── README.md                                       # README
```

### Description

MAST contains two main folders:

1. `src/`: contains the source code of MAST
2. `data/`: contains the micro-environment configurations used for our experiments

In particular, the folder `data/` contains both general and specific configurations of the parameters useful to define
the micro-environment that the user wants to simulate.

Within `data/` a general configuration `setup.sim` file, along with an arbitrary number of sub-folders referred to
as `environments` (e.g., `paper_bulk/`) can be defined. For each `environment`, an arbitrary number of files `.dat`
denoted as `systems` can be defined. Each `system` represents a specific subset of general parameters under which a
specific tumor micro-environment should evolve during each simulation.

In particular, `data/` folder must contain only one `setup.sim` file, since it is used to set all the default parameters
necessary to execute a basic simulation. In order to simulate custom micro-environments, it is necessary to generate
specific system `.dat` files, each one containing a particular subset of parameters that will overwrite locally
the `setup.sim` file in order to avoid inconsistencies between micro-environments. Parameters used within MAST are
encoded in the `<parameter>:<value>` format, a detailed explanation of the parameters will be
found [here](PARAMETERS.md).

MAST generates files within two specific folders that **must be created** before running a simulation:

1. `output/`: contains the simulations tabular output of MAST
2. `plot/`: contains the graphical representation of the simulations tabular output generated with MAST

## Getting started

### Run in Docker container (suggested)

In order to run MAST in provided Docker container, you can follow these steps:

#### Prerequisites

Make sure you have installed all the following prerequisites on your machine:

* **Docker** - [Download & Install Docker](https://docs.docker.com/get-docker/)
* **Singularity** - [Download & Install Singularity](https://sylabs.io/guides/3.5/admin-guide/installation.html). It
  becomes a prerequisite only in case you want to run simulations on High Performance Computing (HPC) clusters.

#### Project folder setup

```shell
mkdir <project-folder>                                        # Create a project folder
cd <project-folder>                                           # Move to the project folder

mkdir data                                                    # Create a local data/ folder
mkdir plot                                                    # Create local plot/ folder 
mkdir output                                                  # Create local output/ folder

# Define the setup (.sim) file
cd data                                                       # Move to the data/ folder
touch setup.sim                                               # Create the setup.sim file
```

> **N.B.** The setup (.sim) file definition can be found [here](data/setup.sim), otherwise you can find it on Zenodo at [https://doi.org/10.5281/zenodo.6417153](https://doi.org/10.5281/zenodo.6417153).

#### Get the Docker container

```
# Pull the docker container
docker pull registry.gitlab.com/sysbiobig/mast                

# Run MAST inside the docker container
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py [...]                                  
```

> **N.B.** If you're a Linux user, to be able to run docker as a non-root, we recommend you to refer to the following [post-installation steps](https://docs.docker.com/engine/install/linux-postinstall/).

#### Container extras

MAST can also run on high-performance computing clusters. However, to run within a cluster the Docker container must be
converted to a Singularity container (i.e., `.sif` file). In order to generate a Singularity container starting from a
Docker container, more information can be found [here](singularity/README.md).

### Run locally

##### Prerequisites

Make sure you have installed all the following prerequisites on your machine:

* **Git** - [Download & Install Git](https://git-scm.com/downloads).
* **Python 3.6 (or higher)** - [Download & Install Python](https://www.python.org/downloads/)
* **GNU Make** - [Download & Install GNU Make](https://www.gnu.org/software/make/)
* **GNU Fortran 7.5.0** - [Download & Install GNU Fortran](https://fortran-lang.org/learn/os_setup/install_gfortran)

#### Project folder setup

```shell
# Clone the repository
git clone git@gitlab.com:sysbiobig/mast.git <project-folder>  

# Move to the project folder
cd <project-folder>                                           

mkdir plot                                                    # Create plot/ folder 
mkdir output                                                  # Create output/ folder
```

> **N.B.** If project-folder is omitted, <project-folder> will be mast.

#### Environment setup

```shell
 # Move to the project folder
cd <project-folder>                                          

# Create a virtual environment
VIRTUAL_ENV=/path/to/your/virtual/environment                 # Define your virtual environment path
python3 -m venv $VIRTUAL_ENV                                  # Generate the virtual environment

# Install all the dependencies for MAST inside the environment
pip install -r requirements.txt     
```

#### Compile PYPDELab module

```shell
# Move inside the module PyPDELab
cd <project-folder>/simulator/PyPDELab/fortran

# Build the .f90 files
./build.sh 

# Move outside the module PyPDELab
cd <project-folder>/simulator

# Install the module
./install.sh
```

#### Run the simulator

Now you can run MAST through the following commands:

```shell
# Move to the project folder
cd <project-folder>

# Run MAST
python3 src/main.py [...]                                  
```

### Command-line arguments

MAST accepts several arguments from the command line, it can be used as a simulator or a drawer. To set the behavior of
MAST you need to set one of these flags.

1. `-s, --simu`: flag that enable MAST to act as simulator, further details
   in [Simulator arguments](#simulator-arguments) section
2. `-p, --plot`: flag that enable MAST to generate plot in an interactive way, further details
   in [Plotter (interactive) arguments](#plotter-interactive-arguments) section
3. `-b, --batch`: flag that enable MAST to generate plot in a batch way, further details
   in [Plotter (batch) arguments](#plotter-batch-arguments) section

> **N.B.** The three above flags are mutually exclusive, so their combinations are not valid (e.g., -sp, -pb , -sbp)

##### Simulator arguments

When `-s` flag is set, MAST act as micro-environments simulator and accepts the following parameters:

1. `-e, --environments`: the list of environments (e.g., sub-folders in `data/`) you want to consider. Each environment
   must be comma separated from another, otherwise the value `all` can be passed. All environment are simulated by
   default
2. `--id`: the unique identifier of the systems in the selected environments within the current simulation. If set the
   output folder of the simulated system will have the format `<system>_<identifier>`. It can be both a numeric
   identifier or a string (e.g., JOBID provided within an HPC scheduler
   like [SLURM](https://slurm.schedmd.com/documentation.html))
3. `-d, --debug`: flag that enable debug messages
4. `-par, --parallel`: flag that enable parallel execution. When this flag is active, the maximum number of available
   cpus is considered

##### Plotter (interactive) arguments

When `-p` flag is set, MAST can generate plot in an interactive way through a menu. The menu will guide you in chart,
environment, system, simulation and some customization parameter selection.

##### Plotter (batch) arguments

When `-b` flag is set, MAST can generate plot in a batch way and accepts the following parameters:

1. `--chart`: type of chart you want to create, you can choose between `single`, `multiple`, `agent-comparison`
   , `antigens` (default is `single`) .See the below [Plot in a batch way](#plot-in-a-batch-way) section for further
   information.

2. `--index`: type of numerical index you want to observe to summarize cell cardinality, you can choose among `mean_std`
   , `median` (default is `mean_std`)

3. `--env`: the environment (e.g., sub-folder name in `output/`) to be considered during the generation of the plot (
   selected with the `--chart` argument)

4. `--systems`: the systems to be considered during the generation of the plot. Valid systems are sub-folders name
   within the environment selected with the `--env` argument. Systems can be provided as comma-separated list of
   sub-folders name contained in the environment folder or `all` (default is `all`)

5. `--simulations`: the simulations to be considered during the generation of the plot. Valid simulation are output
   files within the systems selected with the `--systems` argument. Simulations can be provided as comma-separated list
   of valid simulation indices (see `--num_simulations` argument) in the system folder or `all` (default is `all`)

6. `--agents`: the agents to be considered during the generation of the plot. Valid agents are **tumor** (PD-L1-), 
   **pdlp** (PD-L1+), **stroma** (CAF), **n_killer** (Natural killer), **dc** (Dendritic cell), **necro** (Necrotic cell)
   , **ctl** (CTL), **treg** (T regulatory cell). Agents can be provided as comma-separated list or `all` (default
   is `all`)

7. `--num_cycles`: the number of cycles of simulation that you want to consider (e.g., 600)
8. `--num_simulations`: the number of simulations that one wants to take in consideration (e.g., 100)
9. `--scale`: type of scale to be used within the plot, it is possible to choose between linear or logarithmic setting
   respectively `lin` or `log` (default is `lin`)
10. `--base`: if the scale argument has been set to logarithmic, it is possible to set the base of the logarithm to be
    used. You can choose between `log2`,`log3`,`log5` and `log10` (default is `log2`)
11. `--ext`: file extension to be set. It is possible to obtain `png`, `jpg`,`jpeg`, `pdf` and `svg` files (default
    is `png`)

In case of arguments inconsistency with the selected chart type, an error message is reported to the user and the
execution is halted.

## Usage example

After the container is pulled through the command shown in [Running the Docker image](#getting-started) section, MAST
can be used as follows.

### Run as simulator

In order to simulate systems (e.g., `.dat` files) within one (or more) environment(s) it is necessary that inside the
folder `data/` at least one definition system file (e.g., `.dat` file) is present. It is useful to note that by default
MAST does not show debug prints, if you need to enable them just add the `--debug` flag (within in brackets in the code
pieces below). As default, MAST generates two tabular output:

* `*_data_position.csv` which stores the positions of the agents within the simulation
* `*_antigens_position.csv` which stores the list of antigens acquired from a specific agents

Moreover, if specified through the simulation parameter `gif_generate` (see Table in [Parameters](PARAMETERS.md)), the
following plot can be automatically generated at the end of a simulation.

<div align="center">
<img alt="Detailed GIF" src="examples/detailed_animation.gif" width="600">
</div>

#### Sequential execution

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -s --id <id> --environments <environments> (--debug)    
```

#### Parallel execution

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -s --id <id> --environments <environments> --parallel (--debug) 
```

### Plot in a batch way

In order to plot simulation results in a batch way it is necessary that inside the folder `output/` at least one
simulated system is present. Below are shown the graphs generated in a batch way, it is useful to note that the same
graphs can be generated in an interactive way (see the [Plot in an interactive way](#plot-in-an-interactive-way)
section).

#### Single system multiple agents plot

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart single --env paper_bulk --systems bulk_CMS1 --agents all \
                       --num_cycles 600 --num_simulations 100 \
                       --index mean_std --scale log --base log2 --ext png
```

<div align="center">
<img alt="Single system multiple agents plot" src="examples/single_system.png" width="600">
</div>

#### Multiple systems multiple agents plot

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart multiple --env paper_bulk --systems all --agents all \
                       --num_cycles 600 --num_simulations 100 \
                       --index mean_std --scale log --base log2 --ext png
```

<div align="center">
<img alt="Multiple systems multiple agents plot" src="examples/systems_comparison.png">
</div>

#### Single system antigens plot

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart antigens --env paper_bulk --systems bulk_CMS1 --simulations 60 --agents all \
                       --num_cycles 600 --num_simulations 100 \
                       --ext png
```

<div align="center">
<img alt="Single system antigens plot" src="examples/antigens.png">
</div>

#### Single agent multiple system comparison plot

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart agent-comparison --env paper_bulk --systems all --agents ctl \
                       --num_cycles 600 --num_simulations 100 \
                       --ext png
```

<div align="center">
<img alt="Single system antigens plot" src="examples/ctl_comparison.png"  width="600">
</div>

#### Spatial evolution plot

###### Generate GIF

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart gif --env paper_bulk --systems bulk_CMS1 --simulations 2 \
                       --num_cycles 600 --num_simulations 100
```

<div align="center">
<img alt="Animation" src="examples/animation.gif" width="600">
</div>

###### Extract frames

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -b --chart frames --env paper_bulk --systems all --simulations 2 \
                       --num_cycles 600 --num_simulations 100
```

<div align="center">
<img alt="Animation frame 30" src="examples/animation_frame_30.png" width="300" >
<img alt="Animation frame 60" src="examples/animation_frame_60.png" width="300">
<img alt="Animation frame 90" src="examples/animation_frame_90.png" width="300">
<img alt="Animation frame 120" src="examples/animation_frame_120.png" width="300">
<img alt="Animation frame 150" src="examples/animation_frame_150.png" width="300">
</div>

### Plot in an interactive way

In order to plot simulation results in an interactive way it is necessary that inside the folder `output/` at least one
simulated system is present.

```shell
docker run -v $(pwd)/output:/home/mast/output \               # Bind the output/ folder
           -v $(pwd)/data:/home/mast/data \                   # Bind the data/ folder
           -v $(pwd)/plot:/home/mast/plot \                   # Bind the plot/ folder
           -u $(id -u):$(id -g) \                             # Set user/group id
           -ti registry.gitlab.com/sysbiobig/mast \           # Set the container
           src/main.py -p
```

## Create a bug report

If you run into any issue, please feel free to [create a bug report](https://gitlab.com/sysbiobig/mast/-/issues/new).

## License

This project is licensed under the GNU General Public License v3.0. See the [LICENSE](LICENSE) file for details

## Contact

* **Cesaro Giulia**<sup>1</sup> - [giulia.cesaro.1@phd.unipd.it](mailto:giulia.cesaro.1@phd.unipd.it)
* **Milia Mikele**<sup>1</sup> - [mikele.milia@phd.unipd.it](mailto:mikele.milia@phd.unipd.it)
* **Baruzzo Giacomo**<sup>1</sup> - [giacomo.baruzzo@unipd.it](mailto:giacomo.baruzzo@unipd.it)
* **Di Camillo Barbara**<sup>1,2</sup> - [barbara.dicamillo@unipd.it](mailto:barbara.dicamillo@unipd.it)

<sup>1</sup> Department of Information Engineering, University of Padova, Padova, Italy
<br/>
<sup>2</sup> Department of Comparative Biomedicine and Food Science, University of Padova, Padova, Italy
