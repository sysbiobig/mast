import io
import os

from setuptools import find_packages, setup

# Package meta-data.
NAME = 'mastdsa'
DESCRIPTION = 'MAST is a multi-agent based simulator of the interaction between immune system and tumor.'
URL = 'https://gitlab.com/sysbiobig/mast.git'
EMAIL = ''  # Specify an email
LICENSE = ''  # Specify a license
AUTHOR = 'SysBioBig - System Biology and Bioinformatics Group'
REQUIRES_PYTHON = '>=3.6.0'
VERSION = '0.1.0'

# Specify which packages are required
REQUIRED = []

# Specify which packages are optional
EXTRAS = {}

# Retrieve the current working directory
cwd = os.path.abspath(os.path.dirname(__file__))

# README and use it as the long-description.
# Note: this will only work if 'README.md' is present in your MANIFEST.in file!
try:
    with io.open(os.path.join(cwd, 'README.md'), encoding='utf-8') as f:
        long_description = '\n' + f.read()
except FileNotFoundError:
    long_description = DESCRIPTION

# Load the package's __version__.py module as a dictionary.
about = {}
if not VERSION:
    project_slug = NAME.lower().replace("-", "_").replace(" ", "_")
    with open(os.path.join(cwd, project_slug, '__version__.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION

# Where the magic happens:
setup(
    name=NAME,
    version=about['__version__'],
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    author_email=EMAIL,
    python_requires=REQUIRES_PYTHON,
    url=URL,
    packages=find_packages(
        exclude=[
            "tests",
            "*.tests",
            "*.tests.*",
            "tests.*"
        ]
    ),
    # entry_points={
    #     'console_scripts': [],
    # },
    install_requires=REQUIRED,
    extras_require=EXTRAS,
    include_package_data=True,
    license=LICENSE,
    classifiers=[
        'Programming Language :: Python :: 3.8'
    ]
)
